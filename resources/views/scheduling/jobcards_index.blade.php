@extends('template')
@section('content')
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Scheduling </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item"><a href="#">Scheduling</a></li>
									<li class="breadcrumb-item active">Job Cards</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Job Cards</strong>
								</div>
									
								<div class="card-body">
									<p><strong>Click on each cards to see the full list of job cards. </strong></p>
									<div class="row">
										<div class="col-xs-12 col-md-6 ">
											<a href="{{url('job_cards/today_new_orders')}}">
												<div class="card-box noradius noborder bg-default">
													<i class="fa fa-diamond float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Today New Orders</h6>
													<h1 class="m-b-20 text-white counter">{{$new_uncompleted_orders}}</h1>
												</div>
											</a>
										</div>
	
										<div class="col-xs-12 col-md-6">
											<a href="{{url('job_cards/past_uncompleted_orders')}}">
												<div class="card-box noradius noborder bg-danger">
													<i class="fa fa-minus-square float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Past Uncompleted Orders</h6>
													<h1 class="m-b-20 text-white counter">{{$past_uncompleted_orders}}</h1>
												</div>
											</a>
											
										</div>
	
										<div class="col-xs-12 col-md-6">
											<a href="{{url('job_cards/completed_orders')}}">
												<div class="card-box noradius noborder bg-success">
													<i class="fa fa-check-square float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Completed Orders</h6>
													<h1 class="m-b-20 text-white counter">{{$completed_orders}}</h1>
												</div>
											</a>
										</div>
										
										<div class="col-xs-12 col-md-6">
											<a href="#">
												<div class="card-box noradius noborder bg-info">
													<i class="fa fa-calendar-check-o float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">All Orders</h6>
													<h1 class="m-b-20 text-white counter">{{$all_orders}}</h1>
												</div>
											</a>
										</div>
	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
