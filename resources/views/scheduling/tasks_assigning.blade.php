@extends('template')
@section('content')
		<style type="text/css">
		.task_sortable{
				padding-left:0;
				float: left;
				width: 100%;
				min-height: 50px;
				list-style:none;
		}
		</style>
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<?php use App\Http\Controllers\scheduling\tasks; ?>
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Scheduling </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item"><a href="#">Scheduling</a></li>
									<li class="breadcrumb-item "><a href="{{url('job_cards/')}}">Job Cards</a></li>
									<li class="breadcrumb-item"><a href="{{url('job_cards/task/')}}/{{$jobcards->idjobcards}}">Tasks</a></li>
									<li class="breadcrumb-item active">Task Assigning</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Task General Details</strong>
								</div>
									
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>Task Unique ID</td>
													<td>
														{{$tasks->taskname}}
													</td>
												</tr>
												<tr>
													<td>Job Type</td>
													<td>
														<?php 
															if($tasks->job_type == 1){
																$job_type = "Bin Delivery";
																$icon = "fa-calendar-plus-o";
															} elseif($tasks->job_type == 2) {
																$job_type = "Bin Collection";
																$icon = "fa-times-plus-o";
															} elseif($tasks->job_type == 3) {
																$job_type = "Depo Visits";
																$icon = "fa-times-plus-o";
															}
														?>
														{{$job_type}}
													</td>
												</tr>
												<tr>
													<td>Customer</td>
													<td> {{$tasks->organization_name}}</td>
												</tr>
												<tr>
													<td>Delivery address</td>
													<td> {{$tasks->unit_number}} {{$tasks->street_name}} {{$tasks->post_code}}</td>
												</tr>
												<tr>
													<td>Start Time</td>
													<td><?=date('d-m-Y', strtotime($tasks->starttime)); ?></td>
												</tr>
												<tr>
													<td>Expected End Time</td>
													<td>
														<form action="{{url('tasks/update_endtime')}}" method="post">
															{{csrf_field()}}
															<div class="form-row">
																<div class="col-8">
																	<input type="hidden" name="idtask_endtime" value="{{$tasks->idtask}}"/>
																	<input type="text" class="form-control form-control-sm form-datepicker" name="endtime" 
																	value="<?=date('d-m-Y', strtotime($tasks->endtime)); ?>"/>
																</div>
																<div class="col-4">
																	<button type="submit" class="btn btn-primary btn-sm">Update</button>
																</div>
															</div>
														</form>
													</td>
												</tr>
												<tr>
													<td>Assigned By</td>
													<td>
														{{$supplier->name}}
													</td>
												</tr>
												<tr>
													<td>Status</td>
													<td>
														
														<form action="{{url('tasks/update_status')}}" method="post">
															{{csrf_field()}}
															<div class="form-row">
																<div class="col-8">
																	
																	<input type="hidden" name="idtask_taskstatus" value="{{$tasks->idtask}}"/>
																	<select name="status" id="status" class="form-control">
																		@if(!is_null($statuses))
																			@foreach($statuses as $status)
																				<option <?=$tasks->taskstatus == $status->id ? 'selected="selected"' : ''; ?> value="{{$status->id}}" 
																				 >{{$status->status}}</option>
																			@endforeach
																		@endif
																	</select>
																</div>
																<div class="col-4">
																	<button type="submit" class="btn btn-primary">Update</button>
																</div>
															</div>
														</form>
													</td>
												</tr>
												<tr>
													
												</tr>
												<tr>
													<td>Bin Order</td>
													<td>
														<?php $orderitems = tasks::fetch_order_items_id($tasks->idorderitems);?>
														@if(!is_null($orderitems))
															{{$orderitems->bintype_name}} - {{$orderitems->size}}
														@endif
													</td>
												</tr>
												<tr>
													<td>Delivery Note</td>
													<td>
														@if(!is_null($orderitems))
															{{$orderitems->delivery_comments}}
														@endif
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Available Drivers / Idle Drivers</strong>
								</div>
									
								<div class="card-body">
									<p>Drag and drop these items into task tab</p>
									@if(!is_null($drivers) && ($drivers->count() > 0))
										
										<ul id="available_driver" class="task_sortable driver_sortable" style="float:left;">
											@foreach($drivers as $driver)
												@if($tasks->driver != $driver->id)
														<li class="ui-state-default btn btn-success mb-2 w-100" data-driver-id="{{$driver->id}}">{{$driver->name}} - {{$driver->licence}}</li>
												@endif
											@endforeach
										</ul>
										
									@endif
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Task assigning</strong>
								</div>
									
								<div class="card-body">
									<p>Assign driver and truck to this section</p>
									<ul id="main_send" class="task_sortable  driver_target driver_sortable mb-0" style="float:left;">
										<input type="hidden" name="idtaks" value="{{$tasks->idtask}}"/>
									
										@if(!is_null($tasks->driver) && $tasks->driver!=0)
											<li class="btn btn-success mb-2 w-100 ui-state-highlighted" data-driver-id="{{$tasks->driver}}">{{$tasks->drivername}} - {{$tasks->licence}}</li>
										@endif
										
									</ul>
									
									<button class="btn btn-primary btn-sm" type="submit" id="update_assign">Update task</button>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4">
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Bin Stock Information - Heavy Mixed Waste </strong> <br />
									<span>Today, <?=date('d-m-Y', strtotime('now'));?></span>
								</div>
									
								<div class="card-body">
									@if(!is_null($sizes) && ($sizes->count() > 0))
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													@foreach($sizes as $size)
														<tr>
															<td>{{$size->size}}</td>
															@if(!is_null($baseprice_heavymixed))
																<?php foreach($baseprice_heavymixed as $data):?>
																	@if($size->idSize == $data->idBinSize)
																		<?php foreach($binservicebaseprice_heavymixed[$data->idBinSize] as $baseitems):?>
																			@if(is_null($baseitems->default_stock))
																				<?php $value = 0 ?>
																			@else
																				<?php $value = $baseitems->default_stock ?>
																			@endif
																		<?php endforeach?>

																		<?php foreach($binservicestok_heavymixed[$data->idBinService] as $discitems => $itemsvalue):?>

																			@if($itemsvalue->date == date('Y-m-d', strtotime('now')))
																				<?php $value =  $itemsvalue->stock; break;?>
																		
																			@endif
																		<?php endforeach?>
																	@endif
																<?php endforeach;?>
															@endif
															<td>{{$value}}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
										
									@else
										No bin data
									@endif
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#available_driver, .driver_target").sortable({
					connectWith: ".driver_sortable"
				}).disableSelection();
				
			});
		</script>
@endsection
