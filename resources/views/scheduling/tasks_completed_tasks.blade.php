@extends('template')
@section('content')
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Scheduling </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item"><a href="#">Scheduling</a></li>
									<li class="breadcrumb-item active">Tasks</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<?php use App\Http\Controllers\scheduling\tasks; ?>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Date range tasks list</strong>
								</div>
								<div class="card-body">
									<p><strong>Click on each cards to see the full list of tasks. </strong></p>
									<div class="row">
										<div class="col-xs-12 col-md-3 ">
											<a href="{{url('tasks/today_new_tasks')}}">
												<div class="card-box noradius noborder bg-default">
													<i class="fa fa-diamond float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Today</h6>
													<h1 class="m-b-20 text-white counter"></h1>
													<span class="text-white">Show today tasks</span>
												</div>
											</a>
										</div>
										<div class="col-xs-12 col-md-3">
											<a href="{{url('tasks/past_uncompleted_tasks')}}">
												<div class="card-box noradius noborder bg-danger">
													<i class="fa fa-minus-square float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Past</h6>
													<h1 class="m-b-20 text-white counter"></h1>
													<span class="text-white">Show past uncompleted tasks</span>
												</div>
											</a>
										
										</div>
	
										<div class="col-xs-12 col-md-3">
											<a href="{{url('tasks/zone_tasks')}}">
												<div class="card-box noradius noborder bg-success">
													<i class="fa fa-globe float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Zone</h6>
													<h1 class="m-b-20 text-white counter"></h1>
													<span class="text-white">Show tasks by zone</span>
												</div>
											</a>
										</div>
										
										<div class="col-xs-12 col-md-3">
											<a href="{{url('tasks/date_tasks')}}">
												<div class="card-box noradius noborder bg-info">
													<i class="fa fa-calendar-check-o float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Date</h6>
													<h1 class="m-b-20 text-white counter"></h1>
													<span class="text-white">Show tasks by date</span>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					@if(!is_null($tasks) && ($tasks->count() > 0))
						<div class="row">
							<div class="col-12">
								<div class="card mb-3 mt-2">
									<div class="card-body">
										<strong>All completed tasks list</strong>
										<div class="table-responsive">
											<table class="table table-bordered table-striped">
												@foreach($tasks as $task)
													<?php 
														if($task->job_type == 1){
															$job_type = "Bin Delivery";
															$icon = "fa-calendar-plus-o";
														} elseif($task->job_type == 2) {
															$job_type = "Bin Collection";
															$icon = "fa-times-plus-o";
														} elseif($task->job_type == 3) {
															$job_type = "Depo Visits";
															$icon = "fa-times-plus-o";
														}
													?>
													
													<tr>
														<th>Task ID</th>
														<th>Job Type</th>
														<th>Size</th>
														<th>Driver</th>
														<th>Status</th>
													</tr>
													<tr>
														<td>{{$task->taskname}}</td>
														<td>{{$job_type}}</td>
														<td>{{$task->size}}</td>
														<td>{{$task->drivername}}</td>
														<td>
															<a class="btn btn-success btn-sm">{{$task->taskstatusword}}</a>
														</td>
													</tr>
													<tr>
														<td colspan="2"><strong>Delivery Address</strong></td>
														<td colspan="3">{{$task->unit_number}} {{$task->street_name}} {{$task->post_code}}</td>
													</tr>													<tr>														<td colspan="2"><strong>Start</strong></td>														<td colspan="3"><?=date('d-m-Y', strtotime($task->starttime)); ?></td>													</tr>													<tr>														<td colspan="2"><strong>End</strong></td>														<td colspan="3"><?=date('d-m-Y', strtotime($task->endtime)); ?></td>													</tr>													<tr>														<td colspan="2"><strong>Delivery Note</strong></td>														<td colspan="3">{{$task->delivery_comments}}</td>													</tr>													<tr>														<td colspan="5"><hr /></td>													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					@else 
						<div class="row">
							<div class="col-12">
								<div class="card mb-3 mt-2">
									<div class="card-body">
										<strong>No new task data</strong>
									</div>
								</div>
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>
@endsection
