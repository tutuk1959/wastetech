@extends('template')
@section('content')
<style type="text/css">
	.btn{
		white-space: initial;
	}
</style>
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Scheduling </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item"><a href="#">Scheduling</a></li>
									<li class="breadcrumb-item active">Tasks</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<?php use App\Http\Controllers\scheduling\tasks; ?>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Today tasks list</strong>
								</div>
									
								<div class="card-body">
									<p><strong>Click on each cards to see the full list of tasks. </strong></p>
									<div class="row">
										<div class="col-xs-12 col-md-3 ">
											<a href="{{url('tasks/today_new_tasks')}}">
												<div class="card-box noradius noborder bg-default">
													<i class="fa fa-diamond float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Today</h6>
													<h1 class="m-b-20 text-white counter"></h1>
													<span class="text-white">Show today tasks</span>
												</div>
											</a>
										</div>
	
										<div class="col-xs-12 col-md-3">
											<a href="{{url('tasks/past_uncompleted_tasks')}}">
												<div class="card-box noradius noborder bg-danger">
													<i class="fa fa-minus-square float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Past</h6>
													<h1 class="m-b-20 text-white counter"></h1>
													<span class="text-white">Show past uncompleted tasks</span>
												</div>
											</a>
											
										</div>
	
										<div class="col-xs-12 col-md-3">
											<a href="{{url('tasks/zone_tasks')}}">
												<div class="card-box noradius noborder bg-success">
													<i class="fa fa-globe float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Zone</h6>
													<h1 class="m-b-20 text-white counter"></h1>
													<span class="text-white">Show tasks by zone</span>
												</div>
											</a>
										</div>
										
										<div class="col-xs-12 col-md-3">
											<a href="{{url('tasks/date_tasks')}}">
												<div class="card-box noradius noborder bg-info">
													<i class="fa fa-calendar-check-o float-right text-white  mt-0"></i>
													<h6 class="text-white text-uppercase m-b-20">Date</h6>
													<h1 class="m-b-20 text-white counter"></h1>
													<span class="text-white">Show tasks by date</span>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@if(!is_null($drivers) && ($drivers->count() > 0))
						<div class="row">
							<div class="col-12">
								<div class="card mb-3 mt-2">
									<div class="card-header">
										<strong>Drivers </strong>
										<a href="{{url('tasks/completed_tasks')}}" class="btn btn-primary btn-sm ml-3"> See list of completed tasks</a>
									</div>
									<div class="card-body">
										<strong class="text-danger">Refresh the page to see task progress by the drivers. </strong>
										@if(!is_null($drivers) && ($drivers->count() > 0))
											<div class="row">
												@foreach($drivers as $driver)
													<div class="col-12 col-md-4">
														<div class="card mb-3 mt-2">
															<div class="card-header">
																<strong>{{$driver->drivername}} - {{$driver->truckname}}</strong>
															</div>
														
															<div class="card-body">
																<input type="hidden" name="iddriver" value="{{$driver->driverid}}"/>
																<ul class="driver_target task_sortable p-0" style="float: left;min-width: 100%;min-height: 50px;">
																	<?php $driver_tasks = tasks::get_available_drivers_task($driver->driverid); ?>
																	
																	@foreach($driver_tasks as $task)
																		<?php 
																			if($task->is_quote == 1){
																				$quote = "Quote";
																			} elseif($task->is_quote == 0) {
																				$job_type = "Order";
																			} else {
																				$job_type = "Order";
																			}
																			?>
																		<li class="ui-state-default btn btn-primary mb-2 w-100" data-task-id="{{$task->idtask}}">
																			<p class="mb-0">
																				<a class="btn btn-warning btn-sm ml-2">{{$task->taskstatusword}} </a> {{$task->taskname}}
																				- {{$task->unit_number}} {{$task->street_name}} {{$task->post_code}} / {{$task->size}}
																				<?php if($task->active_task == 1):?>
																					<a class="btn btn-success btn-sm ml-2">Active</a>
																				<?php else: ?>
																					<a class="btn btn-danger btn-sm ml-2">Inactive</a>
																				<?php endif; ?>
																				<a href="{{url('tasks/assign/')}}/{{$task->idtask}}" class="btn btn-dark btn-sm ml-2">View task <i class="fa fa-search"></i></a>
																			</p>
																		</li>
																	@endforeach
																</ul>
																<button class="btn btn-primary btn-sm assign_driver" type="submit"
																>Assign</button>
															</div>
														</div>
													</div>
												
												@endforeach
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>
					@endif
					@if(!is_null($tasks) && ($tasks->count() > 0))
						<div class="row">
							<div class="col-12 col-md-8">						
								<div class="card mb-3 mt-2">
									<div class="card-header">
										<strong>Assignable tasks by today. </strong>
										<span>All new uncompleted tasks list</span>
									</div>
										
									<div class="card-body">
										<p>Drag and drop these items into drivers tab</p>
										<ul id="available_tasks" class="task_sortable p-0" style="float: left;min-width: 100%;min-height: 50px;">
											@foreach($tasks as $task)
												<?php 
													if($task->is_quote == 1){
														$quote = "Quote";
													} elseif($task->is_quote == 0) {
														$job_type = "Order";
													} else {
														$job_type = "Order";
													}
													?>
												<li class="ui-state-default btn btn-primary mb-2 w-100" data-task-id="{{$task->idtask}}">
													<p class="mb-0">
														<a class="btn btn-warning btn-sm ml-2">{{$task->taskstatusword}} </a> {{$task->taskname}}
														- {{$task->unit_number}} {{$task->street_name}} {{$task->post_code}} / {{$task->size}}
														<?php if($task->active_task == 1):?>
															<a class="btn btn-success btn-sm ml-2">Active</a>
														<?php else: ?>
															<a class="btn btn-danger btn-sm ml-2">Inactive</a>
														<?php endif; ?>
														<a href="{{url('tasks/assign/')}}/{{$task->idtask}}" class="btn btn-dark btn-sm ml-2">View task <i class="fa fa-search"></i></a>
													</p>
												</li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
							
							<div class="col-12 col-md-4">
								<strong>Bin Stock Information </strong> <br />
								<span>Today, <?=date('d-m-Y', strtotime('now'));?></span>
								@if(!is_null($sizes) && ($sizes->count() > 0))
									<div class="table-responsive">
										<table class="table table-striped">
											<tbody>
												@foreach($sizes as $size)
													<tr>
														<td>{{$size->size}}</td>
														@if(!is_null($baseprice_heavymixed))
															<?php foreach($baseprice_heavymixed as $data):?>
																@if($size->idSize == $data->idBinSize)
																	<?php foreach($binservicebaseprice_heavymixed[$data->idBinSize] as $baseitems):?>
																		@if(is_null($baseitems->default_stock))
																			<?php $value = 0 ?>
																		@else
																			<?php $value = $baseitems->default_stock ?>
																		@endif
																	<?php endforeach?>

																	<?php foreach($binservicestok_heavymixed[$data->idBinService] as $discitems => $itemsvalue):?>

																		@if($itemsvalue->date == date('Y-m-d', strtotime('now')))
																			<?php $value =  $itemsvalue->stock; break;?>
																	
																		@endif
																	<?php endforeach?>
																@endif
															<?php endforeach;?>
														@endif
														<td>{{$value}}</td>
													</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								@else
									No bin data
								@endif
							</div>
						</div>
					@else 
						<div class="row">
							<div class="col-12 col-md-8">						
								<div class="card mb-3 mt-2">
									<div class="card-header">
										<strong>Assignable tasks by today. </strong>
										<span>All new uncompleted tasks list</span>
									</div>
										
									<div class="card-body">
										<p>Drag and drop these items into drivers tab</p>
										<ul id="available_tasks" class="task_sortable p-0" style="float: left;min-width: 100%;min-height: 50px;">
											
											
										</ul>
									</div>
								</div>
							</div>
							
							<div class="col-12 col-md-4">
								<strong>Bin Stock Information </strong> <br />
								<span>Today, <?=date('d-m-Y', strtotime('now'));?></span>
								@if(!is_null($sizes) && ($sizes->count() > 0))
									<div class="table-responsive">
										<table class="table table-striped">
											<tbody>
												@foreach($sizes as $size)
													<tr>
														<td>{{$size->size}}</td>
														@if(!is_null($baseprice_heavymixed))
															<?php foreach($baseprice_heavymixed as $data):?>
																@if($size->idSize == $data->idBinSize)
																	<?php foreach($binservicebaseprice_heavymixed[$data->idBinSize] as $baseitems):?>
																		@if(is_null($baseitems->default_stock))
																			<?php $value = 0 ?>
																		@else
																			<?php $value = $baseitems->default_stock ?>
																		@endif
																	<?php endforeach?>

																	<?php foreach($binservicestok_heavymixed[$data->idBinService] as $discitems => $itemsvalue):?>

																		@if($itemsvalue->date == date('Y-m-d', strtotime('now')))
																			<?php $value =  $itemsvalue->stock; break;?>
																	
																		@endif
																	<?php endforeach?>
																@endif
															<?php endforeach;?>
														@endif
														<td>{{$value}}</td>
													</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								@else
									No bin data
								@endif
							</div>
						</div>
					@endif
					
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#available_tasks, .driver_target").sortable({
					connectWith: ".task_sortable"
				}).disableSelection();
				
			});
		</script>
@endsection
