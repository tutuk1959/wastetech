@extends('template')
@section('content')
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<?php use App\Http\Controllers\scheduling\tasks; ?>
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Scheduling </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item"><a href="#">Scheduling</a></li>
									<li class="breadcrumb-item "><a href="{{url('job_cards/')}}">Job Cards</a></li>
									<li class="breadcrumb-item active">Tasks</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-md-6">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Task for Job Orders #{{$jobcards->paymentUniqueCode}}</strong>
								</div>
									
								<div class="card-body">
									<p><strong>General Details</strong></p>
									@if(!is_null($jobcards) && !is_null($tasks))
										<div class="table-responsive">
											<table class="table table-bordered table-striped">
												<tbody>
													
													<tr>
														<td>Organization Name</td>
														<td>{{$jobcards->organization_name}}</td>
													</tr>
													<tr>
														<td>Project Site</td>
														<td>{{$jobcards->sitename}}</td>
													</tr>
													<tr>
														<td>Job Date</td>
														<td><?=date('l d-m-Y', strtotime($jobcards->delivery_date));?></td>
													</tr>
													<tr>
														<td>Primary Contact Person</td>
														<td>
															<form action="{{url('job_cards/update_primary_contact')}}" method="post">
																{{csrf_field()}}
																<div class="form-row">
																	<div class="col-8">
																		<input type="hidden" name="idjobcards" value="{{$jobcards->idjobcards}}"/>
																		<input type="text" class="form-control form-control-sm" name="primary_contact" value="{{$jobcards->primarycontact}}"/>
																	</div>
																	<div class="col-4">
																		<button type="submit" class="btn btn-primary btn-sm">Update</button>
																	</div>
																</div>
															</form>
														</td>
													</tr>
													<tr>
														<td>Job Card Status</td>
														<td>
															@if($jobcards->jobcard_status == 1)
																<button class="btn btn-info btn-sm" type="button">Open</button>
															@elseif($jobcards->jobcard_status == 2)
																<button class="btn btn-warning btn-sm" type="button">In-Progress</button>
															@elseif($jobcards->jobcard_status == 3)
																<button class="btn btn-danger btn-sm" type="button">Closed</button>
															@endif
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										
										<div class="row">
											<div class="col-12">
												<p>Click on each tasks to see the details or assign driver!</p>
											</div>
											@foreach($tasks as $task)
												<?php
													$status = "";
													$color = "default";
													if($task->job_type == 1){
														if($task->taskstatus == '1'){
															$status = "Open";
															$color = "default";
														} elseif($task->taskstatus == '2') {
															$status = "In Progress";
															$color = "warning";
														} elseif($task->taskstatus == '3'){
															$status = "On Route";
															$color = "warning";
														} elseif($task->taskstatus == '4'){
															$status = "On Site";
															$color = "warning";
														} elseif($task->taskstatus == '5'){
															$status = "Completed";
																$color = "success";
														} elseif($task->taskstatus == '6'){
															$status = "Cancel";
																$color = "danger";
														} elseif($task->taskstatus == '7'){
															$status = "Paused";
															$color = "warning";
														} 
														
														$job_type = "Bin Delivery";
														$icon = "fa-calendar-plus-o";
													} elseif($task->job_type == 2) {
														if($task->taskstatus == '1'){
															$status = "Open";
															$color = "default";
														} elseif($task->taskstatus == '2') {
															$status = "In Progress";
															$color = "warning";
														} elseif($task->taskstatus == '3'){
															$status = "On Route";
															$color = "warning";
														} elseif($task->taskstatus == '4'){
															$status = "On Site";
															$color = "warning";
														} elseif($task->taskstatus == '5'){
															$status = "Completed";
																$color = "success";
														} elseif($task->taskstatus == '6'){
															$status = "Cancel";
																$color = "danger";
														} elseif($task->taskstatus == '7'){
															$status = "Paused";
															$color = "warning";
														} 
														$job_type = "Bin Collection";
														$icon = "fa-times-plus-o";
													} elseif($task->job_type == 3) {
														if($task->taskstatus == '1'){
															$status = "Open";
															$color = "default";
														} elseif($task->taskstatus == '2') {
															$status = "In Progress";
															$color = "warning";
														} elseif($task->taskstatus == '3'){
															$status = "On Route";
															$color = "warning";
														} elseif($task->taskstatus == '4'){
															$status = "On Site";
															$color = "warning";
														} elseif($task->taskstatus == '5'){
															$status = "Completed";
																$color = "success";
														} elseif($task->taskstatus == '6'){
															$status = "Cancel";
																$color = "danger";
														} elseif($task->taskstatus == '7'){
															$status = "Paused";
															$color = "warning";
														} 
														$job_type = "Depo Visits";
														$icon = "fa-times-plus-o";
													}
													
													
												?>
													<div class="col-xs-12 col-md-6 ">
														<a href="{{url('tasks/assign')}}/{{$task->idtask}}">
															<div class="card-box noradius noborder bg-{{$color}}">
																<i class="fa float-right text-white  mt-0"></i>
																<?php $orderitems = tasks::fetch_order_items_id($task->idorderitems);?>
																@if(!is_null($orderitems))
																	<h6 class="text-white text-uppercase m-b-20">{{$orderitems->bintype_name}} - {{$orderitems->size}}</h6>
																@endif
																
																<h3 class="m-b-20 text-white counter">{{$job_type}}</h3>
																<span class="text-white"><?=date('d-m-Y', strtotime($task->starttime)); ?></span> <br />
																<span class="text-white">Status : {{$status}}</span>
															</div>
														</a>
													</div>
											@endforeach
										</div>
									@endif
								</div>
							</div>
							
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Available Drivers / Idle Drivers</strong>
								</div>
									
								<div class="card-body">
									
									@if(!is_null($drivers) && ($drivers->count() > 0))
										<div class="table-responsive">
											<table class="table table-striped">
												<thead>
													<td>Name</td>
													<td>License</td>
												</thead>
												<tbody>
													@foreach($drivers as $driver)
														<tr>
															<td>{{$driver->name}}</td>
															<td>{{$driver->licence}}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									@endif
								</div>
							</div>
						</div>
						
						<div class="col-12 col-md-6">
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Bin Stock Information - General Waste </strong> <br />
									<span>Today, <?=date('d-m-Y', strtotime('now'));?></span>
								</div>
									
								<div class="card-body">
									@if(!is_null($sizes) && ($sizes->count() > 0))
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													@foreach($sizes as $size)
														<tr>
															<td>{{$size->size}}</td>
															@if(!is_null($baseprice_generalwaste))
																<?php foreach($baseprice_generalwaste as $data):?>
																	@if($size->idSize == $data->idBinSize)
																		<?php foreach($binservicebaseprice_generalwaste[$data->idBinSize] as $baseitems):?>
																			@if(is_null($baseitems->default_stock))
																				<?php $value = 0 ?>
																			@else
																				<?php $value = $baseitems->default_stock ?>
																			@endif
																		<?php endforeach?>

																		<?php foreach($binservicestok_generalwaste[$data->idBinService] as $discitems => $itemsvalue):?>

																			@if($itemsvalue->date == date('Y-m-d', strtotime('now')))
																				<?php $value =  $itemsvalue->stock; break;?>
																		
																			@endif
																		<?php endforeach?>
																	@endif
																<?php endforeach;?>
															@endif
															<td>{{$value}}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
										
									@else
										No bin data
									@endif
								</div>
							</div>
							
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Bin Stock Information - Heavy Mixed Waste </strong> <br />
									<span>Today, <?=date('d-m-Y', strtotime('now'));?></span>
								</div>
									
								<div class="card-body">
									@if(!is_null($sizes) && ($sizes->count() > 0))
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													@foreach($sizes as $size)
														<tr>
															<td>{{$size->size}}</td>
															@if(!is_null($baseprice_heavymixed))
																<?php foreach($baseprice_heavymixed as $data):?>
																	@if($size->idSize == $data->idBinSize)
																		<?php foreach($binservicebaseprice_heavymixed[$data->idBinSize] as $baseitems):?>
																			@if(is_null($baseitems->default_stock))
																				<?php $value = 0 ?>
																			@else
																				<?php $value = $baseitems->default_stock ?>
																			@endif
																		<?php endforeach?>

																		<?php foreach($binservicestok_heavymixed[$data->idBinService] as $discitems => $itemsvalue):?>

																			@if($itemsvalue->date == date('Y-m-d', strtotime('now')))
																				<?php $value =  $itemsvalue->stock; break;?>
																		
																			@endif
																		<?php endforeach?>
																	@endif
																<?php endforeach;?>
															@endif
															<td>{{$value}}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
										
									@else
										No bin data
									@endif
								</div>
							</div>
							
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Bin Stock Information - Green Waste </strong> <br />
									<span>Today, <?=date('d-m-Y', strtotime('now'));?></span>
								</div>
									
								<div class="card-body">
									@if(!is_null($sizes) && ($sizes->count() > 0))
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													@foreach($sizes as $size)
														<tr>
															<td>{{$size->size}}</td>
															@if(!is_null($baseprice_green))
																<?php foreach($baseprice_green as $data):?>
																	@if($size->idSize == $data->idBinSize)
																		<?php foreach($binservicebaseprice_green[$data->idBinSize] as $baseitems):?>
																			@if(is_null($baseitems->default_stock))
																				<?php $value = 0 ?>
																			@else
																				<?php $value = $baseitems->default_stock ?>
																			@endif
																		<?php endforeach?>

																		<?php foreach($binservicestok_green[$data->idBinService] as $discitems => $itemsvalue):?>

																			@if($itemsvalue->date == date('Y-m-d', strtotime('now')))
																				<?php $value =  $itemsvalue->stock; break;?>
																		
																			@endif
																		<?php endforeach?>
																	@endif
																<?php endforeach;?>
															@endif
															<td>{{$value}}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
										
									@else
										No bin data
									@endif
								</div>
							</div>
							
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Bin Stock Information - Dirt Waste</strong> <br />
									<span>Today, <?=date('d-m-Y', strtotime('now'));?></span>
								</div>
									
								<div class="card-body">
									@if(!is_null($sizes) && ($sizes->count() > 0))
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													@foreach($sizes as $size)
														<tr>
															<td>{{$size->size}}</td>
															@if(!is_null($baseprice_green))
																<?php foreach($baseprice_dirt as $data):?>
																	
																	@if($size->idSize == $data->idBinSize)
																		<?php foreach($binservicebaseprice_dirt[$data->idBinSize] as $baseitems):?>
																			@if(is_null($baseitems->default_stock))
																				<?php $value = 0 ?>
																			@else
																				<?php $value = $baseitems->default_stock ?>
																			@endif
																		<?php endforeach?>

																		<?php foreach($binservicestok_dirt[$data->idBinService] as $discitems => $itemsvalue):?>

																			@if($itemsvalue->date == date('Y-m-d', strtotime('now')))
																				<?php $value =  $itemsvalue->stock; break;?>
																		
																			@endif
																		<?php endforeach?>
																	@endif
																<?php endforeach;?>
															@endif
															<td>{{$value}}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
										
									@else
										No bin data
									@endif
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
