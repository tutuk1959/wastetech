<!DOCTYPE html>
<html lang="en">
<head>
	<script>
		window.dataLayer = window.dataLayer || [];
		var trx = {
			'id': '{{$reference}}',
			'total': {{floatval($amount/100)}},
			'gst': {{$order['gst']}},
			'sku': '{{$order['sku']}}',
			'name': '{{$order['name']}}',
			'price': {{$order['price']}}
		};
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MR7J49J');</script>
	<!-- End Google Tag Manager -->

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{csrf_token()}}">
	<title>Waste Technology Solutions</title>
	<link rel="shortcut icon" href="public/assets/images/favicon.ico">
	<link rel="shortcut icon" href="public/assets/images/favicon.ico">
	<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
	<script src="{{url('assets/js/jquery.min.js')}}"></script>
</head>
<body >
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MR7J49J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div class="top-header">
		<div class="container">
			<div class="row align-items-top">
				<div class="col-12">
					<div class="header text-center py-2">
						<a href="{{url('/away')}}" class="logo"><img alt="Logo" src="{{url('assets/images/wastetech-logo_04.png')}}" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="page-title" style="background-color:#0a6cb3;color:#fff;">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center py-2">
					<h1>Payment</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="content">
		<div class="container">
			<div class="row">
				<?php
					/**
					<div class="col-xl-12">
						<div class="breadcrumb-holder">
							<ol class="breadcrumb float-left">
								<li class="breadcrumb-item active"><strong>1. Insert your billing address</strong></li>
								<li class="breadcrumb-item">2. Insert your credit card details</li>
								<li class="breadcrumb-item">3. Finish</li>
							</ol>
							<h1 class="main-title float-right">Payment process steps</h1>
							<div class="clearfix"></div>
						</div>
					</div>
					**/
				?>
			</div>
			<div class="row">
				<div class="col-12 py-1 payment-status">
					<div class="section-content">
						<input type="hidden" id="username" value="{{Config::get('scn.username')}}"/>
						<input type="hidden" id="shared_secret" value="{{Config::get('scn.shared_secret')}}"/>
						<input type="hidden" id="currency" value="{{Config::get('scn.currency')}}"/>
						<input type="hidden" id="reference" value="{{$reference}}"/>
						<input type="hidden" id="r" value="{{$r}}"/>
						<input type="hidden" id="amount" value="{{$amount}}"/>
						<input type="hidden" id="currency" value="{{$id}}"/>
						<input type="hidden" id="token" value="{{$token}}"/>
						<input type="hidden" id="id" value="{{$id}}"/>
						<input type="hidden" id="v" value="{{$v}}"/>
						<input type="hidden" id="successful" value="{{$successful}}"/>
						<input type="hidden" id="card_category" value="{{$card_category}}"/>
						<input type="hidden" id="card_type" value="{{$card_type}}"/>
						<input type="hidden" id="card_holder" value="{{$card_holder}}"/>
						<input type="hidden" id="card_number" value="{{$card_number}}"/>
						<input type="hidden" id="verification" />
					</div>
					<div class="loading">
						<div class="lds-css ng-scope">
							<div style="width:100%;height:100%" class="lds-facebook">
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
						
					</div>
					
					<div class="alert alert-success mr-2 text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
						<h3>Payment success</h3>
						<p>Thank you, we have received your payment. We will process your skip bin hire immediately.</p>
						<br />
						<a href="{{url('/away')}}"><button type="button" class="btn btn-success" >Return to Website</button></a>
						
					</div>
					<div class="alert alert-danger text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
							<h3>Payment error</h3>
							@if ($message = Session::has('error'))
								<p><?php echo Session::get('error');?></p>
							@else
								<p>Our system has detected anomaly in your payment.</p>
							@endif
							
							<br />
						<a href="{{url('/away')}}"><button type="button" class="btn btn-danger" >Return to Website</button></a>
					</div>
				
				</div>
				
				<?php Session::forget('error');?>
			</div>
		</div>
	</div>

	
	
	<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
	<script src="{{url('assets/js/modernizr.min.js')}}"></script>
	<script src="{{url('assets/js/moment.min.js')}}"></script>
	<script src="{{url('assets/js/popper.js')}}"></script>
	<script src="{{url('assets/js/detect.js')}}"></script>
	<script src="{{url('assets/js/fastclick.js')}}"></script>
	<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
	<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
	<script src="{{url('assets/js/pikeadmin.js')}}"></script>
	<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>
	<script src="{{url('assets/plugins/datetimepicker/js/daterangepicker.js')}}"></script>
	<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
	<script src="{{url('assets/js/script.js')}}"></script>
	<script src="{{url('assets/js/md5.js')}}"></script>
	<script src="{{url('assets/js/payment_response.js')}}"></script>
	<script type="text/javascript">
		var url = "<?php echo route('ajax_hash_post')?>";
	</script>
	<script src="{{url('assets/js/ajax_response_verification.js')}}"></script>
	
</body>
</html>
