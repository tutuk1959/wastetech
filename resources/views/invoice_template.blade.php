<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Waste Technology Solutions</title>
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<script src="{{url('assets/js/jquery.min.js')}}"></script>
</head>
<body class="adminbody">
	<div id="main">
		<div class="container">
			@section('invoice_content')
			
			@show
		</div>
	</div>
<!-- END main -->
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/modernizr.min.js')}}"></script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/popper.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/detect.js')}}"></script>
<script src="{{url('assets/js/fastclick.js')}}"></script>
<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('assets/js/pikeadmin.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>\
<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{url('assets/js/script.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>