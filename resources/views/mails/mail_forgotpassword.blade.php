@extends('login_template')
@section('login_content')
		 <style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
		</style>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/wastetech-logo_04.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														Waste Technology Solutions<br/>
														7/48 Prindiville Drive <br />
														Wangara, Australia <br />
														6065 <br />
														(+61) 0429 966 184
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<h3 >Your account password changes</h3><br />
							<p>Hi {{ $name }},</p>
							<p>Your password has been resetted and changed. Please log in to manage your bin rates or your account.</p>
							<p>If you did not request any password changes and you might suspect a third-party involvement, please contact us immediately. </p>
							<p>Thank you ! <br /></p>
							<p>Good Day !</p>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
