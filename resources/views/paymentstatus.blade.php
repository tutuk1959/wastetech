@extends('payment_template')
@section('payment_content')
    <div class="col-12 py-3">
        <div class="section-content">
			@if ($message = Session::get('status_api'))
                <div class="alert alert-warning text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <h1>{!! $message_api !!}</h1>
                        <br />
                </div>
                <?php Session::forget('status_api');?>
            @endif
			
            @if ($message = Session::get('success'))
                <div class="alert alert-success mr-2 text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <h1>{!! $message !!}</h1>
                        <br />
                    <a href="{{url('/away')}}"><button type="button" class="btn btn-success" >Return to Website</button></a>
                </div>
                <?php Session::forget('success');?>
            @endif

            @if ($message = Session::get('error'))
                <div class="alert alert-danger text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <h1>{!! $message !!}</h1>
                        <br />
                    <a href="{{url('/away')}}"><button type="button" class="btn btn-danger" >Return to Website</button></a>
                </div>
                <?php Session::forget('error');?>
            @endif
		
			
        </div>
    </div>
@endsection

@if ($message = Session::get('success'))
    @push('dataLayer')
        
    @endpush
@endif