<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Waste Technology Solutions</title>
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<script src="{{url('assets/js/jquery.min.js')}}"></script>
</head>
<body >
	<input type="hidden" id="username" value="{{Config::get('scn.username')}}"/>
	<input type="hidden" id="shared_secret" value="{{Config::get('scn.shared_secret')}}"/>
	<input type="hidden" id="currency" value="{{Config::get('scn.currency')}}"/>
	<input type="hidden" id="totalprice" value="<?php echo sprintf('%1.2f', $totalprice)?>"/>
	<input type="hidden" id="paymenttemp" value="{{$paymenttemp}}"/>
	<input type="hidden" id="payment" value="{{$payment}}"/>
	<input type="hidden" id="order" value="{{$order}}"/>
	<input type="hidden" id="insert_payment" value="{{$insert_payment}}"/>
	<div class="top-header">
		<div class="container">
			<div class="row align-items-top">
				<div class="col-12">
					<div class="header text-center py-2">
						<a href="{{url('/away')}}" class="logo"><img alt="Logo" src="{{url('assets/images/wastetech-logo_04.png')}}" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="page-title" style="background-color:#0a6cb3;color:#fff;">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center py-2">
					<h1>Payment</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="content">
		<div class="container">
			<div class="row">
				<?php
					/**
					<div class="col-xl-12">
						<div class="breadcrumb-holder">
							<ol class="breadcrumb float-left">
								<li class="breadcrumb-item active"><strong>1. Insert your billing address</strong></li>
								<li class="breadcrumb-item">2. Insert your credit card details</li>
								<li class="breadcrumb-item">3. Finish</li>
							</ol>
							<h1 class="main-title float-right">Payment process steps</h1>
							<div class="clearfix"></div>
						</div>
					</div>
					**/
				?>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="img-wrapper py-2 text-center">
						<img class="m-auto" src="{{url('assets/images/SSL.png')}}" alt="SSL Protected" />
						<p>Payment gateway is secured by SSL</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-4">
					<div class="table-responsive">
						 <table class="table table-striped">
							<thead>
								<tr>
									<td colspan="2"><h5><strong>Billing Details</strong></h5></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="30%"><strong>First name</strong></td>
									<td>
										{{$data->first_name}}
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Last name</strong></td>
									<td>
										{{$data->last_name}}
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Address</strong></td>
									<td>
										{{$data->address}}
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Suburb</strong></td>
									<td>
										{{$data->suburb}}
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Zipcode</strong></td>
									<td>
										{{$data->zipcode}}
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Phone</strong></td>
									<td>
										{{$data->phone}}
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Email</strong></td>
									<td>
										{{$data->email}}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<td colspan="2"><h5><strong>Bin Details</strong></h5></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="30%"><strong>Bin Type</strong></td>
									<td>
										{{$binservice->name}}
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Size</strong></td>
									<td>
										{{$binservice->size}}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="table-responsive">
						 <table class="table table-striped">
							<thead>
								<tr>
									<td colspan="2"><h5><strong>Pricing Details</strong></h5></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="30%"><strong>Note</strong></td>
									<td>   
										<?php if (isset($binhireoptions->extraHireagePrice) && ($binhireoptions->extraHireagePrice > 0) && ($binhireoptions->extraHireageDays > 0)) :?>
											<p><span>Charged $<?php echo sprintf('%1.2f',$binhireoptions->extraHireagePrice )?> per day after <?=$binhireoptions->extraHireageDays?> days hire</span></p>
										<?php endif;?>
											
										<?php if (isset($binhireoptions->excessWeightPrice) && ($binhireoptions->excessWeightPrice > 0)) :?>
											<p><span>First 300kg included in price. Prorata price thereafter $<?=$binhireoptions->excessWeightPrice?> per 100kg. </span></p>
										<?php endif;?>
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Extra days</strong></td>
									<td>
										<?=($exactplusdays > 0) ? $exactplusdays.'<sup>day / days</sup>' : '-'?>
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>Bin hire price</strong></td>
									<td>
										$<?php echo sprintf('%1.2f',$subtotal)?>
									</td>
								</tr>
								
								<tr>
									<td width="30%"><strong>Booking fee</strong></td>
									<td>
										$<?php echo sprintf('%1.2f',$bookingprice->price )?>
									</td>
								</tr>
								<tr>
									<td width="30%"><strong>GST 10% Paid</strong></td>
									<td>
										$<?php 
											echo sprintf('%1.2f',$gst)?>
									</td>
								</tr>
								<tr>
									<td width="30%"><strong class="text-danger">Grand Total</strong></td>
									<td>
										<strong  class="text-danger">$<?php echo sprintf('%1.2f',$totalprice )?></strong>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-12 col-lg-8">
					<div class="section-header text-center">
						<h5><strong>Enter your credit card details</strong> </h5>
					</div>
					<div class="section-content">
						
						<iframe id="paynow" src="about:blank" width="100%" height="500" ></iframe>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	
	
	<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
	<script src="{{url('assets/js/modernizr.min.js')}}"></script>
	<script src="{{url('assets/js/moment.min.js')}}"></script>
	<script src="{{url('assets/js/popper.js')}}"></script>
	<script src="{{url('assets/js/detect.js')}}"></script>
	<script src="{{url('assets/js/fastclick.js')}}"></script>
	<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
	<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
	<script src="{{url('assets/js/pikeadmin.js')}}"></script>
	<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>
	<script src="{{url('assets/plugins/datetimepicker/js/daterangepicker.js')}}"></script>
	<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
	<script src="{{url('assets/js/script.js')}}"></script>
	<script src="{{url('assets/js/md5.js')}}"></script>
	<script src="{{url('assets/js/pynw.js')}}"></script>
	
</body>
</html>