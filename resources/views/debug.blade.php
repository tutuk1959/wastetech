@extends('login_template')
@section('login_content')
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12 col-md-6">
									<a href="{{url('/away')}}"><img src="{{url('assets/images/skipbin-logo_03.png')}}" /></a>
								</div>
								<div class="col-12 col-md-6">
									<h3 class="float-right">Debug Mode</h3>
								</div>
							</div>
							
							
						</div>

						
						<div class="col-12">
							@if (!empty(session('access_violated_status')))
								@if (session('access_violated_status') == 'danger')
									<div class="alert alert-warning" role="alert">
										{{session('access_violated_message')}} <br />
									</div>
								@endif
							@endif
						</div>
						
						<div class="col-12">
							@if($result['status'] != '')
								@if($result['status'] == 'danger')
									<div class="alert alert-danger" role="alert">
										<?php if ($result['message'] > 1):?>
											@foreach($result['message'] as $notif)
												{{$notif}}
											@endforeach
										<?php else: ?>
											{{$result['message']}}
										<?php endif;?>
										
									</div>
								@elseif ($result['status'] == 'success')
									<div class="alert alert-success" role="alert">
										{{$result['message']}}
									</div>
								@elseif ($result['status'] == 'warning')
									<div class="alert alert-danger" role="alert">
										<strong>{{$result['message']}}</strong>
									</div>
								@endif
							@endif

							@if ($errors->has('emailAddress'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('emailAddress') }}
								</div>
							@endif
						</div>
						<div class="col-12 col-lg-9">
							<div class="ezyskipbinpanel card mb-3 mt-3">
								<div class="card-header">
									<strong>Register</strong>
								</div>
								
								<div class="card-body">
									<form action="/registerInterest" method="POST">
										{{csrf_field()}}
										<div class="form-group">
											<label for="username">Username <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" name="username" id="username"  required="" type="text">
										</div>
										<div class="form-group">
											<label for="password">Password <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" name="password" id="password"  required="" type="password">
										</div>
										<div class="form-group">
											<label for="retype_password">Re-type Password <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" name="retype_password" id="retype_password"  required="" type="password">
										</div>
										<div class="form-group">
											<label for="organizationName">Organization Name <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" name="organizationName" id="organizationName" required="" type="text">
										</div>
										<div class="form-group">
											<label for="abn">ABN <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" name="abn" id="abn" required="" type="text">
										</div>
										<div class="form-group">
											<label for="contactName">Contact Name <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" id="contactName"  required="" type="text" name="contactName">
										</div>
										<div class="form-group">
											<label for="daytimePhoneNumber">Daytime Phone Number <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" id="daytimePhoneNumber"  required="" type="text" name="daytimePhoneNumber">
										</div>
										<div class="form-group">
											<label for="mobilePhoneNumber">Mobile Phone Number <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" id="mobilePhoneNumber"  required="" type="text" name="mobilePhoneNumber">
										</div>
										<div class="form-group">
											<label for="emailAddress">Email Address <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" id="emailAddress"  required="" type="text" name="emailAddress">
										</div>
										<div class="form-group">
											<label for="comments">Comments / Question</label>
											<textarea class="form-control"name="comments" id="comments" cols="30" rows="10"></textarea>
										</div>
										<button name="submit" type="submit" class="btn button-yellow">Submit</button>
									</form>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-3">						
							<div class="ezyskipbinpanel card mb-3 mt-3">
								<div class="card-header">
									<strong>Supplier Login</strong>
								</div>
									
								<div class="card-body">
									<form action="/lord_of_all_dragons/loginUser" method="POST">
										{{csrf_field()}}
										<div class="form-group">
											<label for="username">Username</label>
											<input class="form-control" name="username"id="username" aria-describedby="usernameHelp" required="" type="text">
										</div>
										<div class="form-group">
											<label for="email">Email</label>
											<input class="form-control" id="email" required="" type="email" name="email">
										</div>
										<button name="submit" type="submit" class="btn button-yellow">Submit</button>
										<a class="float-right"  href="{{url('/forgotpassword')}}">Forgot password?</a>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
