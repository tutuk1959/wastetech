<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>404 Error - Ezyskips Online Supplier Zone</title>
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<script src="{{url('assets/js/jquery.min.js')}}"></script>
		
</head>
<body >
	<div class="top-header">
		<div class="container">
			<div class="row align-items-top">
				<div class="col-12">
					<div class="header text-center py-2">
						<img src="{{url('assets/images/wastetech-logo_04.png')}}" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="page-title" style="background-color:#0a6cb3;color:#fff;">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center py-2">
					<h1>Sorry, internal server error</h1>
				</div>
			</div>
		</div>
	</div>
<!-- END main -->
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/modernizr.min.js')}}"></script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/popper.js')}}"></script>
<script src="{{url('assets/js/detect.js')}}"></script>
<script src="{{url('assets/js/fastclick.js')}}"></script>
<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('assets/js/pikeadmin.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>
<sc>
<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{url('assets/js/script.js')}}"></script>
</body>
</html>