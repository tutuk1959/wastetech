@extends('payment_template')
@section('payment_content')
	<?php
		/**
		<div class="col-xl-12">
			<div class="breadcrumb-holder">
				<ol class="breadcrumb float-left">
					<li class="breadcrumb-item active"><strong>1. Insert your billing address</strong></li>
					<li class="breadcrumb-item">2. Insert your credit card details</li>
					<li class="breadcrumb-item">3. Finish</li>
				</ol>
				<h1 class="main-title float-right">Payment process steps</h1>
				<div class="clearfix"></div>
			</div>
		</div>
		**/
	?>
	
    <div class="col-12 py-3">
        <div class="section-content">
			@if ($errors->has('amount'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('amount') }}
				</div>
			@endif
			@if ($errors->has('first_name'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('first_name') }}
				</div>
			@endif
			@if ($errors->has('last_name'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('last_name') }}
				</div>
			@endif
            @if ($errors->has('address'))
                <div class="alert alert-danger mr-2" role="alert">
                    {{ $errors->first('address') }}
                </div>
            @endif
			@if ($errors->has('suburb'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('suburb') }}
				</div>
			@endif
			@if ($errors->has('phone'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('phone') }}
				</div>
			@endif
			@if ($errors->has('email'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('email') }}
				</div>
			@endif
			@if ($errors->has('agree'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('agree') }}
				</div>
			@endif
            @if ($errors->has('postal_code'))
                <div class="alert alert-danger mr-2" role="alert">
                    {{ $errors->first('postal_code') }}
                </div>
            @endif
            
            @if ($message = Session::get('success'))
                <div class="alert alert-success mr-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        {!! $message !!}
                </div>
                <?php Session::forget('success');?>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        {!! $message !!}
                </div>
                <?php Session::forget('error');?>
            @endif
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="section-header">
            <h5><strong>Enter your details and shipping details. <sup>* required</sup></strong>  </h5>
        </div>
        <div class="section-content">
            <form id="form_geo" action="{!! URL::route('payment') !!}" method="POST"> 
				{{ csrf_field() }} 
				<input type="hidden" name="idpaymenttemp" value="{{$idpaymenttemp}}"/>
                <input type="hidden" name="amount" value="{{$purchasedetails->price}}">
                <input type="hidden" name="idBinType" value="{{$purchasedetails->idBinType}}">
                <input type="hidden" name="idBinSize" value="{{$purchasedetails->idBinSize}}">
                <input type="hidden" name="zipcode" value="{{$purchasedetails->zipcode}}">
                <input type="hidden" name="deliverydate" value="{{$purchasedetails->deliveryDate}}">
                <input type="hidden" name="collectiondate" value="{{$purchasedetails->collectionDate}}">
                <input type="hidden" name="idSupplier" value="{{$purchasedetails->idSupplier}}">
                <input type="hidden" id="area" name="area" value="{{$servicearea->area}}">
                <input type="hidden" name="postal_code" value="" data-geo="postal_code">

                <div class="form-row">
                    <div class="form-group col-md-6 mb-3">
                        <label for="first_name">First name <sup>* <?=($errors->has('first_name')) ? '<strong class="text-danger">'.$errors->first('first_name').'</strong>' : ''?></sup></label>
                        <input class="form-control <?=($errors->has('first_name')) ? 'is-invalid' : ''?>" id="first_name" name="first_name" placeholder="First name" required="" type="text" value="<?=($errors->has('first_name')) ? '' : old('first_name')?>">
                    </div>
                    <div class="form-group col-md-6 mb-3">
                        <label for="last_name">Last name <sup>* <?=($errors->has('last_name')) ? '<strong class="text-danger">'.$errors->first('last_name').'</strong>' : ''?></sup></label>
                        <input class="form-control <?=($errors->has('last_name')) ? 'is-invalid' : ''?>" id="last_name" name="last_name" placeholder="Last name" required="" type="text" value="<?=($errors->has('last_name')) ? '' : old('last_name')?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address">Delivery Address <sup>* <?=($errors->has('address')) ? '<strong class="text-danger">'.$errors->first('address').'</strong>' : ''?></sup></label>
                    <input class="text form-control <?=($errors->has('address')) ? 'is-invalid' : ''?>" name="address" id="autocomplete" cols="30" rows="10" required=""  placeholder="Type your delivery Address"/>
                </div>
                <div class="form-group">
                    <label for="suburb">Suburb <sup>* <?=($errors->has('suburb')) ? '<strong class="text-danger">'.$errors->first('suburb').'</strong>' : ''?></sup></label>
                    <input type="text" class="form-control <?=($errors->has('suburb')) ? 'is-invalid' : ''?>" name="suburb" id="locality" required="" placeholder="Suburb" value="" data-geo="locality">
                </div>
                <div class="form-group">
                    <label for="zipcode">Postcode <sup>* <?=($errors->has('zipcode')) ? '<strong class="text-danger">'.$errors->first('zipcode').'</strong>' : ''?></sup></label>
                    <input type="text" class="form-control <?=($errors->has('zipcode')) ? 'is-invalid' : ''?>" name="zipcode" id="postal_code" value="{{$purchasedetails->zipcode}}" readonly>
                </div>
                <div class="form-group">
                    <label for="phone">Phone <sup>* <?=($errors->has('phone')) ? '<strong class="text-danger">'.$errors->first('phone').'</strong>' : ''?></sup></label>
                    <input type="text" class="form-control <?=($errors->has('phone')) ? 'is-invalid' : ''?>" name="phone" id="phone" required="" placeholder="Phone" value="<?=($errors->has('phone')) ? '' : old('phone')?>">
                </div>
                <div class="form-group">
                    <label for="email">Email <sup>* <?=($errors->has('email')) ? '<strong class="text-danger">'.$errors->first('email').'</strong>' : ''?></sup></label>
                    <input type="text" class="form-control <?=($errors->has('email')) ? 'is-invalid' : ''?>" name="email" id="email" required="" placeholder="Email" value="<?=($errors->has('email')) ? '' : old('email')?>">
                </div>
                <div class="form-group">
					<label for="note">Special Note</label>
					<textarea class="form-control" name="note" id="note" cols="30" rows="10" placeholder="Note">{{old('note')}}</textarea>
				</div>
        </div>
    </div>
    @if(!is_null($purchasedetails))
    <div class="col-12 col-md-6">
        <table class="table table-striped">
            <tbody>
				 <tr>
                    <td width="30%"><strong>Note</strong></td>
                    <td>   
						<?php if (isset($binhireoptions->extraHireagePrice) && ($binhireoptions->extraHireagePrice > 0) && ($binhireoptions->extraHireageDays > 0)) :?>
							<p><span>Charged $<?php echo sprintf('%1.2f',$binhireoptions->extraHireagePrice )?> per day after <?=$binhireoptions->extraHireageDays?> days hire</span></p>
						<?php endif;?>
							
						<?php if (isset($binhireoptions->excessWeightPrice) && ($binhireoptions->excessWeightPrice > 0)) :?>
							<p><span>First 300kg included in price. Prorata price thereafter $<?=$binhireoptions->excessWeightPrice?> per 1000kg. </span></p>
						<?php endif;?>
					</td>
				</tr>
                <tr>
                    <td width="30%"><strong>Days</strong></td>
                    <td>
						{{$daysmargin}} (Day / Days) <br />(<?=date('l d-m-Y', strtotime($purchasedetails->deliveryDate))?> - <?=date('l d-m-Y', strtotime($purchasedetails->collectionDate))?>)
                    </td>
                </tr>
				<tr>
                    <td width="30%"><strong>Extra days</strong></td>
                    <td>
						<?=($exactplusdays > 0) ? $exactplusdays.'<sup>day / days</sup>' : '-'?>
                    </td>
                </tr>
				<tr>
					<td width="30%"><strong>Bin hire price</strong></td>
					<td>
						$<?php echo sprintf('%1.2f',$totalprice )?>
					</td>
				</tr>
				<tr>
					<td width="30%"><strong>Booking fee</strong></td>
					<td>
						$<?php echo sprintf('%1.2f',$bookingprice->price )?>
						<input type="hidden" name="bookingfee" value="{{$bookingprice->price}}"/>
						<input type="hidden" name="subtotal" value="{{$totalprice}}"/>
					</td>
				</tr>
				<tr>
					<td width="30%"><strong>GST 10% Paid</strong></td>
					<td>
						$<?php 
							echo sprintf('%1.2f',$gst)?>
					</td>
				</tr>
				<input type="hidden" name="gst" value="{{$gst}}"/>
				<tr>
					<td width="30%"><strong class="text-danger">Grand Total</strong></td>
					<td>
						<strong  class="text-danger">$<?php echo sprintf('%1.2f',$grandtotal )?></strong>
						<input type="hidden" name="totalprice" value="{{$grandtotal}}"/>
					</td>
				</tr>
            </tbody>
        </table>
        <h5><strong>Check this box to proceed</strong></h5>
        <strong>Before submitting your bin hire request, please make sure you read the <a href="#">terms and conditions</a> carefully.</strong>
		<div class="form-check checkout">

			<input type="checkbox" name="agree" id="agree" />
			<label for="agree">I agree with Waste Technology Solution <a href="#">terms and conditions</a></label>
        </div>
		<button type="submit" name="submit" value="Submit" id="submit" class="btn btn-success float-md-right">Checkout</button>
		</form>
    </div>
    
    @endif
    
@endsection
