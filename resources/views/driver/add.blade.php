@extends('template')
@section('content')
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Drivers </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item"><a href="{{url('drivers')}}">Drivers</a></li>
									<li class="breadcrumb-item active">Add Driver</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Add Driver</strong>
								</div>
								
								<div class="card-body">
									<form action="{{url('add_drivers')}}" method="post">
										{{csrf_field()}}
										<div class="form-row">
											<div class="col-12 col-6">
												<label for="name">Driver's Name</label>
												<input id="name" type="text" name="name" class="form-control" placeholder="Input driver's name" />
											</div>
											<div class="col-12 col-6">
												<label for="license">Driver's License</label>
												<input id="license" type="text" name="license" class="form-control" placeholder="Input driver's license" />
											</div>
										</div>
										<div class="form-row">
											<div class="col">
												<label for="username">Username</label>
												<input id="username" type="text" name="username" class="form-control" placeholder="Input driver's username" />
											</div>
											<div class="col">
												<label for="password">Password</label>
												<input id="password" type="password" name="password" class="form-control" placeholder="Input driver's password" />
											</div>
											<div class="col">
												<label for="confirm_password">Confirm Password</label>
												<input id="confirm_password" type="password" name="confirm_password" class="form-control" placeholder="Retype password" />
											</div>
										</div>
										<div class="form-row">
											<div class="col">
												<label for="email_address">Email address</label>
												<input id="email_address" type="text" name="email_address" class="form-control" placeholder="Input driver's email address" />
											</div>
											
										</div>
										<button type="submit" class="btn btn-primary mt-3">Save</button>
									</form>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
@endsection
