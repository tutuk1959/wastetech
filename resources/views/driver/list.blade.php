@extends('template')
@section('content')
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Drivers </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Drivers</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					@if(!is_null($drivers) && ($drivers->count() > 0))
						<div class="row">
							<div class="col-12">
								<div class="card mb-3 mt-2">
									<div class="card-header">
										<strong>Drivers List</strong>
										<a href="{{url('add_drivers')}}" class="btn btn-primary ml-3">Add Driver</a>
									</div>
									
									<div class="card-body">
										<div class="table-responsive">
											<table class="table table-striped">
												<thead>
													<tr>
														<td>#</td>
														<td>Drivers</td>
														<td>License</td>
														<td>On Duty?</td>
														<td></td>
													</tr>
												</thead>
												<tbody>
													<?php $i = 1;?>
													@foreach($drivers as $driver)
														<tr>
															<td>{{$i}}</td>
															<td>{{$driver->name}}</td>
															<td>
																@if(!is_null($driver->licence)&& ($driver->licence <> ''))
																	
																	{{$driver->licence}}
																@else
																	-
																@endif
															</td>
															<td>
																@if(!is_null($driver->onDuty))
																	@if($driver->onDuty == 1)
																		{{'Yes'}}
																	@else
																		{{'Idle'}}
																	@endif
																	
																@else
																	{{'Idle'}}
																@endif
															</td>
															<td>
																<a href="{{url('update_drivers')}}/{{$driver->id}}" 
																class="btn btn-warning mr-3"><i class="fa fa-pencil"></i></a>
																<a href="{{url('driver_details')}}/{{$driver->id}}" class="btn btn-primary mr-3"><i class="fa fa-id-card"></i></a>
															</td>
														</tr>
														<?php $i++?>
													@endforeach
												</tbody>
											</table>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					@else
						<div class="row">
							<div class="col-12">
								<p>No drivers data.</p>
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>
@endsection
