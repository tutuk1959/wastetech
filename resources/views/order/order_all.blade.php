@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Orders </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">List</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('selectsupplier'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('selectsupplier') }}
								</div>
							@endif
							@if ($errors->has('selectstatus'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('selectstatus') }}
								</div>
							@endif
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Sort order based on Date</strong>
									<a href="{{url('/add_orders')}}" class="btn btn-primary ml-3" ><i class="fa fa-plus"></i> Add New</a>
								</div>
									
								<div class="card-body">
									<form action="/orders" method="post" class="form-inline">
										{{csrf_field()}}
										<label class="mr-3" for="selectsupplier">Choose Date</label>
											<?php /*<input type="text" class="form-control mr-3" name="order_management_datepicker" width="30"> */ ?>
											<input type="text" class="form-control mr-3" name="fromdate" width="30">-
											<input type="text" class="form-control ml-3 mr-3" name="todate" width="30">
											<input type="hidden" name="start_order_date" value="">
											<input type="hidden" name="end_order_date" value="">
  										<button type="submit" class="btn btn-primary">See Supplies</button>
									</form>
								</div>
							</div><!-- end card-->
						</div>
					</div>
					
					@if(!is_null($suppliesdata))
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Orders Supplies</strong>
								</div>
									
								<div class="card-body">
									<div class="table-responsive">
										<table class="order-table table table-bordered" style="border:none;">
											<thead class="text-center">
												<th>Type</th>
												<th>Order Ref</th>
												<th>Order Date</th>
												<th>Project Sites</th>
												<th>Organization</th>
												<th>Order Status</th>
												<th>More action</th>
											</thead>
											<tbody>
												
												@foreach($suppliesdata as $data)
													<tr>
														<td>
															@if($data->is_quote == 1)
																{{'QUOTE'}}
															@elseif($data->is_quote == 0)
																{{'ORDER'}}
															@else
																{{'ORDER'}}
															@endif
														</td>
														<td><a href="{{ url('/') }}/orders_details/{{$data->paymentUniqueCode}}/{{$data->idOrderService}}">{{$data->paymentUniqueCode}}</a></td>
														<td>{{date('d/m/Y', strtotime($data->orderDate))}}</td>
														<td>{{$data->sitename}}</td>
														<td>{{$data->organization_name}}</td>
														<td>
															@if(!is_null($data->is_paid))
																@if($data->is_paid == 1)
																	{{'Paid'}}
																@elseif($data->is_paid == 0)
																	{{'Unpaid'}}
																@endif
															@endif
														</td>
														
														<td>
															<?php /* <a href="{{url('/update_orders/')}}/{{$data->paymentUniqueCode}}/{{$data->idOrderService}}" class="btn btn-primary mr-2"><i class="fa fa-pencil"></i></a> */ ?>
															<a href="{{url('/orders_details/')}}/{{$data->paymentUniqueCode}}/{{$data->idOrderService}}" class="btn btn-warning mr-2"><i class="fa fa-search"></i></a>
														</td>
													</tr>
												@endforeach
											</tbody>
										</table>		
									</div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
@endsection
