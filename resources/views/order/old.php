@extends('template')
@section('content')
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Orders </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item">List</li>
									<li class="breadcrumb-item active">Add Orders</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Add Orders</strong>
									<a href="{{url('/add_orders')}}" class="btn btn-primary ml-3" ><i class="fa fa-plus"></i> Add New</a>
								</div>
									
								<div class="card-body">
									<form action="/add_orders" method="post" class="add_order">
										{{csrf_field()}}
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="unique_id">Unique Id</label>
												<input type="text" name="unique_id" class="form-control" id="unique_id" value="{{$uniqueid}}" readonly >
											</div>
											<div class="form-group col-md-6">
												<label for="date">Date</label>
												<input type="text" name="date" class="form-control form-datepicker" id="date" placeholder="Select date">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-group col-md-12">
												<label for="supplier">Select bin supplier</label>
												<select name="supplier" id="supplier" class="form-control">
													<option value="">Select supplier</option>
													@if(!is_null($suppliers))
														@foreach($suppliers as $supplier)
															<option value="{{$supplier->idSupplier}}" <?=(old('supplier') == $supplier->idSupplier) ? 'selected' : ''?> >{{$supplier->name}}</option>
														@endforeach
													@endif
												</select>
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="organization">Select customer's organization</label>
												<select name="organization" id="organization" class="form-control">
													<option value="">Select organization</option>
													@if(!is_null($organizations))
														@foreach($organizations as $organization)
															<option value="{{$organization->id}}" <?=(old('organization') == $organization->id) ? 'selected' : ''?> >{{$organization->organization_name}}</option>
														@endforeach
													@endif
												</select>
											</div>
											
											<div class="form-group col-md-6">
												<label for="project_site">Select project site</label>
												<select name="project_site" id="project_site" class="form-control">
													<option value="">Select project site</option>
													@if(!is_null($project_sites))
														@foreach($project_sites as $project_site)
															<option value="{{$organization->id}}" <?=(old('project_site') == $project_site->id) ? 'selected' : ''?> > {{$project_site->sitename}} </option>
														@endforeach
													@endif
												</select>
											</div>
										</div>
										
										
										
										<hr />
										<strong>Items List</strong> <br />
										<button id="add_more" class="btn btn-primary my-3 mr-3" type="button">Add more items</button>
										<button id="delete_more" class="btn btn-danger my-3" type="button"  >Delete items</button>
										<div id="bin_items" class="container">
											<div class="row mt-3 row_items">
												<div class="col col-md-2">
													<div class="form-group">
														<label for="wastetype">Waste Type</label>
														<select name="wastetype[]" id="wastetype" class="form-control form-control-sm">
															<option value="">Select bin type</option>
															@if(!is_null($project_sites))
																@foreach($waste_type as $type)
																	<option value="{{$type->idBinType}}" <?=(old('wastetype') == $type->idBinType) ? 'selected' : ''?> > {{$type->name}} </option>
																@endforeach
															@endif
														</select>
													</div>
												</div>
												
												<div class="col col-md-2">
													<div class="form-group">
														<label for="binsize">Bin Size</label>
														<select name="binsize[]" id="binsize" class="form-control form-control-sm">
															<option value="">Select bin size</option>
															@if(!is_null($binsize))
																@foreach($binsize as $size)
																	<option value="{{$size->idSize}}" <?=(old('binsize') == $size->idSize) ? 'selected' : ''?> > {{$size->size}} </option>
																@endforeach
															@endif
														</select>
													</div>
												</div>
												
												<div class="col col-md-1">
													<div class="form-group">
														<label for="qty">Quantity</label>
														<input type="number" class="form-control form-control-sm" name="qty[]" id="qty" min="1" max="999" value="1" />
													</div>
												</div>
												
												<div class="col col-md-3">
													<div class="form-group">
														<label for="address">Address</label>
														<select class="form-control form-control-sm address" name="address[]" id="address">
															<option value="0">Select your address</option>
															@foreach($address as $add)
																<option value="{{$add->iddelivery_address}}">{{$add->street_name}} - {{$add->unit_number}} - {{$add->post_code}}</option>
															@endforeach
														</select>
													</div>
												</div>
												
												<div class="col col-md-2">
													<div class="form-group">
														<label for="price">Price</label>
														<input type="hidden" name="idbinservice[]" id="idbinservice"/>
														<input type="text" name="price[]" class="form-control form-control-sm" id="price" placeholder="" readonly />
													</div>
												</div>
												
												<div class="col col-md-2">
													<div class="form-group">
														<label for="subtotal">Sub Total</label>
														<input type="text" name="subtotal[]" class="form-control form-control-sm" id="subtotal" placeholder="" readonly />
													</div>
												</div>
												
												<div class="col col-md-6">
													<div class="form-group">
														<label for="delivery_date">Delivery Date</label>
														<input type="text" name="delivery_date[]" class="form-control form-datepicker" id="delivery_date" placeholder="Select delivery date">
													</div>
												</div>
												
												<div class="col col-md-6">
													<div class="form-group">
														<label for="collection_date">Collection Date</label>
														<input type="text" name="collection_date[]" class="form-control form-datepicker-collection" id="collection_date" placeholder="Select collection date">
													</div>
												</div>
												
												<div class="col col-md-12">
													<label for="delivery_comments">Delivery Comments</label>
													<textarea class="form-control" name="delivery_comments[]" id="delivery_comments" cols="30" rows="10"></textarea>
												</div>
												<hr />
											</div>
											
										</div>
										
										<div class="container">
											<div class="row">
												<div class="col-12 text-right">
													<input type="hidden" name="all_total" value="0"/>
													Total : <strong class="total"></strong> <br /> 
													<button type="submit" class="btn btn-primary mt-4">Save</button>
												</div>
											</div>
										</div>
										
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
