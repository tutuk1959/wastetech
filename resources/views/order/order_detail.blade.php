@extends('login_template')
@section('login_content')
		 <style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
			
			.bin_desc ul li{
				color:#c40005;
			}
			ul li{
				padding-left:0;
			}
		</style>
		<?php use \App\Http\Controllers\order\OrderController; ?>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/wastetech-logo_04.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														Waste Technology Solutions<br/>
														7/48 Prindiville Drive <br />
														Wangara, Australia <br />
														6065 <br />
														(+61) 0429 966 184
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="card mb-3">
								<div class="card-body">	
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">
													<table class="table" border="0" width="100%" cellpadding="3">
														<tr>
															<td style="text-align:center">
																<h3 style="text-transform:uppercase; ">Order slip</h3><br />
																<strong style="text-transform:uppercase;">Invoice no. {{$order_detail->paymentUniqueCode}}</strong><br />
																
																<strong style="text-transform:uppercase;">Date: <?=date('l d-m-Y', strtotime($order_detail->orderDate));?></strong>
															</td>
														</tr>
													</table>
												</div>

												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">1. Order details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Bin type</strong>
															</td>
															<td style="text-transform:uppercase">
																<?php $i = 1;?>
																@if(!is_null($orderitem))
																	@foreach($orderitem as $item)
																		<?php $ot = OrderController::orders_item_id($item->id);?>
																		@if(!is_null($ot))
																			{{$i}} . {{$ot->bintype_name}} - {{$ot->size}} <br />
																		@endif
																		<?php $i++;?>
																	@endforeach
																@else
																	{{'No order items selected'}}
																@endif
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Description</strong>
															</td>
															<td style="text-transform:uppercase" >
																<?php $i = 1;?>
																@if(!is_null($orderitem))
																	@foreach($orderitem as $item)
																		<?php $ot = OrderController::orders_item_id($item->id);?>
																		@if(!is_null($ot))
																			{{$i}} . <?php $tags = array("strong", "b");?>
																					<p><?php echo preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $ot->bintype_description_2);?></p><br />
																					<div style="color:#c40005;" class="bin_desc"><?php echo $ot->bintype_description; ?></div>
																		@endif
																		<?php $i++;?>
																	@endforeach
																@else
																	{{'No order items selected'}}
																@endif
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Delivery Date</strong>
															</td>
															<td style="text-transform:uppercase">
																<?php $i = 1;?>
																@if(!is_null($orderitem))
																	@foreach($orderitem as $item)
																		<?php $ot = OrderController::orders_item_id($item->id);?>
																		@if(!is_null($orderitem))
																			{{$i}} . <?=date('l d-m-Y', strtotime($ot->delivery_date));?> <br />
																		@endif
																		<?php $i++;?>
																	@endforeach
																@else
																	{{'No order items selected'}}
																@endif
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Collection Date</strong>
															</td>
															<td style="text-transform:uppercase">
																<?php $i = 1;?>
																@if(!is_null($orderitem))
																	@foreach($orderitem as $item)
																		<?php $ot = OrderController::orders_item_id($item->id);?>
																		@if(!is_null($ot))
																			{{$i}} . <?=date('l d-m-Y', strtotime($ot->collection_date));?> <br />
																		@endif
																		<?php $i++;?>
																	@endforeach
																@else
																	{{'No order items selected'}}
																@endif
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Delivery Address</strong>
															</td>
															<td style="text-transform:uppercase">
																<?php $i = 1;?>
																@if(!is_null($orderitem))
																	@foreach($orderitem as $item)
																		<?php $ot = OrderController::orders_item_id($item->id);?>
																		@if(!is_null($ot))
																			{{$i}} . {{$ot->street_name}} - Unit Number: {{$ot->unit_number}} - Lot Number: {{$ot->lot_number}} - {{$ot->suburb}} - {{$ot->post_code}} - {{$ot->state}} <br />
																		@endif
																		<?php $i++;?>
																	@endforeach
																@else
																	{{'No order items selected'}}
																@endif
															</td>
														</tr>
														
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Customer additional note</strong>
															</td>
															<td style="text-transform:uppercase">
																<?php $i = 1;?>
																@if(!is_null($orderitem))
																	@foreach($orderitem as $item)
																		<?php $ot = OrderController::orders_item_id($item->id);?>
																		@if(!is_null($ot) && !is_null($ot->delivery_comments))
																			{{$i}}. {{$ot->delivery_comments}} <br />
																		@else
																			{{$i}}. {{'No specified message'}} <br />
																		@endif
																		<?php $i++;?>
																	@endforeach
																@else
																	{{'No order items selected'}}
																@endif
															</td>
														</tr>
														
													</table>
												</div>
												
												
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">2. Customer delivery details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Organization Name</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$organization->organization_name}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Address</strong>
															</td>
															<td style="text-transform:uppercase" >
																{{$organization->address}}
															</td>
														</tr>
												
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Phone</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$organization->phone}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Email</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$organization->email_address}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Cash Customer Only</strong>
															</td>
															<td style="text-transform:uppercase">
																
																@if(!is_null($organization->cashonly) && $organization->cashonly == '1' )
																	{{'Yes'}}
																@else
																	{{'No'}}
																@endif
															</td>
														</tr>
													</table>
												</div>
												
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">3. Payment confirmation</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Subtotal</strong>
															</td>
															<td style="text-transform:uppercase">
																<strong>${{sprintf('%1.2f',$order_detail->subtotal)}}</strong>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Booking Fee</strong>
															</td>
															<td style="text-transform:uppercase">
																<strong>${{sprintf('%1.2f',$order_detail->bookingfee)}}</strong>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">GST</strong>
															</td>
															<td style="text-transform:uppercase">
																<strong>${{sprintf('%1.2f',$order_detail->gst)}}</strong>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Grand Total</strong>
															</td>
															<td style="text-transform:uppercase">
																<strong>${{sprintf('%1.2f',$order_detail->totalServiceCharge)}}</strong>
															</td>
														</tr>
														@if(!is_null($order_detail->card_category))
															<tr>
																<td width="30%">
																	<strong style="text-transform:uppercase;">Card category</strong>
																</td>
																<td style="text-transform:uppercase">
																	{{$order_detail->card_category}}
																</td>
															</tr>
														@endif
														@if(!is_null($order_detail->card_type))
															<tr>
																<td width="30%">
																	<strong style="text-transform:uppercase;">Card type</strong>
																</td>
																<td style="text-transform:uppercase">
																	{{$order_detail->card_type}}
																</td>
															</tr>
														@endif
														@if(!is_null($order_detail->card_holder))
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Card holder</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$order_detail->card_holder}}
															</td>
														</tr>
														@endif
														@if(!is_null($order_detail->card_number))
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Card number</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$order_detail->card_number}}
															</td>
														</tr>
														@endif
													</table>
												</div>
												
											</div>
										</div>
									</div>	
								</div>														
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection