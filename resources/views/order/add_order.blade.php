@extends('template')
@section('content')
		<style type="text/css">
			.loader-default:after{
				position: absolute;
			}
			.loader-default{
				position: absolute;
			}
			.loader{
				z-index:1;
			}
		</style>
		<div class="content-page ">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Orders </h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item">List</li>
									<li class="breadcrumb-item active">Add Orders</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Add Orders</strong>
									<a href="{{url('/add_orders')}}" class="btn btn-primary ml-3" ><i class="fa fa-plus"></i> Add New</a>
								</div>
									
								<div class="card-body">
									<div class="row">
										<div class="col-12 col-md-7 position-relative">
											
											<div class="form-row">
												<div class="col">
													<label for="uniqueid">Unique ID</label>
													<input id="date" type="text" name="uniqueid" class="form-control" value="<?=$uniqueid?>" readonly />
												</div>
												<div class="col">
													<label for="date">Date</label>
													<input id="date" type="text" name="date" class="form-control form-datepicker" value=""/>
												</div>
												
											</div>
											<div class="form-row">
												<div class="col">
													<label for="binsize">Bin Size</label>
													
													<select name="binsize" id="binsize" class="form-control form-control-sm">
														<option value="">Select bin size</option>
														@if(!is_null($binsize))
															@foreach($binsize as $size)
																<option value="{{$size->idSize}}" <?=(old('binsize') == $size->idSize) ? 'selected' : ''?> > {{$size->size}} </option>
															@endforeach
														@endif
													</select>
												</div>
											</div>
											<div class="form-row">
												<div class="col">
													<label for="street_number">Street Number</label>
													<input id="street_number" type="text" name="street_number" class="form-control" value=""/>
												</div>
												<div class="col">
													<label for="street_name">Street Name</label>
													<input id="street_name" type="text" name="street_name" class="form-control" value=""/>
												</div>
											</div>
											<div class="form-row">
												<div class="col">
													<label for="postcode">Postcode</label>
													<input id="postcode" type="text" name="postcode" class="form-control" value=""/>
												</div>
												<div class="col">
													<label for="suburb">Suburb</label>
													<input id="suburb" type="text" name="suburb" class="form-control" value=""/>
												</div>
											</div>
											
											<div class="form-row">
												<div class="col">
													<label for="delivery_date">Delivery Date</label>
													<input type="text" name="delivery_date" class="form-control form-datepicker-delivery" id="delivery_date" />
												</div>
												<div class="col">
													<label for="collection_date">Collection Date</label>
													<input type="text" name="collection_date" class="form-control form-datepicker-collection" id="collection_date" />
												</div>
											</div>
											<div class="form-row">
												<div class="col">
													<button type="button" id="price" class="my-3 btn btn-primary btn-sm">Find Price</button>
												</div>
											</div>
											<div class="form-row">
												<div class="col">
													<label for="price">Price</label>
													<input type="hidden" name="idbinservice" />
													<input type="hidden" name="isupdate" />
													<input id="price" type="text" name="price" class="form-control" value=""/>
												</div>
												
											</div>
											<div class="form-group">
												<label for="delivery_comments">Delivery Instructions</label>
												<textarea class="form-control" name="delivery_comments" id="delivery_comments" cols="30" rows="3"></textarea>
											</div>
											
											<input type="hidden" name="ordermain" value=""/>
											
											<div id="loader" class="loader1 loader loader-default is-active"></div>
										</div>
										
										<div class="col-12 col-md-5">
											<div class="initial_step p-3 border border-primary position-relative" >
												<div class="form-row align-items-end">
													<div class="col">
														<input type="hidden" name="idorg" value="" />
														<label for="phone">Phone</label>
														<input id="phone" type="text" name="phone" class="form-control" value=""/>
													</div>
													<div class="col">
														<button type="submit" id="search_by_phone" class="btn btn-primary mr-3">Search</button>
													</div>
												</div>
												<div class="form-group">
													<label for="sites">Sites</label>
													<input type="hidden" name="iddelivery_address" value="" />
													<select name="sites" id="sites" class="form-control form-control-sm">
														
													</select>
												</div>
												<button type="button" id="use_site" class="btn btn-primary btn-sm mr-3">Use Site</button>
												<?php /* <button type="button" id="edit_site" class="btn btn-primary btn-sm mr-3">Edit Site</button>
												*/?>
												<div class="form-group">
													<input type="hidden" name="idorderitems" val="" />
													<input type="hidden" name="idorderservice" val="" />
													<label for="quote">Orders / Quotes</label>
													<select name="quote" id="quote" class="form-control form-control-sm">
														
													</select>
												</div>
												<button type="button" id="load" class="btn btn-primary btn-sm mr-3">Load</button>
												<button type="button" id="copy_to_order" class="btn btn-primary btn-sm mr-3">Copy to Order</button>
												 <div  class="loader2 loader loader-default is-active"></div>
											</div>
											<div class="form-row">
												<div class="col">
													<label for="client_name">Clients Name <small></small></label>
													<input type="text" name="client_name" class="form-control" id="client_name" />
												</div>
												<div class="col">
													<label for="bill_address">Billing Address </label>
													<input type="text" name="bill_address" class="form-control" id="bill_address" />
													<div class="form-check">
														<input class="form-check-input" type="checkbox" value="" name="use_as_delivery_address" id="use_as_delivery_address">
														<label class="form-check-label" for="use_as_delivery_address">
															Use this as delivery address
														</label>
													</div>
												</div>
											</div>
											<div class="form-row">
												<div class="col">
													<label for="email_address">Email Address</label>
													<input type="text" name="email_address" class="form-control" id="email_address" />
												</div>
												<div class="col">
													<label for="work_phone">Work Phone</label>
													<input type="text" name="work_phone" class="form-control" id="work_phone" />
												</div>
											</div>
											<button type="button" id="convert_to_order" class="btn btn-primary mt-3 mr-3" disabled >Convert to Order</button>
											<button type="button" id="new_quote" class="btn btn-primary  mt-3 mr-3" disabled >New Quote</button>
										</div>
									</div>
									
									<!-- Invoice -->
									<div id="invoice_section" class="row mt-3">
										<div class="col-12">
											<div class="table-responsive">
												<table class="table table-bordered">
													<tbody>
														<input type="hidden" name="org_payment" />
														
														<input type="hidden" name="ordermain_payment" />
														<tr>
															<td>
																Invoice ID <br />
																<span id="invoice_id"></span>
															</td>
															<td>
																Date <br />
																<span id="order_date"></span>
															</td>
														</tr>
														<tr>
															<td>
																Client's name <br />
																<span id="clients_name"></span>
															</td>
															<td>
																Billing Address <br />
																<span id="payment_bill_address"></span>
															</td>
														</tr>
														<tr>
															<td>
																Email Address <br />
																<span id="payment_email_address"></span>
															</td>
															<td>
																Work Phone <br />
																<span id="payment_work_phone"></span>
															</td>
														</tr>
													</tbody>
												</table>
												
												<table class="table table-bordered items_table">
													<tbody class="items_wrapper">
														<tr>
															<input type="hidden" name="orderitemsid_payment[]" />
															<td>
																Jobs Id <br />
																<span id="jobs_id"></span>
															</td>
															<td>
																Delivery date <br />
																<span id="delivery_date"></span>
															</td>
															<td>
																Collection date <br />
																<span id="collection"></span>
															</td>
															<td>
																Delivery Address <br />
																<span id="delivery_address"></span>
															</td>
															<td>
																Bin Size <br />
																<span id="binsize"></span>
															</td>
															<input type="hidden" name="price[]" />
														</tr>
													</tbody>
												</table>
												
												<table class="table table-bordered">
													<tbody>
														<tr>
															<td align="right">
																Subtotal
															</td>
															<td>
																$<span id="subtotal"></span>
															</td>
														</tr>
														
														<tr>
															<td align="right">
																Booking price
															</td>
															<td>
																$<span id="bookingprice"></span>
															</td>
														</tr>
														
														<tr>
															<td align="right">
																GST
															</td>
															<td>
																$<span id="gst"></span>
															</td>
														</tr>
														
														<tr>
															<td align="right">
																Total
															</td>
															<td>
																$<span id="total_charge"></span>
															</td>
														</tr>
														
														<tr>
															<td colspan="2">
																<button class="btn btn-primary btn-sm" id="take_payment" type="submit" disabled >
																	Payments / Save
																</button>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
@endsection
