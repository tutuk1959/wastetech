@extends('login_template')
@section('login_content')
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12 col-md-6">
									<a href="{{url('/away')}}"><img src="{{url('assets/images/skipbin-logo_03.png')}}" /></a>
								</div>
								<div class="col-12 col-md-6">
									<h3 class="float-right">Supplier Zone</h3>
								</div>
							</div>
							
							
						</div>

						<div class="col-12">
							
							<p>Please insert the following fields to reset your password and change it. By doing this, you would be able to login and manage your bin rates on <a href="{{url('/away')}}">Ezyskips Online</a>.</p>
						</div>
						
						<div class="col-12">
							@if (!empty(session('access_violated_status')))
								@if (session('access_violated_status') == 'danger')
									<div class="alert alert-warning" role="alert">
										{{session('access_violated_message')}} <br />
									</div>
								@endif
							@endif
						</div>
						
						<div class="col-12">
							@if($result['status'] != '')
								@if($result['status'] == 'danger')
									<div class="alert alert-danger" role="alert">
										<?php if ($result['message'] > 1):?>
											@foreach($result['message'] as $notif)
												{{$notif}}
											@endforeach
										<?php else: ?>
											{{$result['message']}}
										<?php endif;?>
										
									</div>
								@elseif ($result['status'] == 'success')
									<div class="alert alert-success" role="alert">
										{{$result['message']}}
									</div>
								@elseif ($result['status'] == 'warning')
									<div class="alert alert-warning" role="alert">
										{{$result['message']}}
									</div>
								@endif
							@endif

							@if ($errors->has('retype_password'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('retype_password') }}
								</div>
							@endif
							
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
						</div>
						<div class="col-12 col-lg-9">
							<div class="ezyskipbinpanel card mb-3 mt-3">
								<div class="card-header">
									<strong>Forgot Password</strong>
								</div>
									
								<div class="card-body">
									<form action="/forgot_password_submit" method="POST">
										{{csrf_field()}}
										<div class="form-group">
											<label for="username">Username <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" name="username" id="username"  required="" type="text">
										</div>
										<div class="form-group">
											<label for="username">Your email <sup style="color:#ca0202">* Required </sup> <sub style="color:#ca0202"> ** Please use email 1 that you have registered on the system</sub></label>
											<input class="form-control" name="email"id="en" aria-describedby="email" required="" type="text">
										</div>
										<div class="form-group">
											<label for="password">New Password <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" name="password" id="password"  required="" type="password">
										</div>
										<div class="form-group">
											<label for="retype_password">Re-type New Password <sup style="color:#ca0202">* Required</sup></label>
											<input class="form-control" name="retype_password" id="retype_password"  required="" type="password">
										</div>
										<button name="submit" type="submit" class="btn button-yellow">Submit</button>
									</form>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-3">						
							<div class="ezyskipbinpanel card mb-3 mt-3">
								<div class="card-header">
									<strong>Supplier Login</strong>
								</div>
									
								<div class="card-body">
									<form action="/loginUser" method="POST">
										{{csrf_field()}}
										
										<div class="form-group">
											<label for="username">Username</label>
											<input class="form-control" name="username"id="username" aria-describedby="usernameHelp" required="" type="text">
										</div>
										<div class="form-group">
											<label for="password">Password</label>
											<input class="form-control" id="password" required="" type="password" name="password">
										</div>
										<button name="submit" type="submit" class="btn button-yellow">Submit</button>
										<a class="float-right" href="{{url('/forgotpassword')}}">Forgot password?</a>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
