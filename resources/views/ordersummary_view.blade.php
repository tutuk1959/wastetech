@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Order Summary</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Order Summary</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('selectsupplier'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('selectsupplier') }}
								</div>
							@endif
							@if ($errors->has('selectstatus'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('selectstatus') }}
								</div>
							@endif
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Order Supplies per Supplier</strong>
								</div>
									
								<div class="card-body">
									<form action="/see_supplies_by_supplier_summary" method="post" class="form-inline">
										{{csrf_field()}}
  										<label class="mr-3" for="selectsupplier">Choose Supplier</label>
  											<select name="selectsupplier" id="selectsupplier" class="form-control mr-3">
												
  												<option value="#">Scroll down</option>
  												@foreach($supplierdata as $supplier)
													@if(!is_null($selected_supplierdata))
														<option value="{{$supplier->idSupplier}}" <?=($selected_supplierdata->idSupplier == $supplier->idSupplier) ? 'selected' : '';?>>{{$supplier->name}}</option>
													@else
														<option value="{{$supplier->idSupplier}}" >{{$supplier->name}}</option>
													@endif
  												@endforeach
  											</select>
  										
										<label class="mr-3" for="selectsupplier">Choose Date</label>
											<?php /*<input type="text" class="form-control mr-3" name="order_management_datepicker" width="30"> */ ?>
											<input type="text" class="form-control mr-3" name="fromdate" width="30">-
											<input type="text" class="form-control ml-3 mr-3" name="todate" width="30">
											<input type="hidden" name="start_order_date" value="">
											<input type="hidden" name="end_order_date" value="">
  										<button type="submit" class="btn btn-primary">See Supplies</button>
									</form>
								</div>
							</div><!-- end card-->
						</div>
					</div>
					
					@if(!is_null($suppliesdata))
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									@if(!is_null($actualStartDate) && !is_null($actualEndDate))
										<strong>Order Supplies for {{$selected_supplierdata->name}} , {{$actualStartDate}} to {{$actualEndDate}}</strong>
									@else
										<strong>Order Supplies for {{$selected_supplierdata->name}}</strong>
									@endif
								</div>
									
								<div class="card-body">
									<div class="table-responsive">
									
										<table class="table table-bordered" style="border:none;">
											<thead class="text-center">
												<th><input type="checkbox" name="ordersummary_checkbox_all" value="1"/></th>
												<th>Order Ref</th>
												<th>Order Date</th>
												<th>Bin Type</th>
												<th>Bin Size</th>
												<th>Customer Name</th>
												<!--<th>Customer Address</th>-->
												<th>Delivery Date</th>
												<th>Collection Date</th>
												<th>Revenue</th>
											</thead>
											<tbody>
												<?php $sum = 0;$sub= 0;?>
												@foreach($suppliesdata as $data)
													<tr>
														<td>
															<input type="hidden" name="startDate" value="{{$actualStartDate}}"/>
															<input type="hidden" name="endDate" value="{{$actualEndDate}}"/>
															<input type="hidden" name="idsupplier" value="{{$selected_supplierdata->idSupplier}}"/>
															<input type="checkbox" name="ordersummary_checkbox[]" value="{{$data->paymentUniqueCode}}"/>
														</td>
														<td><a href="{{ url('/') }}/order_detail/{{$data->paymentUniqueCode}}/{{$data->idSupplier}}/{{$data->idCustomer}}/{{$data->idBinType}}/{{$data->idBinService}}">{{$data->paymentUniqueCode}}</a></td>
														<td>{{date('d/m/Y', strtotime($data->orderDate))}}</td>
														<td>{{$data->bintypename}}</td>
														<td>{{$data->binsize}}</td>
														<td>{{$data->customerName}}</td>
														<!--<td>{{$data->deliveryAddress}}</td>-->
														<td>{{date('d/m/Y', strtotime($data->deliveryDate))}}</td>
														<td><p class="text-danger"><strong>{{date('d/m/Y', strtotime($data->collectionDate))}}</strong></p></td>
														<td>
															<?php
																$gst = $data->subtotal*0.10;
																$subtotal = $data->subtotal;
																$sub = $sub+ $subtotal;
																$sum = 0.85*$sub;
															?>
															${{sprintf('%1.2f',$data->subtotal)}}
														</td>	
													</tr>	
												@endforeach
												<tr>
													<td colspan="8" class="text-center">
														<strong>Subtotal</strong>
													</td>
													<td>
														<strong>${{sprintf('%1.2f',$sub)}}</strong>
													</td>
												</tr>
												<tr>
													<td colspan="8" class="text-center">
														<strong>Grand Total (After deducted 15% for Ezyskips Online)</strong>
													</td>
													<td>
														<strong>${{sprintf('%1.2f',$sum)}}</strong>
													</td>
												</tr>
											</tbody>
										</table>		
									</div>
								
								<div class="row">
									<div class="col-12 text-center">
										<a href="{{ url('/') }}/order_summary/export/pdf/{{$selected_supplierdata->idSupplier}}/{{$actualStartDate}}/{{$actualEndDate}}" class="mb-2">
											<button name="button" type="button" class="btn btn-primary">
												<i class="fa fa-download"></i> Export the selected time range report to .pdf
											</button>
										</a>
										
										<button name="button" type="button" id="per_transaction" class="btn btn-primary">
											<i class="fa fa-download"></i> Export selected transaction report to .pdf
										</button>
										<iframe id="download_report" style="display:none;"></iframe>
									</div>
								</div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
@endsection
