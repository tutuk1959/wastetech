@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Orders Receipt</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Orders Receipt</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if (!empty($result))
								@if ($result['status'] == 'danger')
									<div class="alert alert-danger" role="alert">
										@foreach ($result['message'] as $message)
											{{$message}} <br />
										@endforeach
									</div>
								@elseif ($result['status'] == 'success')
									<div class="alert alert-success" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'warning')
									<div class="alert alert-warning" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'notif')
									<div class="alert alert-danger" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@endif
							@endif

							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						@if(!is_null($errorStatus) && $errorStatus == 'invoice_not_found')
							<div class="col-12">						
								<h1>Error Occurred</h1>
							</div>
						@else
							<div class="col-12">						
								<div class="card mb-3 mt-2">
									<div class="card-header">
										<strong>Order receipts for {{ $supplierData->name}}, {{$supplierData->contactName}} - {{$supplierData->fullAddress}}</strong>
									</div>
									
									<div class="card-body">
										<?php if (!is_null($orders)):?>
											<div class="table-responsive">
												<table class="table  table-bordered" style="border:none;">
													<thead class="text-center">
														<th>Order Ref</th>
														<th>Order Date</th>
														<th>Customer Name</th>
														<!--<th>Customer Address</th>-->
														<th>Delivery Date</th>
														<th>Collection Date</th>
														<th>Price</th>
														<th>Order Status</th>
														<th>Last Status Update</th>
														<th>Change Status</th>
														<th>Action</th>
													</thead>
													<tbody>
														@foreach($orders as $data)
															<tr>
																
																<td><a href="{{ url('/') }}/order_detail/{{$data->paymentUniqueCode}}/{{$data->idSupplier}}/{{$data->idCustomer}}/{{$data->idBinType}}/{{$data->idBinService}}">{{$data->paymentUniqueCode}}</a></td>
																<td>{{date('d/m/Y', strtotime($data->orderDate))}}</td>
																<td>{{$data->customerName}}</td>
																<!--<td>{{$data->deliveryAddress}}</td>-->
																<td>{{date('d/m/Y', strtotime($data->deliveryDate))}}</td>
																<td>{{date('d/m/Y', strtotime($data->collectionDate))}}</td>
																<td>${{sprintf('%1.2f',$data->subtotal)}}</td>	

																<td>
																	@if(!is_null($data->status))
																		@if($data->status == 1)
																			{{'Paid'}}
																		@elseif($data->status == 2)
																			{{'Accepted'}}
																		
																		@endif
																	@endif
																</td>
																<td>
																	<?php echo date('d/m/Y h:i:s A', strtotime($data->updated_at));?>
																</td>
																<td width="20%">
																	@if($data->status)
																		<form action="/change_order_status_by_user" method="post">
																			{{csrf_field()}}
																			<input type="hidden" name="idOrder" value="{{$data->idOrderService}}">
																			<input type="hidden" name="idSupplier" value="{{$data->idSupplier}}">
																			<div class="form-row">
    																			<div class="col-12">
      																				<select name="selectstatus" id="selectstatus" class="form-control m-1">
  																						<option value="#">Scroll down</option>
  																						<option value="2">Accepted</option>
  																						
  																					</select>
    																			</div>
    																			<div class="col-12">
      																				<button type="submit" class="btn btn-primary m-1">Update</button>
    																			</div>
  																			</div>
																		</form>
																	@endif
																</td>	

															<td>
																<a href="{{ url('/') }}/orderservice/{{$data->idOrderService}}" class="mb-2 mr-2 float-left">
																	<button name="button" type="button" class="btn btn-primary">
																		<i class="fa fa-search"></i>
																</button>

																<a href="{{ url('/') }}/export/pdf/{{$data->idOrderService}}" class="mb-2 float-left">
																	<button name="button" type="button" class="btn btn-primary">
																		<i class="fa fa-download"></i>
																	</button>
																</a>
															</td>
														</tr>	
														@endforeach
													</tbody>
												</table>
											</div>
											
										<?php endif;?>
									</div>
								</div><!-- end card-->
							</div>
						@endif
						
					</div>
				</div>
			</div>
		</div>
@endsection
