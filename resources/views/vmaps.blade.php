@extends('template')
@section('content')
<style>
.scroll {
    max-height: 400px;
    overflow-y: auto;
}
#map-canvas {
width: 100%;
height: 400px;
}


</style>
<script type="text/javascript">

</script>
 <script type="text/javascript">
 	

    var markers = [

      ['Truck C - Drop - Freemantle 55 south av', -32.057192, 115.749559,'blue'],
      ['Truck A - Pick Up - Melvile 3 Road PI', -32.033938, 115.78621,'red'],
      ['Truck B - Pick Up - Melvile 3 Road PI', -32.033938, 115.79621,'yellow'],
      ['Truck A - Pick Up - Freemantle 55 south av', -32.057192, 115.759559,'red'],
      ['Truck C - Drop - City of Perth', -31.95224, 115.8514,'blue'],
    	 ['Truck B - Drop - City of Perth', -31.95224, 115.8614, 'yellow'],
		['Truck B - Drop - City of Perth', -31.95224, 115.7614, 'yellow']
    ];

      function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
        		position: {lat:-31.95224, lng:115.8614},
          		zoom: 8,
              	mapTypeId: google.maps.MapTypeId.ROADMAP
        }     
        var map = new google.maps.Map(mapCanvas, mapOptions)

    var infowindow = new google.maps.InfoWindow(), marker, i;
    var bounds = new google.maps.LatLngBounds(); // diluar looping
	    for (i = 0; i < markers.length; i++) {  
	    pos = new google.maps.LatLng(markers[i][1], markers[i][2]);
	    bounds.extend(pos); // di dalam looping
	    marker = new google.maps.Marker({
	        position: pos,
	        map: map,
	        icon: {
			      url: "http://maps.google.com/mapfiles/ms/icons/" + markers[i][3] + "-dot.png"
			    }

	        });
	google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            infowindow.setContent(markers[i][0]);
            infowindow.open(map, marker);
        }
    })(marker, i));
    map.fitBounds(bounds); // setelah looping
    }

      }


      google.maps.event.addDomListener(window, 'load', initialize);
    </script>

		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Bin Logistic Views</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Bin Logistic View</li>
								</ol>


								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div></div>
					
					<div class="row">
						<div class="col-3">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Driver Run Sheet</strong>
									<button type="submit" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addnew">test new</button>
								</div>
									
								<div class="card-body scroll" style="height:400px;">
								@foreach($driver_sheet as $data)

									<div class="col-sm bg-{{$data->color_code}} mb-3">
										<div class="row">
										<i><b>New {{$data->type}} <br> {{$data->datetime}}</b></i>	
										{{$data->driver}}, {{$data->truck}}<br>
										{{$data->type}}: {{$data->address}}<br>
										</div>
									</div>
									@endforeach
									
								</div>
							</div><!-- end card-->
						</div>
						<div class="col-9">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Logistic Maps</strong>
								</div>
									
								<div class="card-body" style="height:400px;">
									<div id="map-canvas"></div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Bin Logistic View</strong>
								</div>
									
								<div class="card-body">
									<div class="row">
										<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
										  <li class="nav-item">
										    <a class="nav-link active" id="pills-scheduling-tab" data-toggle="pill" href="#pills-scheduling" role="tab" aria-controls="pills-scheduling" aria-selected="false">Scheduling</a>
										  </li>
										  <li class="nav-item">
										    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">All Bins</a>
										  </li>
										  <li class="nav-item">
										    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Waste Facility</a>
										  </li>
										  <li class="nav-item">
										    <a class="nav-link" id="pills-recycle-tab" data-toggle="pill" href="#pills-recycle" role="tab" aria-controls="pills-recycle" aria-selected="false">Recycled</a>
										  </li>
										  <li class="nav-item">
										    <a class="nav-link" id="pills-hazardous-tab" data-toggle="pill" href="#pills-hazardous" role="tab" aria-controls="pills-hazardous" aria-selected="false">Hazardous</a>
										  </li>
										</ul>
										<div class="tab-content" id="pills-tabContent">
										  
										  <div class="tab-pane fade show active" id="pills-scheduling" role="tabpanel" aria-labelledby="pills-scheduling-tab">
										  this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon. 
										</div>
										<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
										   this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon.
										</div>
										  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
										    this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon.
										  </div>
										  	<div class="tab-pane fade" id="pills-recycle" role="tabpanel" aria-labelledby="pills-recycle-tab">
										    this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon.
										  </div>
										  	<div class="tab-pane fade" id="pills-hazardous" role="tabpanel" aria-labelledby="pills-hazardous-tab">
										  	 this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon. this features is coming soon.
										  </div>
										</div>
									     
									</div>
								</div>
							</div><!-- end card-->
						</div>

					</div>
					
					
				</div>
			</div>
		</div>
@endsection
<div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	<form action="/insert_to_maps" method="post" class="form-inline">
      		{{csrf_field()}}
        <h5 class="modal-title" id="exampleModalLongTitle">Add new log</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<label class="mr-3" for="selectsupplier">Datetime</label>
      	<input type="text" class="form-control mr-3" name="datetime" value="2020-02-17 09:20:30">
		<label class="mr-3" for="selecttype">Select Type</label>
		<select name="selecttype" id="selecttype" class="form-control mr-3">
			<option value="1">Pick Up</option>
			<option value="2">Drop</option>
		</select>
		<label class="mr-3" for="selecttruck">Select Truck</label>
		<select name="selecttruck" id="selecttruck" class="form-control mr-3">
			<option value="1">Truck A - Sam The Driver</option>
			<option value="2">Truck B - Bob The Driver</option>
			<option value="3">Truck C - Tim The Driver</option>
		</select>
		<label class="mr-3" for="selectbintype">Select Location</label>
		<select name="selectlocation" id="selectlocation" class="form-control mr-3">
			<option value="1">Freemantle 55 south av</option>
			<option value="2">Melville 3 road PI</option>
			<option value="3">Melville 10 some PI</option>
		</select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
  	</form>
    </div>
  </div>
</div>

    <script>
     /* function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }*/
    </script>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDM1f1ZAYRCi0LqEqI-CZuU7mebY1KbDJ0&callback=initMap"></script>

	
