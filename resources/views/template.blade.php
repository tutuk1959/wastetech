<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="_token" content="{{csrf_token()}}" />
		<title>Waste Technology Solutions</title>
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/plugins/datetimepicker/css/daterangepicker.css')}}" rel="stylesheet" /> 
		<link href="{{url('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" /> 
		<link href="{{url('assets/css/css-loader.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
		<script src="{{url('assets/js/jquery.min.js')}}"></script>
</head>
<style type="text/css">
	.card-box{
		min-height: 120px;
	}
</style>
<body class="adminbody">
	<div id="main">
		<div class="headerbar">
			<div class="headerbar-left">
				<a href="{{url('/away')}}" class="logo"><img alt="Logo" src="{{url('assets/images/wastetech-logo_04.png')}}" /></a>
			</div>
			<nav class="navbar-custom">
				
				<ul class="list-inline float-right mb-0">
					<li class="list-inline-item dropdown notif">
						<a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<img src="{{url('assets/images/admin.png')}}" alt="Profile image" class="avatar-rounded">
						</a>
						<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5 class="text-overflow"><small>{{session('conName')}} , {{session('email')}}</small> </h5>
							</div>
	
							<!-- item-->
							<a href="/details_service_zone" class="dropdown-item notify-item">
								<i class="fa fa-user"></i> <span>Profile</span>
							</a>
							
							@if(session('role'))
								@if(session('role') == '1')
									<a href="{{url('manage_user')}}" class="dropdown-item notify-item">
										<i class="fa fa-user-circle"></i> <span>Manage User</span>
									</a>
									<?php /**
									<a href="{{url('manage_order')}}" class="dropdown-item notify-item">
										<i class="fa fa-shopping-cart"></i> <span>Manage Order</span>
									</a>
									<a href="{{url('all_supplier_order')}}" class="dropdown-item notify-item">
										<i class="fa fa-shopping-cart"></i> <span>Suppliers Supplies</span>
									</a>
									<a href="{{url('all_supplier_order_date')}}" class="dropdown-item notify-item">
										<i class="fa fa-shopping-cart"></i> <span>All Orders</span>
									</a>
									<a href="{{url('manage_summary')}}" class="dropdown-item notify-item">
										<i class="fa fa-file-pdf-o"></i> <span>Order Summary</span>
									</a>
									*/?>
									<a href="{{url('booking_fee')}}" class="dropdown-item notify-item">
										<i class="fa fa-money"></i> <span>Booking Fee</span>
									</a>
									<?php /**
									<a href="{{url('drivers')}}" class="dropdown-item notify-item">
										<i class="fa fa-id-card"></i> <span>Drivers</span>
									</a>
									
									<a href="{{url('assets')}}" class="dropdown-item notify-item">
										<i class="fa fa-truck"></i> <span>Assets</span>
									</a>
									
									<a href="{{url('organization')}}" class="dropdown-item notify-item">
										<i class="fa fa-sitemap"></i> <span>Organizations</span>
									</a>
									*/?>
								@endif
							@endif
							

							<!-- item-->
							<a href="/logout" class="dropdown-item notify-item">
								<i class="fa fa-power-off"></i> <span>Logout</span>
							</a>

						</div>
					</li>
				</ul>
				
				<ul class="list-inline menu-left mb-0">
					<li class="float-left">
						<button class="button-menu-mobile open-left">
							<i class="fa fa-fw fa-bars"></i>
						</button>
					</li>
				</ul>
			</nav>
		</div>
		<!-- End Navigation -->
		<!-- Left Sidebar -->
		<div class="left main-sidebar">
			<div class="sidebar-inner leftscroll">
				<div id="sidebar-menu">
					<ul>
						<li class="submenu">
							<a {{{ (Request::is('instructions') ? 'class=active' : '') }}} href="/instructions"><i class="fa fa-fw fa-info-circle"></i><span> Intructions & Informations</span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('maps') ? 'class=active' : '') }}} {{{ (Request::is('maps/{status}/{message}') ? 'class=active' : '') }}} href="/maps"><i class="fa fa-fw fa-id-card-o"></i><span> Bin Logistic View </span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('details_service_zone') ? 'class=active' : '') }}} {{{ (Request::is('details_service_zone/{status}/{message}') ? 'class=active' : '') }}} href="/details_service_zone"><i class="fa fa-fw fa-id-card-o"></i><span> Details & Service Zone</span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('general_waste') ? 'class=active' : '') }}} {{{ (Request::is('general_waste/{status}.{message}') ? 'class=active' : '') }}} href="/general_waste"><i class="fa fa-fw fa-circle"></i><span> General Waste</span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('mixed_heavy_waste') ? 'class=active' : '') }}} {{{ (Request::is('mixed_heavy_waste/{status}.{message}') ? 'class=active' : '') }}} href="/mixed_heavy_waste"><i class="fa fa-fw fa-circle"></i><span> Mixed Heavy Waste</span> </a>
						</li>
						<!--<li class="submenu">
							<a {{{ (Request::is('clean_fills_schedule') ? 'class=active' : '') }}} {{{ (Request::is('clean_fills_schedule/{status}.{message}') ? 'class=active' : '') }}} href="/clean_fills_schedule"><i class="fa fa-fw fa-circle"></i><span> Clean Fills Schedule </span> </a>
						</li>-->
						<li class="submenu">
							<a {{{ (Request::is('green_waste_schedule') ? 'class=active' : '') }}} {{{ (Request::is('green_waste_schedule/{status}.{message}') ? 'class=active' : '') }}} href="/green_waste_schedule"><i class="fa fa-fw fa-circle"></i><span> Green Waste </span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('dirt_waste_schedule') ? 'class=active' : '') }}} {{{ (Request::is('dirt_waste_schedule/{status}.{message}') ? 'class=active' : '') }}} href="/dirt_waste_schedule"><i class="fa fa-fw fa-circle"></i><span> Dirt Waste </span> </a>
						</li>
						<li class="submenu">
							<a {{ (Request::is('orders') ? 'class=active' : '') }} href="{{url('orders')}}"><i class="fa fa-fw fa-file"></i><span>Orders</span> </a>
						</li>
						
						<li class="submenu">
							<a {{(Request::is('task_lists_index') ? 'class=active' : '')}}  href="{{url('task_lists_index')}}"><i class="fa fa-calendar"></i><span>Scheduling </span></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- End Sidebar -->
		@section('content')
		@show
		<!-- END content-page -->
		
		<footer class="footer">
			<span class="text-right">
				Copyright <a target="_blank" href="https://somsweb.com.au">SOMS Web</a>
			</span>
		</footer>
	</div>
<!-- END main -->
<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/modernizr.min.js')}}"></script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/popper.js')}}"></script>
<script src="{{url('assets/js/jquery-ui.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/detect.js')}}"></script>
<script src="{{url('assets/js/fastclick.js')}}"></script>
<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('assets/js/pikeadmin.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/daterangepicker.js')}}"></script>
<script src="{{url('assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	var generate_pdf_checbox_url = "<?php echo route('pdfexporterselected');?>";
	var tasks_assign = "<?php echo route('task_assignment');?>";
	var assign_driver = "<?php echo route('assign_driver');?>";
	var org_based_phone = "<?php echo route('org_based_phone');?>";
	var order_based_phone = "<?php echo route('order_based_phone');?>";
	var load_order = "<?php echo route('load_order');?>";
	var get_price = "<?php echo route('get_price');?>";
	var convert_to_order = "<?php echo route('convert_to_order');?>";
	
	var load_orderitems_based_id = "<?php echo route('load_orderitems_based_id');?>";
	var load_order_based_id = "<?php echo route('load_order_based_id');?>";
	var calculating_total = "<?php echo route('calculating_total');?>";
	
	var fetch_delivery_address = "<?php echo route('fetch_delivery_address');?>";
	var take_payments = "<?php echo route('take_payments');?>";
</script>
<script src="{{url('assets/js/script.js')}}"></script>
<script src="{{url('assets/js/scheduling_script.js')}}"></script>
<script src="{{url('assets/js/order_new_workflow.js')}}"></script>

</body>
</html>