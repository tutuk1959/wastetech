@extends('template')
@section('content')

<style>
	body.dragging, body.dragging * {
  cursor: move !important;
}

.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}

ol.example li.placeholder {
  position: relative;
  /** More li styles **/
}
ol.example li.placeholder:before {
  position: absolute;
  /** Define arrowhead **/
}
</style>
<div class="content-page">
			<div class="content">
				<div class="container-fluid">
						<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Bin Logistic View</strong>
								</div>
									
								<div class="card-body">
									<div class="row">
									
									<ol class='example'>
  <li>First</li>
  <li>Second</li>
  <li>Third</li>
</ol>
<script src='js/jquery-sortable.js'></script>
									     
									</div>
								</div>
							</div><!-- end card-->
						</div>

					</div>

				
					
					
				</div>
			</div>
		</div>
@endsection
<script>
	$(function  () {
  $("ol.example").sortable();
});
	var adjustment;

$("ol.simple_with_animation").sortable({
  group: 'simple_with_animation',
  pullPlaceholder: false,
  // animation on drop
  onDrop: function  ($item, container, _super) {
    var $clonedItem = $('<li/>').css({height: 0});
    $item.before($clonedItem);
    $clonedItem.animate({'height': $item.height()});

    $item.animate($clonedItem.position(), function  () {
      $clonedItem.detach();
      _super($item, container);
    });
  },

  // set $item relative to cursor position
  onDragStart: function ($item, container, _super) {
    var offset = $item.offset(),
        pointer = container.rootGroup.pointer;

    adjustment = {
      left: pointer.left - offset.left,
      top: pointer.top - offset.top
    };

    _super($item, container);
  },
  onDrag: function ($item, position) {
    $item.css({
      left: position.left - adjustment.left,
      top: position.top - adjustment.top
    });
  }
});
</script>