@extends('template')
@section('content')
	<div class="content-page">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-12">
						<div class="breadcrumb-holder">
							<h1 class="main-title float-left">Details & Service Area</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item">Detais & Service Zone</li>
									<li class="breadcrumb-item active">Service Area Importer</li>
								</ol>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						@if (!empty(session('status')))
							@if (session('status') == 'danger')
								<div class="alert alert-danger" role="alert">
										{{session('message')}} <br />
								</div>
							@elseif (session('status') == 'success')
								<div class="alert alert-success" role="alert">
										{{session('message')}} <br />
									
								</div>
							@endif
						@endif
					</div>
				</div>
				
				<div class="row">
					<div class="col-12">
						<div class="card mb-3 mt-2">
							<div class="card-header">
								<strong>CSV Importer for Service Area Data</strong>
							</div>
								
							<div class="card-body">
								<form class="form-horizontal" method="POST" action="{{ route('import_process') }}">
									{{ csrf_field() }}
									<input type="hidden" name="csv_data_file_id" value="{{ $csv_data_file->id }}" />
		
									<table class="table">
										@if (isset($csv_header_fields))
										<tr>
											@foreach ($csv_header_fields as $csv_header_field)
												<th>{{ $csv_header_field }}</th>
											@endforeach
										</tr>
										@endif
										@foreach ($csv_data as $row)
											<tr>
											@foreach ($row as $key => $value)
												<td>{{ $value }}</td>
											@endforeach
											</tr>
										@endforeach
										<tr>
											@foreach ($csv_data[0] as $key => $value)
												<td>
													<select name="fields[{{ $key }}]">
														@foreach (config('app.db_fields') as $db_field)
															<option value="{{ (\Request::has('header')) ? $db_field : $loop->index }}"
																@if ($key === $db_field) selected @endif>{{ $db_field }}</option>
														@endforeach
													</select>
												</td>
											@endforeach
										</tr>
									</table>
		
									<button type="submit" class="btn btn-primary">
										Import Data
									</button>
								</form>
							</div>
						</div><!-- end card-->
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
