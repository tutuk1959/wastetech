@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Bin Type Summary</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Bin Type Summary</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('selectsupplier'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('selectsupplier') }}
								</div>
							@endif
							@if ($errors->has('selectstatus'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('selectstatus') }}
								</div>
							@endif
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Order per Bin Type</strong>
								</div>
									
								<div class="card-body">
									<form action="/see_supplies_by_bin_summary" method="post" class="form-inline">
										{{csrf_field()}}
  										<label class="mr-3" for="selectbintype">Choose Bin Type</label>
  											<select name="selectbintype" id="selectbintype" class="form-control mr-3">
												
  												<option value="#">Select Bin Types</option>
  												@foreach($bintypedata as $bintype)
													@if(!is_null($bintypedata_byid))
	  													@foreach ($bintypedata_byid as $object)
	   													<option value="{{$bintype->idBinType}}" <?=($object->idBinType == $bintype->idBinType) ? 'selected' : '';?>>{{$bintype->name}}</option>
														@endforeach	
													@else
														<option value="{{$bintype->idBinType}}" >{{$bintype->name}}</option>
													@endif
												@endforeach	
  											</select>
										
  										<label class="mr-3" for="selectbinsize">Choose Bin Size</label>
  											<select name="selectbinsize" id="selectbinsize" class="form-control mr-3">
												
  												<option value="#">Select Bin Size</option>
  												@foreach($binsizedata as $binsize)
													@if(!is_null($bintypedata_byid))
	  													@foreach ($binsizedata_byid as $objects)
	   													<option value="{{$bintype->idBinType}}" <?=($objects->IdSize == $binsize->IdSize) ? 'selected' : '';?>>{{$binsize->size}}</option>
														@endforeach	
													@else
														<option value="{{$binsize->IdSize}}" >{{$binsize->size}}</option>
													@endif
												@endforeach	
  											</select>	
  										
										<label class="mr-3" for="selectsupplier">Choose Date</label>
											<?php /*<input type="text" class="form-control mr-3" name="order_management_datepicker" width="30"> */ ?>
											<input type="text" class="form-control mr-3" name="fromdate" width="30">-
											<input type="text" class="form-control ml-3 mr-3" name="todate" width="30">
											<input type="hidden" name="start_order_date" value="">
											<input type="hidden" name="end_order_date" value="">

  										<button type="submit" class="btn btn-primary">Search</button>
									</form>
								</div>
							</div><!-- end card-->
						</div>
					</div>
					

				@if(!is_null($suppliesdata))
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									Bin Type Summary
								</div>
									
								<div class="card-body">
									<div class="table-responsive">
									
										<table class="table table-bordered" style="border:none;">
											<thead class="text-center">
												<th><input type="checkbox" name="ordersummary_checkbox_all" value="1"/></th>
												<th>Bin Type</th>
												<th>Bin Size</th>
												<th>Order Ref</th>
												<th>Order Date</th>
												<th>Customer Name</th>  
												<th>Delivery Date</th>
												<th>Collection Date</th>
												<th>Revenue</th>
											</thead>
											<tbody>
												<?php $sum = 0;$sub= 0; $urut=0; ?>
												@foreach($suppliesdata as $data)
													<tr>
														<td>
															<input type="checkbox" name="ordersummary_checkbox[]" value="{{$data->paymentUniqueCode}}"/>
														</td>
														<td>{{$data->bintypename}}</td>
														<td>{{$data->binsize}}</td>
														<td><a href="{{ url('/') }}/order_detail/{{$data->paymentUniqueCode}}/{{$data->idSupplier}}/{{$data->idCustomer}}/{{$data->idBinType}}/{{$data->idBinService}}">{{$data->paymentUniqueCode}}</a></td>
														<td>{{date('d/m/Y', strtotime($data->orderDate))}}</td>
														<td>{{$data->customerName}}</td>
														<!--<td>{{$data->deliveryAddress}}</td>-->
														<td>{{date('d/m/Y', strtotime($data->deliveryDate))}}</td>
														<td><p class="text-danger"><strong>{{date('d/m/Y', strtotime($data->collectionDate))}}</strong></p></td>
														<td>
															<?php
																$gst = $data->subtotal*0.10;
																$subtotal = $data->subtotal;
																$sub = $sub+ $subtotal;
																$sum = 0.85*$sub;
															?>
															${{sprintf('%1.2f',$data->subtotal)}}
														</td>	
													</tr>
												<?php $urut++; ?>
												@endforeach
												<?php $total=$urut; ?>
												<tr>
													<td colspan="8" class="text-center">
														<strong>Total bin</strong>
													</td>
													<td>
														<strong>{{$total}} pcs</strong>
													</td>
												</tr>
												<tr>
													<td colspan="8" class="text-center">
														<strong>Subtotal</strong>
													</td>
													<td>
														<strong>${{sprintf('%1.2f',$sub)}}</strong>
													</td>
												</tr>
												<tr>
													<td colspan="8" class="text-center">
														<strong>Grand Total (After deducted 15% for Ezyskips Online)</strong>
													</td>
													<td>
														<strong>${{sprintf('%1.2f',$sum)}}</strong>
													</td>
												</tr>
											</tbody>
										</table>		
									</div>
								
								</div>
							</div><!-- end card-->
						</div>
					</div>
					@endif


				</div>
			</div>
		</div>
@endsection
