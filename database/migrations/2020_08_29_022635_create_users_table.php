<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('idUser');
			$table->integer('idSupplier');
			$table->string('username', 100);
			$table->string('password', 100);
			$table->char('isActive', 1);
			$table->char('role', 1);
			$table->char('adminApproved', 1);
			$table->char('userStatus', 1);
			$table->date('registerDate');
			$table->char('adminNotifications', 1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
