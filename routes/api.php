<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('bintype','ApiController@showBinType');
Route::get('bintype/{idBinType}','ApiController@findBinType');
Route::get('sizes','ApiController@showSizes');
Route::get('sizes/{idSize}','ApiController@findSize');
Route::get('binhire/{idBinType}/{idBinSize}/{zipcode}/{deliverydate}/{collectiondate}','ApiController@showBinHire');
Route::get('getbestdeal','ApiController@getBestDeal');
Route::get('showstate/','ApiController@showState');
Route::get('showarea/{zipcode}','ApiController@showArea');
Route::get('showpostcode/{zipcode}','ApiController@showPostcode');
Route::get('findarea/{zipcode}', 'ApiController@findArea');
Route::get('binhireoptions/{idBinType}/{idSupplier}','ApiController@binhireOptions');
Route::get('payment/{idbinhire}/{zipcode}/{deliverydate}/{collectiondate}','ApiController@payment');

/** Mobile Auth **/
Route::post('login', [
	'as' => 'login',
	'uses' => 'mobiledriver\mobiledriver@login'
]);
Route::post('register', 'mobiledriver\mobiledriver@register');

Route::group([
    'middleware' => [
        'auth:api',
        'can:access-api-driver'
    ]
], function(){
	Route::post('change_password', 'mobiledriver\mobiledriver@change_password');
	Route::post('task_master', 'mobiledriver\mobiledriver@task_master');
	Route::post('task_master_date', 'mobiledriver\mobiledriver@task_master_date');
	Route::post('task_master_uncompleted', 'mobiledriver\mobiledriver@task_master_uncompleted');
	Route::post('task_master_completed', 'mobiledriver\mobiledriver@task_master_completed');
	Route::post('task_master_pending', 'mobiledriver\mobiledriver@task_pending');
	Route::post('task_master_non_pending', 'mobiledriver\mobiledriver@task_nonpending');
	Route::post('task_progress', 'mobiledriver\mobiledriver@task_progress');
	Route::post('task_detail_id', 'mobiledriver\mobiledriver@task_detail_id');
	Route::get('bin_size', 'mobiledriver\mobiledriver@bin_size');
	Route::get('bin_type', 'mobiledriver\mobiledriver@bin_type');
	Route::post('driver_detail', 'mobiledriver\mobiledriver@driver_detail');
	Route::get('all_status', 'mobiledriver\mobiledriver@all_status');
	Route::post('update_task_status', 'mobiledriver\mobiledriver@updateTaskStatus');
	Route::post('update_task_has_status', 'mobiledriver\mobiledriver@update_task_has_status');
	Route::post('update_task_has_time', 'mobiledriver\mobiledriver@update_task_has_time');
	Route::post('update_task_geo_information', 'mobiledriver\mobiledriver@update_task_geo_information');
	Route::post('update_task_geo_distance', 'mobiledriver\mobiledriver@update_task_geo_distance');
});
