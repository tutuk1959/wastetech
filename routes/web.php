<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*** 404 Baby ***/

/****** LOGIN & REGISTER ***/
Route::get('/', [
	'as' => 'home',
	'uses' => 'loginController@index'
]);
Route::get('/login', 'loginController@index')->middleware('SignedIn');
Route::post('/registerInterest', 'loginController@register');
Route::get('user/verify/{verification_code}', 'loginController@verifyUser');
Route::post('/loginUser', 'loginController@login');
Route::get('/logout', 'loginController@logout');
Route::get('/forgotpassword',[
	'as' => 'forgot_password_view',
	'uses' => 'loginController@forget_password_index'
]);
Route::post('/forgot_password_submit', 'loginController@forgot_password_submit');

Route::get('/lord_of_all_dragons', [
	'as' => 'home_debug',
	'uses' => 'loginController@debugmode'
]);
Route::post('lord_of_all_dragons/loginUser', 'loginController@debugmode_login');


/****** INSTRUCTION PAGE ***/
Route::get('/instructions', function(){
	return view('dashboard');
})->middleware('checkRole:1,2');

/****** DETAILS & SERVICE AREA ***/
Route::get('/details_service_zone', [
	'as' => 'details_service_zone',
	'uses' => 'ServiceAreaController@index'
])->middleware('checkRole:1,2');
Route::get('/user_details_service_zone/{idUser}', 'ServiceAreaController@userdetails_byId')->name('userdetails')->middleware('checkRole:1,2');
Route::get('/details_service_zone/{status}/{message}',[
	'as' => 'details.service.zone',
	'uses' => 'ServiceAreaController@showResult'
])->middleware('checkRole:1,2');
Route::post('/editSupplierDetails', 'ServiceAreaController@editSupplierDetails')->middleware('checkRole:1,2');
Route::post('/editUserDetails', 'ServiceAreaController@editUserDetails')->middleware('checkRole:1,2');
Route::post('/editArea/{postalcode}', 'ServiceAreaController@editServiceArea')->middleware('checkRole:1,2');

Route::get('/serviceareaimporter', 'ServiceAreaController@csv_importer_view')->name('import_view')->middleware('checkRole:1');
Route::post('/import_parse', 'ServiceAreaController@parseImport')->name('import_parse')->middleware('checkRole:1');
Route::post('/import_process', 'ServiceAreaController@processImport')->name('import_process')->middleware('checkRole:1');

/****** GENERAL WASTE ***/
Route::get('/general_waste', [
	'as' => 'general_waste',
	'uses' => 'GeneralWaste@index'
])->middleware('checkRole:1,2');


Route::get('/general_waste/{status}/{message}',[
	'as' => 'general.waste',
	'uses' => 'GeneralWaste@showResult'
])->middleware('checkRole:1,2');

Route::get('1/calendarOffset/{offset}',[
	'uses' => 'GeneralWaste@showResult'
])->middleware('checkRole:1,2');

Route::get('1/{binsize}/calendarOffset/{offset}',[
	'uses' => 'GeneralWaste@showForm'
])->middleware('checkRole:1,2');

Route::post('/editMiscDetails/1', 'GeneralWaste@editMiscDetail')->middleware('checkRole:1,2');
Route::post('1/editNonDeliveryDays', 'GeneralWaste@editNonDeliveryDays')->middleware('checkRole:1,2');

Route::get('1/showEditPricingForm/{bintype}/{binsize}',[
	'as' => 'generalwaste.pricing.form',
	'uses' => 'GeneralWaste@showForm'
])->middleware('checkRole:1,2');

Route::get('1/showForm/{status}/{message}',[
	'as' => 'generalwaste.form',
	'uses' => 'GeneralWaste@showFormResult'
])->middleware('checkRole:1,2');

Route::post('1/editBinServicePrice', 'GeneralWaste@editBinServicePrice')->middleware('checkRole:1,2');
Route::delete('1/resetPricing/{bintype}/{binsize}', 'GeneralWaste@resetRates')->middleware('checkRole:1,2');

/****** MIXED HEAVY WASTE ***/
Route::get('/mixed_heavy_waste',[
	'as' => 'mixed_heavy_waste',
	'uses' => 'MixedHeavyWaste@index'
])->middleware('checkRole:1,2');

Route::get('/mixed_heavy_waste/{status}/{message}',[
	'as' => 'mixed.heavy.waste',
	'uses' => 'MixedHeavyWaste@showResult'
])->middleware('checkRole:1,2');

Route::get('2/calendarOffset/{offset}',[
	'uses' => 'MixedHeavyWaste@showResult'
])->middleware('checkRole:1,2');

Route::get('2/{binsize}/calendarOffset/{offset}',[
	'uses' => 'MixedHeavyWaste@showForm'
])->middleware('checkRole:1,2');


Route::post('/editMiscDetails/2', 'MixedHeavyWaste@editMiscDetail')->middleware('checkRole:1,2');
Route::post('2/editNonDeliveryDays', 'MixedHeavyWaste@editNonDeliveryDays')->middleware('checkRole:1,2');

Route::get('2/showEditPricingForm/{bintype}/{binsize}',[
	'as' => 'mixed.heavy.waste.pricing.form',
	'uses' => 'MixedHeavyWaste@showForm'
])->middleware('checkRole:1,2');

Route::get('2/showForm/{status}/{message}',[
	'as' => 'mixed.heavy.waste.form',
	'uses' => 'MixedHeavyWaste@showFormResult'
])->middleware('checkRole:1,2');

Route::post('2/editBinServicePrice', 'MixedHeavyWaste@editBinServicePrice')->middleware('checkRole:1,2');
Route::delete('2/resetPricing/{bintype}/{binsize}', 'MixedHeavyWaste@resetRates')->middleware('checkRole:1,2');

/****** CLEAN FILLS SCHEDULE ***/
Route::get('/clean_fills_schedule', 'CleanFills@index')->middleware('checkRole:1,2');

Route::get('/clean_fills_schedule/{status}/{message}',[
	'as' => 'clean.fills.schedule',
	'uses' => 'CleanFills@showResult'
])->middleware('checkRole:1,2');

Route::get('3/calendarOffset/{offset}',[
	'uses' => 'CleanFills@showResult'
])->middleware('checkRole:1,2');

Route::get('3/{binsize}/calendarOffset/{offset}',[
	'uses' => 'CleanFills@showForm'
])->middleware('checkRole:1,2');


Route::post('/editMiscDetails/3', 'CleanFills@editMiscDetail')->middleware('checkRole:1,2');
Route::post('3/editNonDeliveryDays', 'CleanFills@editNonDeliveryDays')->middleware('checkRole:1,2');

Route::get('3/showEditPricingForm/{bintype}/{binsize}',[
	'as' => 'clean.fills.pricing.form',
	'uses' => 'CleanFills@showForm'
])->middleware('checkRole:1,2');

Route::get('3/showForm/{status}/{message}',[
	'as' => 'clean.fills.waste.form',
	'uses' => 'CleanFills@showFormResult'
])->middleware('checkRole:1,2');

Route::post('3/editBinServicePrice', 'CleanFills@editBinServicePrice')->middleware('checkRole:1,2');
Route::delete('3/resetPricing/{bintype}/{binsize}', 'CleanFills@resetRates')->middleware('checkRole:1,2');

/****** GREEN WASTE SCHEDULE ***/
Route::get('/green_waste_schedule',  [
	'as' => 'green_waste',
	'uses' => 'GreenWaste@index'
])->middleware('checkRole:1,2');

Route::get('/green_waste_schedule/{status}/{message}',[
	'as' => 'green.waste.schedule',
	'uses' => 'GreenWaste@showResult'
])->middleware('checkRole:1,2');

Route::get('4/calendarOffset/{offset}',[
	'uses' => 'GreenWaste@showResult'
])->middleware('checkRole:1,2');

Route::get('4/{binsize}/calendarOffset/{offset}',[
	'uses' => 'GreenWaste@showForm'
])->middleware('checkRole:1,2');


Route::post('/editMiscDetails/4', 'GreenWaste@editMiscDetail')->middleware('checkRole:1,2');
Route::post('4/editNonDeliveryDays', 'GreenWaste@editNonDeliveryDays')->middleware('checkRole:1,2');

Route::get('4/showEditPricingForm/{bintype}/{binsize}',[
	'as' => 'green.waste.pricing.form',
	'uses' => 'GreenWaste@showForm'
])->middleware('checkRole:1,2');

Route::get('4/showForm/{status}/{message}',[
	'as' => 'green.waste.form',
	'uses' => 'GreenWaste@showFormResult'
])->middleware('checkRole:1,2');

Route::post('4/editBinServicePrice', 'GreenWaste@editBinServicePrice')->middleware('checkRole:1,2');
Route::delete('4/resetPricing/{bintype}/{binsize}', 'GreenWaste@resetRates')->middleware('checkRole:1,2');

/****** DIRT WASTE SCHEDULE ***/
Route::get('/dirt_waste_schedule', [
	'as' => 'dirt_waste',
	'uses' => 'DirtWaste@index'
])->middleware('checkRole:1,2');

Route::get('/dirt_waste_schedule/{status}/{message}',[
	'as' => 'dirt.waste.schedule',
	'uses' => 'DirtWaste@showResult'
])->middleware('checkRole:1,2');

Route::get('5/calendarOffset/{offset}',[
	'uses' => 'DirtWaste@showResult'
])->middleware('checkRole:1,2');

Route::get('5/{binsize}/calendarOffset/{offset}',[
	'uses' => 'DirtWaste@showForm'
])->middleware('checkRole:1,2');


Route::post('/editMiscDetails/5', 'DirtWaste@editMiscDetail')->middleware('checkRole:1,2');
Route::post('5/editNonDeliveryDays', 'DirtWaste@editNonDeliveryDays')->middleware('checkRole:1,2');

Route::get('5/showEditPricingForm/{bintype}/{binsize}',[
	'as' => 'dirt.waste.pricing.form',
	'uses' => 'DirtWaste@showForm'
])->middleware('checkRole:1,2');

Route::get('5/showForm/{status}/{message}',[
	'as' => 'dirt.waste.form',
	'uses' => 'DirtWaste@showFormResult'
])->middleware('checkRole:1,2');

Route::post('5/editBinServicePrice', 'DirtWaste@editBinServicePrice')->middleware('checkRole:1,2');
Route::delete('5/resetPricing/{bintype}/{binsize}', 'DirtWaste@resetRates')->middleware('checkRole:1,2');

/****** ORDER RECEIPTING  ***/
Route::get('/order_receipt', [
	'as' => 'order.receipt',
	'uses' => 'ReceiptOrder@index'
])->middleware('checkRole:1,2');

Route::get('manage_order/','adminController@manage_order_view')->name('supplier_supplies')->middleware('checkRole:1');
Route::get('all_supplier_order/','adminController@all_order_view')->name('all_supplier_supplies')->middleware('checkRole:1');
Route::get('all_supplier_order_date/','adminController@all_order_view_date')->name('all_supplier_supplies_date')->middleware('checkRole:1');
Route::get('/manage_order/{idSupplier}',[
	'as' => 'supplier_supplies_result',
	'uses' => 'adminController@fetch_supplier_order_status'
])->middleware('checkRole:1');
Route::post('see_supplies_by_supplier/','adminController@fetch_supplier_order')->name('fetch_all_order_supplies')->middleware('checkRole:1');
Route::post('see_supplies_all/','adminController@fetch_all_supplier')->name('fetch_all_supplier')->middleware('checkRole:1');
Route::post('see_supplies_all_order_date/','adminController@fetch_all_supplier_order')->name('fetch_all_supplier_date')->middleware('checkRole:1');
Route::post('change_order_status/', 'adminController@update_order_status')->name('update_order_status')->middleware('checkRole:1,2');

Route::post('change_order_status_by_user/', 'ReceiptOrder@update_order_status')->name('update_order_status_by_user')->middleware('checkRole:1,2');

Route::get('order_detail/{orderref}/{idSupplier}/{idCustomer}/{idBinType}/{idBinService}','adminController@order_detail')->name('detailed_order')->middleware('checkRole:1,2');

/** NEW WORLD ORDER **/
Route::get('orders/','order\OrderController@index')->name('orders')->middleware('checkRole:1,2,3');
Route::post('orders/','order\OrderController@findorder_ondate')->name('orders_post')->middleware('checkRole:1,2,3');
Route::get('orders_details/{payment}/{idorders}','order\OrderController@orders_detail')->name('orders_post')->middleware('checkRole:1,2,3');
Route::post('orders_status/','order\OrderController@orders_status')->name('orders_status')->middleware('checkRole:1,2,3');
Route::get('add_orders/','order\OrderController@add_orders_view')->name('add_orders_view')->middleware('checkRole:1,2,3');
Route::post('add_orders/','order\OrderController@add_orders')->name('add_orders')->middleware('checkRole:1,2,3');
Route::get('find_delivery_address/','order\OrderController@find_delivery_address')->name('find_delivery_address')->middleware('checkRole:1,2,3');

Route::get('update_orders/{payment}/{idorders}','order\OrderController@update_orders_view')->name('update_orders_view')->middleware('checkRole:1,2,3');
Route::post('update_orders/','order\OrderController@update_orders')->name('update_orders')->middleware('checkRole:1,2,3');

/** NEW WORLD ORDER : AJAX **/
Route::post('org_based_phone/','order\OrderController@find_organization_based_phone')->name('org_based_phone')->middleware('checkRole:1,2,3');
Route::post('order_based_phone/','order\OrderController@find_orders_based_phone')->name('order_based_phone')->middleware('checkRole:1,2,3');
Route::post('load_order/','order\OrderController@find_order_items_id')->name('load_order')->middleware('checkRole:1,2,3');
Route::post('get_price/','order\OrderController@get_price')->name('get_price')->middleware('checkRole:1,2,3');
Route::post('convert_to_order/','order\OrderController@convert_to_order')->name('convert_to_order')->middleware('checkRole:1,2,3');
Route::post('load_order_based_id/','order\OrderController@load_order_based_id')->name('load_order_based_id')->middleware('checkRole:1,2,3');
Route::post('load_orderitems_based_id/','order\OrderController@load_orderitems_based_id')->name('load_orderitems_based_id')->middleware('checkRole:1,2,3');
Route::post('calculating_total/','order\OrderController@calculating_total')->name('calculating_total')->middleware('checkRole:1,2,3');
Route::post('fetch_delivery_address/','order\OrderController@fetch_delivery_address')->name('fetch_delivery_address')->middleware('checkRole:1,2,3');
Route::post('take_payments/','order\OrderController@take_payments')->name('take_payments')->middleware('checkRole:1,2,3');

/** SCHEDULING  **/
Route::get('job_cards/','scheduling\jobcards@index')->name('jobcards_index')->middleware('checkRole:1,2,3');
Route::get('job_cards/past_uncompleted_orders/','scheduling\jobcards@past_uncompleted_orders')->name('past_uncompleted_orders')->middleware('checkRole:1,2,3');
Route::get('job_cards/today_new_orders/','scheduling\jobcards@new_uncompleted_orders')->name('today_new_orders')->middleware('checkRole:1,2,3');
Route::get('job_cards/completed_orders/','scheduling\jobcards@completed_orders')->name('completed_orders')->middleware('checkRole:1,2,3');
Route::post('job_cards/update_primary_contact/','scheduling\jobcards@update_primary_contact')->name('update_primary_contact')->middleware('checkRole:1,2,3');
Route::get('job_cards/task/{idjobcards}','scheduling\tasks@tasks_by_job')->name('tasks_by_job')->middleware('checkRole:1,2,3');
Route::get('tasks/assign/{idtask}','scheduling\tasks@tasks_assign_view')->name('tasks_assign_view')->middleware('checkRole:1,2,3');
Route::post('tasks/assignment','scheduling\tasks@task_assignment')->name('task_assignment')->middleware('checkRole:1,2,3');
Route::post('tasks/update_endtime','scheduling\tasks@update_endtime')->name('update_endtime')->middleware('checkRole:1,2,3');
Route::post('tasks/update_taskname','scheduling\tasks@update_taskname')->name('update_taskname')->middleware('checkRole:1,2,3');
Route::get('task_lists_index/','scheduling\tasks@list_index')->name('task_lists_index')->middleware('checkRole:1,2,3');
Route::get('tasks/past_uncompleted_tasks/','scheduling\tasks@past_uncompleted_tasks')->name('past_uncompleted_tasks')->middleware('checkRole:1,2,3');
Route::get('tasks/today_new_tasks/','scheduling\tasks@today_new_tasks')->name('today_new_tasks')->middleware('checkRole:1,2,3');
Route::get('tasks/completed_tasks/','scheduling\tasks@completed_tasks')->name('completed_tasks')->middleware('checkRole:1,2,3');

/** NEW WORLD ORDER : SCHEDULING  **/
Route::post('assign_driver','scheduling\tasks@assign_driver')->name('assign_driver')->middleware('checkRole:1,2,3');
Route::post('tasks/update_status','scheduling\tasks@update_status')->name('update_status')->middleware('checkRole:1,2,3');
Route::get('tasks/zone_tasks/','scheduling\tasks@zone_tasks')->name('zone_tasks')->middleware('checkRole:1,2,3');
Route::get('tasks/date_tasks/','scheduling\tasks@date_tasks')->name('date_tasks')->middleware('checkRole:1,2,3');
Route::post('tasks/date_tasks/','scheduling\tasks@date_tasks_process')->name('date_tasks_process')->middleware('checkRole:1,2,3');

/** DRIVERS  **/
Route::get('drivers/','driver\driverController@index')->name('drivers_index')->middleware('checkRole:1,2,3');
Route::get('add_drivers/','driver\driverController@add_view')->name('add_drivers')->middleware('checkRole:1,2,3');
Route::post('add_drivers/','driver\driverController@add_driver')->name('add_drivers_method')->middleware('checkRole:1,2,3');

/** ASSETS  **/
Route::get('assets/','assets\assetController@index')->name('assets_index')->middleware('checkRole:1,2,3');

/** ORGANIZATION  **/
Route::get('organization/','organization\organization@index')->name('organization_index')->middleware('checkRole:1,2,3');

/** PROJECT SITES  **/
Route::get('project_sites/','sites\project_sites@index')->name('projectsites_index')->middleware('checkRole:1,2,3');

/*** PAYPAL ***/
//Route::get('paymentdetails/{idpaymenttemp}','PaypalController@addPayment')->name('addPayment');
//Route::post('paypal', 'PaypalController@postPaymentWithpaypal')->name('paypal');
//Route::get('paypal','PaypalController@getPaymentStatus')->name('status');
//Route::get('paymentstatus','PaypalController@paymentStatus')->name('paymentstatus');
//Route::get('away','PaypalController@away')->name('away');
Route::get('paymentdetails/{idpaymenttemp}','scnpayment@addPayment')->name('addPayment');
Route::post('payment', 'scnpayment@post_details')->name('payment');
Route::get('paymenthost','scnpayment@scn_iframe')->name('host');
Route::get('payment_engagement/','scnpayment@scn_iframe_response')->name('engagement');
Route::get('payment_status/','scnpayment@paymentStatus')->name('payment_status');
Route::post('ajax_hash_post/','scnpayment@ajax_hash_post')->name('ajax_hash_post');
Route::get('away','scnpayment@away')->name('away');


/** INVOICE **/
Route::get('orderservice/{idorderservice}','ReceiptOrder@orderservicedetail')->name('orderdetails')->middleware('checkRole:1,2');
Route::get('test_invoice_mail/{idorderservice}','ReceiptOrder@test_mail')->name('test_mail')->middleware('checkRole:1,2');
Route::get('export/pdf/{idorderservice}','ReceiptOrder@pdfexporter')->name('pdfexporter')->middleware('checkRole:1,2');
Route::get('export/invoice/pdf/{idorderservice}','ReceiptOrder@invoice_pdfexporter')->name('pdfexporter')->middleware('checkRole:1,2');
/** ADMIN SUPER POWER TECHNIQUE **/
Route::get('manage_user/','adminController@usermanagement_view')->name('user_management')->middleware('checkRole:1');
Route::get('approve_user/{idUser}','adminController@aprroveuser')->name('approve_user')->middleware('checkRole:1');

Route::get('active_user/','adminController@activeuser_view')->name('active_user')->middleware('checkRole:1');
Route::get('kick_user/{idUser}','adminController@kickuser')->name('kick_user')->middleware('checkRole:1');

Route::get('banned_user/','adminController@banuser_view')->name('banned_user')->middleware('checkRole:1');
Route::get('reactivate_user/{idUser}','adminController@reactivate_user')->name('reactivate_user')->middleware('checkRole:1');

/** only for jims 
Route::get('approve_user/jim/{idUser}','adminController@jimapprove')->name('jimapprove')->middleware('checkRole:1');**/
/** SUMMARY **/
Route::get('manage_summary/','orderSummary@ordersummary_view')->name('summary_management')->middleware('checkRole:1');
Route::post('see_supplies_by_supplier_summary/','orderSummary@fetch_supplier_order')->name('fetch_all_order_supplies_summary')->middleware('checkRole:1');
Route::get('order_summary/export/pdf/{idSupplier}/{startDate}/{endDate}','orderSummary@pdfexporter')->name('pdfexporter')->middleware('checkRole:1,2');
Route::post('order_summary_selected/export/pdf/','orderSummary@pdfexporterselected')->name('pdfexporterselected')->middleware('checkRole:1,2');

Route::get('test/','Order_Summary_BinType@order_summary_bin_view')->name('bebas')->middleware('checkRole:1');
Route::post('see_supplies_by_bin_summary/','Order_Summary_BinType@fetch_bin_type')->name('fetch_all_bin_type')->middleware('checkRole:1');

Route::get('maps/','Maps@maps_view')->name('maps')->middleware('checkRole:1');
Route::post('insert_to_maps/','Maps@get_the_result')->name('insert_map_name')->middleware('checkRole:1');

Route::get('/clear-cache', function() {
	$exitCode = Artisan::call('config:clear');
	$exitCode = Artisan::call('cache:clear');
	$exitCode = Artisan::call('config:cache');
	return 'DONE'; 
});

/** BOOKING FEE **/
Route::get('booking_fee/','bookingprice@index')->name('bookingfee')->middleware('checkRole:1');
Route::get('booking_fee/{idBookingPrice}','bookingprice@edit_form')->name('edit_form')->middleware('checkRole:1');
Route::get('booking_form/','bookingprice@bookingform_view')->name('booking_form')->middleware('checkRole:1');
Route::post('edit_bookingfee/','bookingprice@edit_bookingfee')->name('edit_bookingfee')->middleware('checkRole:1');

/** MAILING EXTENSION **/
//user_registration
Route::get('mail/user/verify/{name}/{verification_code}','loginController@mail_extension')->name('mail_extension');