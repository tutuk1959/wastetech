$(document).ready(function(){
	$('.loader').hide();
	$('#convert_to_order').attr('disabled', 'disabled');
	$('#new_quote').attr('disabled', 'disabled');
	/* fetch site */
	$('#search_by_phone').on('click', function(e){
		e.preventDefault();
		$('.loader2').show();
		var phone = $('input[name="phone"]').val();
		var sites = $('select[name="sites"]');
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: org_based_phone,
			data:{
				phone:phone,
			},
			success: function(result){
				if(result.status == '1'){
					var idorg = result.result[0].idorg;
					$('input[name="idorg"]').val(idorg);
					console.log('Id org: '+idorg);
					
					/* append to sites select element */
					$("select[name='sites'] option").each(function() {
						$(this).remove();
					});
					$(sites).append('<option value="0">Select available sites</option>');
					$.each(result.result,function(key, value){
						$(sites).append('<option value="' + value.iddelivery_address + '">' + value.unit_number 
						+ ' '+ value.street_name + '-'+ value.suburb +' ' + value.post_code +'</option>');
					});
				}
				
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			if(result.status == '1'){
				swal({
					icon: "success",
					title: "Sites loaded"
				});
				$('.loader2').hide();
			} else {
				$('.loader2').hide();
				swal({
					title: "No existing customer found. Add a new one?",
					icon: "warning",
					buttons: [
						'No',
						'Yes'
					],
					dangerMode: true,
				}).then(function(isConfirm) {
					if (isConfirm) {
						swal({
							icon: "info",
							title: "Please fill customer information to form below"
						});
						return 
					} else {
						$('input[name="phone"]').val('');
						
						location.reload(true);
					}
				});
			}
			
			
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for fetch sites:' + result);
		});
	});
	
	/* use site */
	$('select[name="sites"]').on('change', function(e){
		var iddelivery_address = $(this).val();
		$(document).find('input[name="iddelivery_address"]').val(iddelivery_address);
	});
	
	/* Fetch order */
	$('#use_site').on('click', function(e){
		e.preventDefault();
		var idorg = $(document).find('input[name="idorg"]').val();
		var idaddress = $(document).find('input[name="iddelivery_address"]').val();
		var order = $('select[name="quote"]');
		
		$('.loader2').show();
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: order_based_phone,
			data:{
				idorg:idorg,
				idaddress:idaddress
			},
			success: function(result){
				
				/* append to quote select element */
				$("select[name='quote'] option").each(function() {
					$(this).remove();
				});
				$(order).append('<option value="0">Select available order</option>');
				$.each(result.result,function(key, value){
					var actual_bin_price = value.subtotal / value.quantity;
					var isquote = null;
					if(value.is_quote == '1'){
						isquote = "Quote";
					} else if(value.is_quote == '0') {
						isquote = "Order";
					} else {
						isquote = "Order";
					}
					
					if(value.is_paid == '1'){
						ispaid = "Paid";
					} else if(value.is_quote == '0') {
						ispaid = "Unpaid";
					} else {
						ispaid = "Unpaid";
					}
					
					const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
					var dateObj = new Date(value.orderDate);
					var month = monthNames[dateObj.getMonth()];
					var day = String(dateObj.getDate()).padStart(2, '0');
					var year = dateObj.getFullYear();
					var d = month  + '\n'+ day  + ',' + year;
					
					$(order).append('<option data-order-service="'+value.idOrderService+'" value="' + value.idorderitems + '">' + isquote 
					+ ' #'+ value.paymentUniqueCode + ' '+d+' - $'+ actual_bin_price +', ' + value.size +' / '+ ispaid+'</option>');
				});
				
				
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			var iddelivery_address = $('input[name="iddelivery_address"]').val();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			});
			$.ajax({
				type: "POST",
				url: fetch_delivery_address,
				data:{
					iddelivery_address:iddelivery_address
				},
				success: function(result){
					if(result.status == '1'){
						$('input[name="client_name"]').val(result.result.organization_name);
						$('input[name="bill_address"]').val(result.result.address);
						$('input[name="email_address"]').val(result.result.email_address);
						$('input[name="work_phone"]').val(result.result.phone);
						$('input[name="street_number"]').val(result.result.unit_number);
						$('input[name="street_name"]').val(result.result.street_name);
						$('input[name="postcode"]').val(result.result.post_code);
						$('input[name="suburb"]').val(result.result.suburb);
					}
				
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function(result ) {
				return
			})
			.fail(function( result ) {
				console.log('Failed AJAX Call for fetch order:' + result);
			});
		
			if(result.result != null && result.result != undefined && result.result != ''){
				swal({
					icon: "success",
					title: "Order loaded"
				});
				$('.loader2').hide();
			} else {
				swal({
					icon: "error",
					title: "No order found"
				});
				$('.loader2').hide();
			}
			
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for fetch order:' + result);
		});
		
	});
	
	/* use site */
	$('select[name="quote"]').on('change', function(e){
		var idorderitems = $(this).val();
		var idorderservice = $(this).find('option:selected').attr('data-order-service');
		$(document).find('input[name="idorderitems"]').val(idorderitems);
		$(document).find('input[name="idorderservice"]').val(idorderservice);
	});
	
	/* click copy to order */
	$('#copy_to_order').on('click', function(e){
		var idorderitems = $('input[name="idorderitems"]').val();
		var iddelivery_address = $('input[name="iddelivery_address"]').val();
		$('.loader2').show();
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: load_order,
			data:{
				idorderitems:idorderitems,
				iddelivery_address:iddelivery_address
			},
			success: function(result){
				$('input[name="client_name"]').val(result.result.organization_name);
				$('select[name="binsize"]').val(result.result.idBinSize);
				$('input[name="bill_address"]').val(result.result.address);
				$('input[name="email_address"]').val(result.result.email_address);
				$('input[name="work_phone"]').val(result.result.phone);
				
				$('input[name="street_number"]').val(result.result.unit_number);
				$('input[name="street_name"]').val(result.result.street_name);
				$('input[name="postcode"]').val(result.result.post_code);
				$('input[name="suburb"]').val(result.result.suburb);
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			if(result.result != null && result.result != undefined && result.result != ''){
				swal({
					icon: "success",
					title: "Order loaded"
				});
				$('.loader2').hide();
			} else {
				swal({
					icon: "error",
					title: "No order found"
				});
				$('.loader2').hide();
			}
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for Load :' + result);
		});
	});
	
	/* find price */
	$('#price').on('click', function(e){
		var idorderitems = $('input[name="idorderitems"]').val();
		var id = $('input[name="iddelivery_address"]').val();
		var binsize = $('select[name="binsize"]').val();
		var postcode = $('input[name="postcode"]').val();
		$('.loader1').show();
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		console.log(get_price);
		$.ajax({
			type: "POST",
			url: get_price,
			data:{
				id:id,
				binsize:binsize,
				postcode:postcode
			},
			success: function(result){
				
				$('input[name="price"]').val(result.result.price);
				$('input[name="isupdate"]').val(result.isupdate);
				$('input[name="idbinservice"]').val(result.result.idBinService);
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			if(result.result != null && result.result != undefined && result.result != ''){
				swal({
					icon: "success",
					title: "Price found"
				});
				$(document).find('#convert_to_order').removeAttr("disabled");;
				$(document).find('#new_quote').removeAttr("disabled");
				$('.loader1').hide();
			} else {
				swal({
					icon: "error",
					title: "No price found"
				});
				$('.loader1').hide();
			}
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for Load :' + result);
		});
	});
	
	/* convert to order */
	$('#convert_to_order').on('click', function(e){
		var uniqueid = $('input[name="uniqueid"]').val();
		var idorg = $('input[name="idorg"]').val();
		var iddelivery_address = $('input[name="iddelivery_address"]').val();
		var is_quote = 0;
		var date = $('input[name="date"]').val();
		var idbinservice = $('input[name="idbinservice"]').val();
		var isupdate = $('input[name="isupdate"]').val();
		var binsize = $('select[name="binsize"]').val();
		var delivery_date = $('input[name="delivery_date"]').val();
		var collection_date = $('input[name="collection_date"]').val();
		var price = $('input[name="price"]').val();
		var delivery_comments = $('textarea[name="delivery_comments"]').val();
		
		var work_phone = $('input[name="work_phone"]').val();
		var email_address = $('input[name="email_address"]').val();
		var bill_address = $('input[name="bill_address"]').val();
		var client_name = $('input[name="client_name"]').val();
		var street_number = $('input[name="street_number"]').val();
		var street_name = $('input[name="street_name"]').val();
		var postcode = $('input[name="postcode"]').val();
		var suburb = $('input[name="suburb"]').val();
		
		$('.loader1').show();
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: convert_to_order,
			data:{
				uniqueid:uniqueid,
				idorg:idorg,
				iddelivery_address:iddelivery_address,
				is_quote:is_quote,
				date:date,
				idbinservice:idbinservice,
				isupdate:isupdate,
				binsize:binsize,
				delivery_date:delivery_date,
				collection_date:collection_date,
				price:price,
				delivery_comments:delivery_comments,
				work_phone:work_phone,
				email_address:email_address,
				bill_address:bill_address,
				client_name:client_name,
				street_number:street_number,
				street_name:street_name,
				postcode:postcode,
				suburb:suburb,
			},
			success: function(result){
				if(result.status == '1'){
					$('input[name="ordermain_payment"]').val(result.tblorderservice);
				}
				
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			if(result.status == '1'){
				/* fetch order master and put it on the table */
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					}
				});
				$.ajax({
					type: "POST",
					url: load_order_based_id,
					data:{
						idorder:result.tblorderservice,
					},
					success: function(result){
						const monthNames = ["January", "February", "March", "April", "May", "June",
						"July", "August", "September", "October", "November", "December"];
						var dateObj = new Date(result.result.orderDate);
						var month = monthNames[dateObj.getMonth()];
						var day = String(dateObj.getDate()).padStart(2, '0');
						var year = dateObj.getFullYear();
						var d = month  + '\n'+ day  + ',' + year;
						
						$('input[name="org_payment"]').val(result.result.idorg);
						$('#invoice_id').html(result.result.paymentUniqueCode);
						$('#order_date').html(d);
						$('#clients_name').html(result.result.organization_name);
						$('#payment_bill_address').html(result.result.address);
						$('#payment_email_address').html(result.result.email_address);
						$('#payment_work_phone').html(result.result.phone);
					},
					error: function( data, status, error ) { 
						console.log(data);
						console.log(status);
						console.log(error);
					}
				})
				.done(function(result ) {
					
				})
				.fail(function( result ) {
					console.log('Failed AJAX Call for Load :' + result);
				});
				
				/********************************************************************************/
				
				
				/* fetch order items and put it on the table */
				var order_items_table_row = $('tbody.items_wrapper').find('tr').clone();
				var order_items_table = $('tbody.items_wrapper');
				$('tbody.items_wrapper').find('tr').remove();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					}
				});
				$.ajax({
					type: "POST",
					url: load_orderitems_based_id,
					data:{
						idorder:result.tblorderservice,
					},
					success: function(result){
						$.each(result.result,function(key, value){
							const monthNames = ["January", "February", "March", "April", "May", "June",
							"July", "August", "September", "October", "November", "December"];
							var dateObj = new Date(value.delivery_date);
							var month = monthNames[dateObj.getMonth()];
							var day = String(dateObj.getDate()).padStart(2, '0');
							var year = dateObj.getFullYear();
							var d = month  + '\n'+ day  + ',' + year;
							
							var dateObj_1 = new Date(value.collection_date);
							var month_1 = monthNames[dateObj_1.getMonth()];
							var day_1 = String(dateObj_1.getDate()).padStart(2, '0');
							var year_1 = dateObj_1.getFullYear();
							var o = month_1  + '\n'+ day_1  + ',' + year_1;
							
						
							$(order_items_table).append('<tr>'
								+'<input type="hidden" name="orderitemsid_payment[]" value="'+value.idorderitems+'"/>'+
								' '+'<td>Jobs Id <br /><span id="jobs_id">'+value.unique_id+'</span></td>'+
								' '+'<td>Delivery Date <br /><span id="delivery_date">'+d+'</span></td>'+
								' '+'<td>Collection Date <br /><span id="collection_date">'+o+'</span></td>'+
								' '+'<td>Delivery Address <br /><span id="delivery_address">'+value.unit_number+' '+value.street_name+' '+value.post_code+'</span></td>'+
								' '+'<td>Binsize <br /><span id="binsize">'+value.size+'</span></td>'+
								' '+'<input type="hidden" name="price[]" value="'+value.subtotal+'" />'+
							'</tr>');
						});
					
					},
					error: function( data, status, error ) { 
						console.log(data);
						console.log(status);
						console.log(error);
					}
				})
				.done(function(result ) {
					
					/* calculating GST and so on */
					var ordermain_payment = $('input[name="ordermain_payment"]').val();
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
						}
					});
					$.ajax({
						type: "POST",
						url: calculating_total,
						data:{
							ordermain_payment:ordermain_payment,
						},
						success: function(result){
							$('#subtotal').html(result.subtotal);
							$('#bookingprice').html(result.bookingfee);
							$('#gst').html(Number(result.gst).toFixed(2));
							$('#total_charge').html(result.total);
						},
						error: function( data, status, error ) { 
							console.log(data);
							console.log(status);
							console.log(error);
						}
					})
					.done(function(result ) {
						$('#take_payment').removeAttr('disabled');
					})
					.fail(function( result ) {
						console.log('Failed AJAX Call for calculating GST :' + result);
					});
					
					/**********************************************************/
				})	
				.fail(function( result ) {
					console.log('Failed AJAX Call for Load to table :' + result);
				});
				
				/********************************************************************************/
				/* Finished */
				$('.loader1').hide();
				swal({
					title: "Add more order items?",
					icon: "warning",
					buttons: [
						'No',
						'Yes'
					],
					dangerMode: true,
				}).then(function(isConfirm) {
					if (isConfirm) {
						$('#convert_to_order').attr('disabled', 'disabled');
						$('#new_quote').attr('disabled', 'disabled');
						return 
					} else {
						$('input[name="client_name"]').val('');
						$('input[name="bill_address"]').val('');
						$('input[name="email_address"]').val('');
						$('input[name="work_phone"]').val('');
						$('input[name="street_number"]').val('');
						$('input[name="street_name"]').val('');
						$('input[name="postcode"]').val('');
						$('input[name="suburb"]').val('');
						
						$('input[name="price"]').val('');
						$('#convert_to_order').attr('disabled', 'disabled');
						$('#new_quote').attr('disabled', 'disabled');
					}
				});
			} else {
				swal({
					icon: "error",
					title: "Some of the fields are missing"
				});
			}
			
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for Add order :' + result);
		});
	});
	
	/* Take payments */
	$('#take_payment').on('click', function(e){
		var ordermain_payment = $('input[name="ordermain_payment"]').val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: take_payments,
			data:{
				ordermain_payment:ordermain_payment
			},
			success: function(result){
				
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			$('input[name="client_name"]').val('');
			$('input[name="bill_address"]').val('');
			$('input[name="email_address"]').val('');
			$('input[name="work_phone"]').val('');
			$('input[name="street_number"]').val('');
			$('input[name="street_name"]').val('');
			$('input[name="postcode"]').val('');
			$('input[name="suburb"]').val('');
			
			$('input[name="price"]').val('');
					
			if(result.status == '1'){
				swal({
					icon: "success",
					title: "Success!"
				});
			} else {
				swal({
					icon: "error",
					title: "Take payment error"
				});
			}
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for Payments :' + result);
		});
		
	});
	
	/* click load */
	$('#load').on('click', function(e){
		var idorderitems = $('input[name="idorderitems"]').val();
		var idorderservice = $('input[name="idorderservice"]').val();
		$('input[name="ordermain_payment"]').val(idorderservice);

		$('.loader2').show();
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		/* fetch order master and put it on the table */
		$.ajax({
			type: "POST",
			url: load_order_based_id,
			data:{
				idorder:idorderservice,
			},
			success: function(result){
				const monthNames = ["January", "February", "March", "April", "May", "June",
				"July", "August", "September", "October", "November", "December"];
				var dateObj = new Date(result.result.orderDate);
				var month = monthNames[dateObj.getMonth()];
				var day = String(dateObj.getDate()).padStart(2, '0');
				var year = dateObj.getFullYear();
				var d = month  + '\n'+ day  + ',' + year;
				
				$('input[name="org_payment"]').val(result.result.idorg);
				$('#invoice_id').html(result.result.paymentUniqueCode);
				$('#order_date').html(d);
				$('#clients_name').html(result.result.organization_name);
				$('#payment_bill_address').html(result.result.address);
				$('#payment_email_address').html(result.result.email_address);
				$('#payment_work_phone').html(result.result.phone);
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for Load :' + result);
		});
		
		/********************************************************************************/
		/* fetch order items and put it on the table */
		var order_items_table_row = $('tbody.items_wrapper').find('tr').clone();
		var order_items_table = $('tbody.items_wrapper');
		$('tbody.items_wrapper').find('tr').remove();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: load_orderitems_based_id,
			data:{
				idorder:idorderservice,
			},
			success: function(result){
				$.each(result.result,function(key, value){
					const monthNames = ["January", "February", "March", "April", "May", "June",
					"July", "August", "September", "October", "November", "December"];
					var dateObj = new Date(value.delivery_date);
					var month = monthNames[dateObj.getMonth()];
					var day = String(dateObj.getDate()).padStart(2, '0');
					var year = dateObj.getFullYear();
					var d = month  + '\n'+ day  + ',' + year;
					
					var dateObj_1 = new Date(value.collection_date);
					var month_1 = monthNames[dateObj_1.getMonth()];
					var day_1 = String(dateObj_1.getDate()).padStart(2, '0');
					var year_1 = dateObj_1.getFullYear();
					var o = month_1  + '\n'+ day_1  + ',' + year_1;
					
				
					$(order_items_table).append('<tr>'
						+'<input type="hidden" name="orderitemsid_payment[]" value="'+value.idorderitems+'"/>'+
						' '+'<td>Jobs Id <br /><span id="jobs_id">'+value.unique_id+'</span></td>'+
						' '+'<td>Delivery Date <br /><span id="delivery_date">'+d+'</span></td>'+
						' '+'<td>Collection Date <br /><span id="collection_date">'+o+'</span></td>'+
						' '+'<td>Delivery Address <br /><span id="delivery_address">'+value.unit_number+' '+value.street_name+' '+value.post_code+'</span></td>'+
						' '+'<td>Binsize <br /><span id="binsize">'+value.size+'</span></td>'+
						' '+'<input type="hidden" name="price[]" value="'+value.subtotal+'" />'+
					'</tr>');
				});
			
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			/* calculating GST and so on */
			var ordermain_payment = idorderservice;
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			});
			$.ajax({
				type: "POST",
				url: calculating_total,
				data:{
					ordermain_payment:ordermain_payment,
				},
				success: function(result){
					$('#subtotal').html(result.subtotal);
					$('#bookingprice').html(result.bookingfee);
					$('#gst').html(Number(result.gst).toFixed(2));
					$('#total_charge').html(result.total);
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function(result ) {
				$('#take_payment').removeAttr('disabled');
			})
			.fail(function( result ) {
				console.log('Failed AJAX Call for calculating GST :' + result);
			});
			/**********************************************************/
			$('html, body').animate({
				scrollTop: $("#invoice_section").offset().top
			}, 500);
			
			$('.loader2').hide();
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for Load order items :' + result);
		});
	});
	
	/* New Qupte */
	$('#new_quote').on('click', function(e){
		var uniqueid = $('input[name="uniqueid"]').val();
		var idorg = $('input[name="idorg"]').val();
		var iddelivery_address = $('input[name="iddelivery_address"]').val();
		var is_quote = 1;
		var date = $('input[name="date"]').val();
		var idbinservice = $('input[name="idbinservice"]').val();
		var isupdate = $('input[name="isupdate"]').val();
		var binsize = $('select[name="binsize"]').val();
		var delivery_date = $('input[name="delivery_date"]').val();
		var collection_date = $('input[name="collection_date"]').val();
		var price = $('input[name="price"]').val();
		var delivery_comments = $('textarea[name="delivery_comments"]').val();
		
		var work_phone = $('input[name="work_phone"]').val();
		var email_address = $('input[name="email_address"]').val();
		var bill_address = $('input[name="bill_address"]').val();
		var client_name = $('input[name="client_name"]').val();
		var street_number = $('input[name="street_number"]').val();
		var street_name = $('input[name="street_name"]').val();
		var postcode = $('input[name="postcode"]').val();
		var suburb = $('input[name="suburb"]').val();
		
		$('.loader1').show();
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: convert_to_order,
			data:{
				uniqueid:uniqueid,
				idorg:idorg,
				iddelivery_address:iddelivery_address,
				is_quote:is_quote,
				date:date,
				idbinservice:idbinservice,
				isupdate:isupdate,
				binsize:binsize,
				delivery_date:delivery_date,
				collection_date:collection_date,
				price:price,
				delivery_comments:delivery_comments,
				work_phone:work_phone,
				email_address:email_address,
				bill_address:bill_address,
				client_name:client_name,
				street_number:street_number,
				street_name:street_name,
				postcode:postcode,
				suburb:suburb,
			},
			success: function(result){
				if(result.status == '1'){
					$('input[name="ordermain_payment"]').val(result.tblorderservice);
				}
				
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			if(result.status == '1'){
				/* fetch order master and put it on the table */
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					}
				});
				$.ajax({
					type: "POST",
					url: load_order_based_id,
					data:{
						idorder:result.tblorderservice,
					},
					success: function(result){
						const monthNames = ["January", "February", "March", "April", "May", "June",
						"July", "August", "September", "October", "November", "December"];
						var dateObj = new Date(result.result.orderDate);
						var month = monthNames[dateObj.getMonth()];
						var day = String(dateObj.getDate()).padStart(2, '0');
						var year = dateObj.getFullYear();
						var d = month  + '\n'+ day  + ',' + year;
						
						$('input[name="org_payment"]').val(result.result.idorg);
						$('#invoice_id').html(result.result.paymentUniqueCode);
						$('#order_date').html(d);
						$('#clients_name').html(result.result.organization_name);
						$('#payment_bill_address').html(result.result.address);
						$('#payment_email_address').html(result.result.email_address);
						$('#payment_work_phone').html(result.result.phone);
					},
					error: function( data, status, error ) { 
						console.log(data);
						console.log(status);
						console.log(error);
					}
				})
				.done(function(result ) {
					
				})
				.fail(function( result ) {
					console.log('Failed AJAX Call for Load :' + result);
				});
				
				/********************************************************************************/
				
				
				/* fetch order items and put it on the table */
				var order_items_table_row = $('tbody.items_wrapper').find('tr').clone();
				var order_items_table = $('tbody.items_wrapper');
				$('tbody.items_wrapper').find('tr').remove();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					}
				});
				$.ajax({
					type: "POST",
					url: load_orderitems_based_id,
					data:{
						idorder:result.tblorderservice,
					},
					success: function(result){
						$.each(result.result,function(key, value){
							const monthNames = ["January", "February", "March", "April", "May", "June",
							"July", "August", "September", "October", "November", "December"];
							var dateObj = new Date(value.delivery_date);
							var month = monthNames[dateObj.getMonth()];
							var day = String(dateObj.getDate()).padStart(2, '0');
							var year = dateObj.getFullYear();
							var d = month  + '\n'+ day  + ',' + year;
							
							var dateObj_1 = new Date(value.collection_date);
							var month_1 = monthNames[dateObj_1.getMonth()];
							var day_1 = String(dateObj_1.getDate()).padStart(2, '0');
							var year_1 = dateObj_1.getFullYear();
							var o = month_1  + '\n'+ day_1  + ',' + year_1;
							
						
							$(order_items_table).append('<tr>'
								+'<input type="hidden" name="orderitemsid_payment[]" value="'+value.idorderitems+'"/>'+
								' '+'<td>Jobs Id <br /><span id="jobs_id">'+value.unique_id+'</span></td>'+
								' '+'<td>Delivery Date <br /><span id="delivery_date">'+d+'</span></td>'+
								' '+'<td>Collection Date <br /><span id="collection_date">'+o+'</span></td>'+
								' '+'<td>Delivery Address <br /><span id="delivery_address">'+value.unit_number+' '+value.street_name+' '+value.post_code+'</span></td>'+
								' '+'<td>Binsize <br /><span id="binsize">'+value.size+'</span></td>'+
								' '+'<input type="hidden" name="price[]" value="'+value.subtotal+'" />'+
							'</tr>');
						});
					
					},
					error: function( data, status, error ) { 
						console.log(data);
						console.log(status);
						console.log(error);
					}
				})
				.done(function(result ) {
					
					/* calculating GST and so on */
					var ordermain_payment = $('input[name="ordermain_payment"]').val();
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
						}
					});
					$.ajax({
						type: "POST",
						url: calculating_total,
						data:{
							ordermain_payment:ordermain_payment,
						},
						success: function(result){
							$('#subtotal').html(result.subtotal);
							$('#bookingprice').html(result.bookingfee);
							$('#gst').html(Number(result.gst).toFixed(2));
							$('#total_charge').html(result.total);
						},
						error: function( data, status, error ) { 
							console.log(data);
							console.log(status);
							console.log(error);
						}
					})
					.done(function(result ) {
						
					})
					.fail(function( result ) {
						console.log('Failed AJAX Call for calculating GST :' + result);
					});
					
					/**********************************************************/
				})	
				.fail(function( result ) {
					console.log('Failed AJAX Call for Load to table :' + result);
				});
				
				/********************************************************************************/
				/* Finished */
				$('.loader1').hide();
				swal({
					title: "Add more order items?",
					icon: "warning",
					buttons: [
						'No',
						'Yes'
					],
					dangerMode: true,
				}).then(function(isConfirm) {
					if (isConfirm) {
						$('#convert_to_order').attr('disabled', 'disabled');
						$('#new_quote').attr('disabled', 'disabled');
						return 
					} else {
						$('input[name="client_name"]').val('');
						$('input[name="bill_address"]').val('');
						$('input[name="email_address"]').val('');
						$('input[name="work_phone"]').val('');
						$('input[name="street_number"]').val('');
						$('input[name="street_name"]').val('');
						$('input[name="postcode"]').val('');
						$('input[name="suburb"]').val('');
						
						$('input[name="price"]').val('');
						$('#convert_to_order').attr('disabled', 'disabled');
						$('#new_quote').attr('disabled', 'disabled');
					}
				});
			} else {
				swal({
					icon: "error",
					title: "Some of the fields are missing"
				});
			}
			
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call for Add order :' + result);
		});
	});
	
	/******************************************************************************************/
	/************** IMPROVEMENTS 2 JUNE 2020 BY PUTU *****************************************/
	$('#use_as_delivery_address').attr('checked', false);
	/* Use as billing address */ 
	$('#use_as_delivery_address').on('change', function(){
		if(this.checked){
			var billing_address = $('#bill_address').val();
			if(billing_address != ''){
				var postcode = billing_address.match(/[\d]{4}$/);
				var street_name =billing_address.replace(/[\d]|(\/)/g, '');
				var street_number = billing_address.match(/^[0-9]{1,2}|[:.,-/]+(\d){1,2} /g);
				console.log(street_number[0]);
				$('#street_number').val(street_number[0]);
				$('#street_name').val(street_name);
				$('#postcode').val(postcode[0]);
			}
		}
	});
});