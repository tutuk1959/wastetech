$(document).ready(function(){
	$('#update_assign').on('click', function(e){
		e.preventDefault();
		var idtask = $('input[name="idtaks"]').val();
		
		var ul_driver = $('#main_send');
		var ul_truck = $('#truck_send');
		if($(ul_driver).find('li[data-driver-id]').length > 0){
			var driver = $(ul_driver).find('li[data-driver-id]').attr('data-driver-id');
		} else {
			var driver = null;
		}
		
		if($(ul_truck).find('li[data-truck-id]').length > 0){
			var truck = $(ul_truck).find('li[data-truck-id]').attr('data-truck-id');
		} else {
			var truck = null;
		}
		
		console.log(idtask);
		console.log(driver);
		console.log(truck);
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: tasks_assign,
			data:{
				idtask:idtask,
				truck: truck, 
				driver:driver
			},
			success: function(result){
				console.log(result);
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			if(result.status == '1'){
				swal({
					icon: "success",
					title: "Task assigned",
				});
			} else {
				swal({
					icon: "error",
					title: "Error on updating task : "+ result.message,
				});
			}
		})
		.fail(function( result ) {
			swal({
				icon: "error",
				title: "Error on updating task : Programming bug",
			});
		});
	})
	
	$('.assign_driver').on('click', function(e){
		e.preventDefault();
		var ul_driver = $(this).parent().find('.driver_target');
		var iddriver = $(this).parent().find('input[name="iddriver"]').val();
		
		var task = []
		if($(ul_driver).children('li[data-task-id]').length > 0){
			
			$(ul_driver).children('li[data-task-id]').each(function(i) {
				task.push($(this).attr('data-task-id'));
			});
		} else {
			var task = null;
		}
		
		console.log(ul_driver);
		console.log(iddriver);
		console.log(task);
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: assign_driver,
			data:{
				iddriver:iddriver,
				task: task
			},
			success: function(result){
				console.log(result);
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			if(result.status == '1'){
				swal({
					icon: "success",
					title: "Task assigned",
				});
			} else {
				swal({
					icon: "error",
					title: "Error on updating task : "+ result.message,
				});
			}
		})
		.fail(function( result ) {
			swal({
				icon: "error",
				title: "Error on updating task : Programming bug",
			});
		});
	})
});