$(document).ready(function(){
	$('iframe#paynow').contents().find("head").append(
			$("<style type='text/css'>"+
			"body {"+
				"background-color: #efefef;"+
			"} "+
			" </style>")
	);
	$('.collapse [name="selectall"]').change(function(){
		var prop = $(this).prop('checked');
		
		if(prop == true){
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', true);
			})
		} else {
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', false);
			})
		}
	});
	
	$('.collapse [name="unselectall"]').change(function(){
		var prop = $(this).prop('checked');
		
		if(prop == true){
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', false);
			})
		} 
	});

	var start = moment().startOf('month');
	var end =  moment().startOf('month').add(14, 'days');
	var end_month =  moment().endOf('month');
	
	var start_lastmonth = moment().startOf('month').subtract(1,'months');
	var end_lastmonth =  moment().subtract(1,'months').endOf('month');
	
	function cb(start, end) {
        $('input[name="order_management_datepicker"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        $('input[name="start_order_date"]').val(start.format('YYYY-MM-DD'));
        $('input[name="end_order_date"]').val(end.format('YYYY-MM-DD'));
	}
	
	function cc(start, end) {
		$('input[name="fromdate"]').val(start.format('DD/MM/YYYY'));
		$('input[name="start_order_date"]').val(start.format('YYYY-MM-DD'));
	}
	
	function cd(start, end) {
		$('input[name="todate"]').val(end.format('DD/MM/YYYY'));
		$('input[name="end_order_date"]').val(end.format('YYYY-MM-DD'));
	}
	
	$('input[name="order_management_datepicker"]').daterangepicker({
		startDate: start_lastmonth,
        endDate: end_lastmonth,
		autoApply:true,
        ranges: {
			'First Fortnight': [moment().startOf('month'), moment().startOf('month').add(14, 'days')],
			'Second Fortnight': [moment().subtract(1,'months').startOf('month').add(15, 'days'), moment().subtract(1,'months').endOf('month')],
        },
		locale: {
			format: 'DD/MM/YYYY'
		}
	}, cb);
	cb(start_lastmonth, end_lastmonth);
	
	$('input[name="fromdate"]').daterangepicker({
		singleDatePicker: true,
		startDate: start,
		opens: 'center', 
		ranges: {
			'First Fortnight (Start)': [moment().startOf('month'), moment()],
			'First Fortnight (End)': [moment().startOf('month').add(14, 'days'), moment()],
			'Second Fortnight (Start)': [moment().subtract(1,'months').startOf('month').add(15, 'days'), moment()],
			'Second Fortnight (End)': [moment().subtract(1,'months').endOf('month'), moment()],
        },
		locale: {
			format: 'DD/MM/YYYY'
		}
	}, cc)
	cc(start, end_month);
	
	$('input[name="todate"]').daterangepicker({
		 singleDatePicker: true,
		 startDate: end_month,
		 opens: 'center', 
		 ranges: {
			'First Fortnight (Start)': [moment().startOf('month'), moment()],
			'First Fortnight (End)': [moment().startOf('month').add(14, 'days'), moment()],
			'Second Fortnight (Start)': [moment().subtract(1,'months').startOf('month').add(15, 'days'), moment()],
			'Second Fortnight (End)': [moment().subtract(1,'months').endOf('month'), moment()],
        },
		locale: {
			format: 'DD/MM/YYYY'
		}
	}, cd)
	cd(start, end_month);
	

	$('#per_transaction').hide();
	$('input[name="ordersummary_checkbox_all"]').on('change', function(){
		if($(this).is(':checked')){
			var checkboxes = $(document).find('input[name="ordersummary_checkbox[]"]');
			checkboxes.each(function(){
				$(this).prop('checked', true);
			})
			$('#per_transaction').show();
		} else {
			var checkboxes = $(document).find('input[name="ordersummary_checkbox[]"]');
			checkboxes.each(function(){
				$(this).prop('checked', false);
			})
			$('#per_transaction').hide();
		}
		
	});
	
	
	$('input[name="ordersummary_checkbox[]"]').on('change', function(){
		if($(this).is(':checked')){
			$('#per_transaction').show();
		} else {
			$('#per_transaction').hide();
		}
		
	});
	
	$('#per_transaction').on('click', function(e){
		e.preventDefault();
		var uniquecode = [];
		var startDate = $('input[name="startDate"]').val();
		var endDate = $('input[name="endDate"]').val();
		var idsupplier = $('input[name="idsupplier"]').val();
		
		var checkboxes = $(document).find('input[name="ordersummary_checkbox[]"]');
		checkboxes.each(function(){
			if($(this).is(':checked')){
				uniquecode.push($(this).val());
			}
		});
		//alert(uniquecode);
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: generate_pdf_checbox_url,
			data:{
				uniquecode:uniquecode,
				startDate: startDate, 
				endDate:endDate, 
				idsupplier:idsupplier
			},
			success: function(result){
				console.log(result);
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			var element = document.createElement('a');
			element.setAttribute('href', result);
			element.setAttribute('download', 'Ezyskips Online Report.pdf');
			element.style.display = 'none';
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call :' + result);
		});
	})
	
	$('.order-table').DataTable();
	$('.datatable').DataTable();
	
	var start = moment().add(7, 'days');
	var start_1 = moment().add(1, 'days');
	$(document).find('.form-datepicker').daterangepicker({
		singleDatePicker: true,
		locale: {
			format: 'DD-MM-YYYY'
		}
	});
	
	$(document).find('.form-datepicker-delivery').daterangepicker({
		startDate: start_1,
		singleDatePicker: true,
		locale: {
			format: 'DD-MM-YYYY'
		}
	});
	
	$(document).find('.form-datepicker-collection').daterangepicker({
		startDate: start,
		singleDatePicker: true,
		locale: {
			format: 'DD-MM-YYYY'
		}
	});
	
	$('.bin_address').select2();
	
	$("#delete_more").attr('disabled',true);
	$("#add_more").on('click',function(){
		$("#delete_more").attr('disabled',false);
		var markup = $('#bin_items').find('.row_items:last').clone();
		$("#bin_items").append(markup);
	});
	
	$("#delete_more").on('click',function(){
		var markup = $('#bin_items').find('.row_items:last');
		$(markup).remove();
	});
	
	$('.add_order input[name="all_total"]').val(0);
	$('.add_order input#qty').val(1);
	
	$(document).on('change','.address',function (e) {
		e.preventDefault();
		var parent =  $(this).parent().parent().parent();
		var organization = $('select[name="supplier"]').val();
		var wastetype = parent.find('select#wastetype').val();
		var binsize = parent.find('select#binsize').val();
		var id = parent.find('select#address').val();
		var qty = parent.find('input#qty').val();
		var date = $('input[name="date"]').val();
		var all_total = parseInt($('input[name="all_total"]').val());
		
		if(date == null || date == 'undefinded'){
			return alert('Date must not empty!');
		}
		
		if(organization == null || organization == 'undefinded'){
			return alert('Organization must not empty!');
		}
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: '/get_price',
			data:{
				organization:organization,
				wastetype: wastetype, 
				binsize:binsize, 
				id:id, 
				qty:qty,
				date:date
			},
			success: function(result){
				console.log(result);
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
		if(result.price == undefined){
				alert('No results. Select another bin type or bin size');
			} else {
				
				var subtotal = qty * result.price;
				var price = result.price;
				console.log('Price : '+price);
				console.log('Subtotal : '+subtotal);
				parent.find('input#price').val(price);
				parent.find('input#subtotal').val(subtotal);
				parent.find('input#idbinservice').val(result.idBinService);
				all_total = parseInt(all_total) + subtotal;
				$('input[name="all_total"]').val(all_total);
				$('strong.total').html('$ '+all_total);
			}
			
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call :' + result);
		});
	});
});