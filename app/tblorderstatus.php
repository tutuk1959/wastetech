<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblorderstatus extends Model
{
 	protected $table = 'tblorderstatus';
 	public $timestamps = false;
}
