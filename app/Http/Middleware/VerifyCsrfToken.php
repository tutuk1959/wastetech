<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
		'/api/task_master_date',
		'/api/task_master',
		'/api/task_master_uncompleted',
		'/api/task_master_completed',
		'/api/task_progress',
		'/api/task_detail_id',
		'/api/driver_detail',
		'/api/update_task_status',
		'/api/update_task_has_status',
		'/api/change_password',
		'/api/task_master_pending',
		'/api/task_master_non_pending',
		'/api/update_task_has_time',
		'/api/update_task_geo_information',
		'/api/update_task_geo_distance',
    ];
}
