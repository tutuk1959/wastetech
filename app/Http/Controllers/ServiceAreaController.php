<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblservicearea;
use App\tblsupplier;
use App\tbluse;
use App\tbluserservicearea;
use App\tblbookingservice;
use App\CsvData;
use Illuminate\Support\Facades\DB;
use Validator;
use Redirect;
use App\Http\Requests\CsvImportRequest;
use Maatwebsite\Excel\Facades\Excel;


class ServiceAreaController extends Controller
{
	public function index($result = null){
		$idUser = 	session('idUser');
		$childArea = null;
		$childServiceArea = null;
		$mainServiceArea = DB::table('tbluser')
			->leftJoin('tblsupplier', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblservicearea', 'tblsupplier.mainServiceArea', '=' , 'tblservicearea.idArea')
			->select('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->first();
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress','tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress','tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		$serviceAreaParent = DB::table('tblservicearea')
							->select('idArea', 'zipcode','area', 'parentareacode')
							->where(['parentareacode' => '0000'])
							->orderBy('area','asc')
							->get();
		
		if(!is_null($serviceAreaParent)){
			foreach ($serviceAreaParent as $row){
				$childArea[$row->idArea][$row->zipcode] = DB::table('tblservicearea')
					->select('idArea', 'zipcode','area', 'parentareacode')
					->where(['parentareacode' => $row->zipcode])
					->get();
			}
			
			if(!is_null($childArea)){
				foreach($childArea as $ca){
					foreach ($ca as $sca){
						foreach ($sca as $ssca){
							$childServiceArea[$ssca->parentareacode][$ssca->idArea]= DB::table('tbluserservicearea')
									->select('idUser', 'idSupplier','idServiceArea', 'serviceAreaParent')
									->where(['idUser' => $idUser])
									->get();
						}
					}
				}
				
			}
			return view('detail_service_area', ['supplierData' => $supplierData, 'mainServiceArea'=> $mainServiceArea,'serviceAreaParent' => $serviceAreaParent, 
			'childArea' => $childArea, 'childServiceArea' => $childServiceArea,'result' => $result]);
		} else {
			return view('detail_service_area', ['supplierData' => $supplierData, 'mainServiceArea'=> $mainServiceArea,'serviceAreaParent' => $serviceAreaParent, 
			'childArea' => $childArea, 'childServiceArea' => $childServiceArea,'result' => $result]);
		}
	}
	
	
	public function userdetails_byId(Request $request){
		$idUser = $request['idUser'];
		$mainServiceArea = DB::table('tbluser')
			->leftJoin('tblsupplier', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblservicearea', 'tblsupplier.mainServiceArea', '=' , 'tblservicearea.idArea')
			->select('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->first();
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress','tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress','tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		$serviceAreaParent = DB::table('tblservicearea')
							->select('idArea', 'zipcode','area', 'parentareacode')
							->where(['parentareacode' => '0000'])
							->get();

		foreach ($serviceAreaParent as $row){
			$childArea[$row->idArea][$row->zipcode] = DB::table('tblservicearea')
							->select('idArea', 'zipcode','area', 'parentareacode')
							->where(['parentareacode' => $row->zipcode])
							->get();
			
		}
		
		foreach($childArea as $ca){
			foreach ($ca as $sca){
				foreach ($sca as $ssca){
					$childServiceArea[$ssca->parentareacode][$ssca->idArea]= DB::table('tbluserservicearea')
							->select('idUser', 'idSupplier','idServiceArea', 'serviceAreaParent')
							->where(['idUser' => $idUser])
							->get();
				}
			}
		}
		return view('userdetails', ['supplierData' => $supplierData, 'mainServiceArea'=> $mainServiceArea,'serviceAreaParent' => $serviceAreaParent, 
			'childArea' => $childArea, 'childServiceArea' => $childServiceArea]);
	}

	public function showResult(Request $request){
		$result['status'] = $request['status'];
		$result['message'] = $request['message'];

		$idUser = 	session('idUser');
		$mainServiceArea = DB::table('tbluser')
			->leftJoin('tblsupplier', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblservicearea', 'tblsupplier.mainServiceArea', '=' , 'tblservicearea.idArea')
			->select('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->first();
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress','tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress','tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		$serviceAreaParent = DB::table('tblservicearea')
							->select('idArea', 'zipcode','area', 'parentareacode')
							->where(['parentareacode' => '0000'])
							->get();

		foreach ($serviceAreaParent as $row){
			$childArea[$row->idArea][$row->zipcode] = DB::table('tblservicearea')
							->select('idArea', 'zipcode','area', 'parentareacode')
							->where(['parentareacode' => $row->zipcode])
							->get();
			
		}
		
		foreach($childArea as $ca){
			foreach ($ca as $sca){
				foreach ($sca as $ssca){
					$childServiceArea[$ssca->parentareacode][$ssca->idArea]= DB::table('tbluserservicearea')
							->select('idUser', 'idSupplier','idServiceArea', 'serviceAreaParent')
							->where(['idUser' => $idUser])
							->get();
				}
			}
		}
		return view('detail_service_area', ['supplierData' => $supplierData, 'mainServiceArea'=> $mainServiceArea,'serviceAreaParent' => $serviceAreaParent, 
			'childArea' => $childArea, 'childServiceArea' => $childServiceArea,'result' => $result]);
		
	}

	public function validateForm($websiteAdminContact, $websiteAdminPhone, $websiteAdminMobile, $customerServicesEmail1){
		$error = array();
		if ($websiteAdminContact == ''){
			$error[] = 'Admin contact name must not empty';
		} 
		if ($websiteAdminPhone == ''){
			$error[] = 'Admin phone number must not empty';
		} 
		if ($websiteAdminMobile == ''){
			$error[] = 'Admin mobile phone number must not empty';
		}
		if ($customerServicesEmail1 == ''){
			$error[] = 'Customer service primary email must not empty';
		}
		
		return $error;
	}

	public function validatePassword($password, $confirmPassword){
		if ($password != $confirmPassword){
			$error[] = 'Please re-type password. Password mismatch.';
		}
		return $error;
	}
	public function editSupplierDetails (Request $request){
		$idSupplier = $request['idSupplier'];
		$idUser = $request['idUser'];
		$websiteAdminContact = $request['websiteAdminContact']; //required
		$customerServiceContact = $request['customerServicesContact']; 
		$websiteAdminPhone = $request['websiteAdminPhone']; //required
		$customerServicePhone = $request['customerServicePhone'];
		$websiteAdminMobile = $request['websiteAdminMobile']; //required
		$customerServiceMobile = $request['customerServiceMobile'];
		$customerServicesEmail1 = $request['customerServicesEmail1']; //required
		$customerServicesEmail2 = $request['customerServicesEmail2'];
		$abn = $request['abn'];
		$username = $request['username'];
		$fullAddress = $request['fullAddress'];
		if (isset($request['openSaturday'])){
			$openSaturday = 1;
		} else {
			$openSaturday = 0;
		}

		if (isset($request['openSunday'])){
			$openSunday = 1;
		} else {
			$openSunday = 0;
		}

		if ($request->isMethod('post') == 'POST'){
			$validate = Validator::make($request->all(), [
				'websiteAdminContact' => 'required|string',
				'websiteAdminPhone' => 'required|string',
				'websiteAdminMobile' => 'required|string',
				'customerServicesEmail1' => 'required|email',
			],[
				'customerServicesEmail1.email' => 'Please input a valid email address on Email 1 Field',
				'websiteAdminContact.string' => 'Please input a proper contact name',
				'websiteAdminPhone.numeric' => 'Please input a proper website admin phone number',
				'websiteAdminMobile.numeric' => 'Please input a proper website admin monile number',
			]);

			if($validate->fails()){
				return Redirect::route('details_service_zone')->withErrors($validate)->withInput();
			}else {
				$supplier = new tblsupplier();
				$supplier::where(['idSupplier' => $idSupplier])
				->update([
					'contactName' => $websiteAdminContact, 
					'phonenumber' => $websiteAdminPhone, 
					'email' => $customerServicesEmail1, 
					'mobilePhone' => $websiteAdminMobile,
					'email2' => $customerServicesEmail2,
					'isOpenSaturday' => $openSaturday,
					'isOpenSunday' => $openSunday,
					'customerServiceContact' => $customerServiceContact,
					'customerServicePhone' => $customerServicePhone,
					'customerServiceMobile' => $customerServiceMobile,
					'fullAddress' => $fullAddress,
					'abn' => $abn
				]);
				if ($supplier){
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating supplier data');
					return redirect()->route('details_service_zone');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update supplier data failed');
					return redirect()->route('details_service_zone');
				}
			}
		}
	}

	public function editUserDetails(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$username = $request['username'];
		$password = $request['password'];
		$confirmPassword = $request['confirmPassword'];

		if ($request->isMethod('post') == 'POST'){
			$validate = Validator::make($request->all(), [
				'username' => 'required|string',
				'password' => 'required|string|same:confirmPassword',
				'confirmPassword' => 'required|string',
			],[
				'password.same' => 'Password mismatch. Type the same password on the both field',
			]);

			if($validate->fails()){
				return Redirect::route('details_service_zone')->withErrors($validate)->withInput();
			} else {
				$user = new tbluse();
				$user::where(['idUser' => $idUser])
				->update([
					'idSupplier' => $idSupplier, 
					'username' => $username, 
					'password' => md5($password)
				]);
				if ($user){
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating supplier password');
					return redirect()->route('details_service_zone');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update supplier password failed');
					return redirect()->route('details_service_zone');
				}
			}
		} 
	}

	public function editServiceArea(Request $request){
		$zipcode = $request['postalcode'];
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idArea = $request['idArea'];

		$userservicearea = new tbluserservicearea();
		$userservicearea->where([
				'idUser' => $idUser,
				'idSupplier' => $idSupplier,
				'serviceAreaParent' => $zipcode
			])->delete();

		if(!is_null($idArea)){
			foreach($idArea as $area){
				$userservicearea = new tbluserservicearea();
				$userservicearea->idUser = $idUser;
				$userservicearea->idSupplier = $idSupplier;
				$userservicearea->idServiceArea = $area;
				$userservicearea->serviceAreaParent = $zipcode;
				if($userservicearea->save()){
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating supplier service area');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update supplier service area failed');
				}
			}
			return redirect()->route('details_service_zone');
		} else {
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', 'Success updating supplier service area');
			return redirect()->route('details_service_zone');
		}
		
		/*$check = DB::table('tbluserservicearea')
				->select('idUserServiceArea', 'idUser')
				->where([
					'idUser' => $idUser,
					'idSupplier' => $idSupplier,
					'serviceAreaParent' => $zipcode
				])
				->get();
		if(!is_null($check)){
			$userservicearea = new tbluserservicearea();
			$userservicearea->where([
				'idUser' => $idUser,
				'idSupplier' => $idSupplier,
				'serviceAreaParent' => $zipcode
			])->delete();

			foreach($idArea as $area){
				$userservicearea->idUser = $idUser;
				$userservicearea->idSupplier = $idSupplier;
				$userservicearea->idServiceArea = $area;
				$userservicearea->serviceAreaParent = $zipcode;

				if($userservicearea->save()){
					$result['status'] = 'success';
					$result['message'] = 'Success updating user service area';
				} else {
					$result['status'] = 'danger';
					$result['message'] = 'Update user service area failed';
				}
			}
		} else {
			$userservicearea = new tbluserservicearea();
			foreach($idArea as $area){
				$userservicearea->idUser = $idUser;
				$userservicearea->idSupplier = $idSupplier;
				$userservicearea->idServiceArea = $area;
				$userservicearea->serviceAreaParent = $zipcode;

				if($userservicearea->save()){
					$result['status'] = 'success';
					$result['message'] = 'Success updating user service area';
				} else {
					$result['status'] = 'danger';
					$result['message'] = 'Update user service area failed';
				}
			}
		}*/
	}
	
	public function csv_importer_view(){
		return view('servicearea_importer');
	}
	
	public function parseImport(CsvImportRequest $request){
		$path = $request->file('csv_file')->getRealPath();
		
		if ($request->has('header')) {
			$data = Excel::load($path, function($reader) {})->get()->toArray();
		} else {
			$data = array_map('str_getcsv', file($path));
		}
		
		
		if (count($data) > 0) {
			if ($request->has('header')) {
				$csv_header_fields = [];
				foreach ($data[0] as $key => $value) {
					$csv_header_fields[] = $key;
				}
			}
			$csv_data = array_slice($data, 0, 100);

			$csv_data_file = CsvData::create([
				'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
				'csv_header' => $request->has('header'),
				'csv_data' => json_encode($data)
			]);
		} else {
			return redirect()->back();
		}
		return view('servicearea_import_fields', compact( 'csv_header_fields', 'csv_data', 'csv_data_file'));
	}

	public function processImport(Request $request){
		$data = CsvData::find($request->csv_data_file_id);
		$csv_data = json_decode($data->csv_data, true);
		foreach ($csv_data as $row) {
			$servicearea = new tblservicearea();
			foreach (config('app.db_fields') as $index => $field) {
				if ($data->csv_header) {
					$servicearea->$field = $row[$request->fields[$field]];
				} else {
					$servicearea->$field = $row[$request->fields[$index]];
				}
			}
			$servicearea->save();
		}
		$request->session()->flash('status', 'success');
		$request->session()->flash('message', 'Importer success');
		return redirect()->route('import_view');
	}
}
