<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblbintype;
use App\tblsize;
use App\tblbinservice;
use App\tblbinserviceoptions;
use App\tblbinserviceupdates;
use App\tbluse;
use App\supplier;
use App\tblbinnondelivery;
use App\tblorderservice;
use App\tblbinservicestok;
use Validator;
use Redirect;
use Illuminate\Support\Facades\DB;

class DirtWaste extends Controller
{
	public function index($result = null){
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('l d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$binservicestok = null;
		$offset = time();

		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);


		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicestok[$v->idBinService] = $this->getstockupdatedprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}

		return view('dirt_waste',['supplierData' => $supplierData, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset,'serviceOptions' => $serviceOptions, 'nonDelivery' => $nonDelivery, 
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice,'binservicestok' => $binservicestok]);
	}

	public function showResult(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('l d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$binservicestok = null;
		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicestok[$v->idBinService] = $this->getstockupdatedprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}

		return view('dirt_waste',['supplierData' => $supplierData, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'binservicestok' => $binservicestok]);
	}

	public function showForm(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$binservicestok = null;
		$bintype = $request['bintype'];
		$binsize = $request['binsize'];
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('d-m-Y', strtotime('now'));

		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicestok[$v->idBinService] = $this->getstockupdatedprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}

		return view('dirt_waste_form',['supplierData' => $supplierData, 'bintype' => $bintype,'binsize' => $binsize, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'binservicestok' => $binservicestok]);
	}

	public function showFormResult(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		$bintype = $request['bintype'];
		$binsize = $request['binsize'];
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$binservicestok = null;
		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicestok[$v->idBinService] = $this->getstockupdatedprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}

		return view('dirt_waste_form',['supplierData' => $supplierData, 'bintype' => $bintype,'binsize' => $binsize, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'binservicestok' => $binservicestok]);
	}

	private function getBinSize (){
		$sizes = DB::table('tblsize')
				->select('idSize','size', 'dimensions')
				->orderBy('idSize', 'asc')
				->get();
		return $sizes;
	}

	private function getSupplierData($idUser){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}

	private function getServiceOptions($idUser, $idSupplier){
		$serviceOptions = DB::table('tblbinserviceoptions')
						->select('idUser', 'idSupplier', 'idBinType', 'extraHireagePrice', 'extraHireageDays', 'excessWeightPrice')
						->where([
							'idUser' => $idUser,
							'idSupplier' => $idSupplier,
							'idBinType' => 5
						])
						->first();
		return $serviceOptions;
	}

	private function getNonDeliveryDays($idUser, $idSupplier){
		$nonDelivery = DB::table('tblbinnondelivery')
					->select('date')
					->where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 5
					])
					->get();
		return $nonDelivery;
	}

	private function getbinservicebaseprice($idSupplier){
		$binserviceprice = DB::table('tblbinservice')
						->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
							,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize')
						->where([
							'tblbinservice.idSupplier' => $idSupplier,
							'tblbinservice.idBinType' => 5,
							])
						->get();
		return $binserviceprice;
	}

	private function getbinservicediscprice($idBinService){
		$binserviceprice = DB::table('tblbinserviceupdates')
						->select('tblbinserviceupdates.price','tblbinserviceupdates.stock','tblbinserviceupdates.date')
						->where([
							'tblbinserviceupdates.idBinService' => $idBinService,
							])
						->get();
		return $binserviceprice;
	}
	
	private function getstockupdatedprice($idBinService){
		$binservicestockupdates = DB::table('tblbinservicestok')
				->select('tblbinservicestok.stock','tblbinservicestok.date')
				->where([
					'tblbinservicestok.idBinService' => $idBinService
				])
				->get();
		return $binservicestockupdates;
	}
	
	private function getspecificbaseprice($idSupplier, $idBinSize){
		$binserviceprice = DB::table('tblbinservice')
						->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
							,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize','tblbinservice.default_stock AS default_stock')
						->where([
							'tblbinservice.idSupplier' => $idSupplier,
							'tblbinservice.idBinSize' => $idBinSize,
							'tblbinservice.idBinType' => 5,
							])
						->get();
		return $binserviceprice;
	}

	private function validateForm($extraHireage, $extraHireageDays){
		$error = array();
		if ($extraHireage == ''){
			$error[] = 'Extra hireage must not empty';
		} 
		if ($extraHireageDays == ''){
			$error[] = 'Extra hireage days must not empty';
		} 
		return $error;
	}

	private function validatePricing($basePrice, $baseStock){
		$error = array();
		if ($basePrice == ''){
			$error[] = 'Please fill a specific base price for your bin';
		} 
		if ($baseStock == ''){
			$error[] = 'Please fill a specific stock for your bin';
		} 
		return $error;
	}

	public function editMiscDetail(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$extraHireage = $request['extraHireage'];
		$extraHireageDays = $request['extraHireageDays'];
		$excessWeight = 0; 

		if ($request->isMethod('post') == 'POST'){
			$error = $this->validateForm($extraHireage, $extraHireageDays);
			$validate = Validator::make($request->all(), [
				'extraHireage' => 'required|numeric',
				'extraHireageDays' => 'required|numeric',
			],[
				'extraHireage.numeric' => 'Extra hireage price should be numeric',
				'extraHireageDays.numeric' => 'Extra hireage days should be numeric',
			]);
			if($validate->fails()){
				return Redirect::route('dirt_waste')->withErrors($validate)->withInput();
			} else {
				$check = DB::table('tblbinserviceoptions')
						->select('idUser','idSupplier', 'idBinType')
						->where([
							'idUser' => $idUser,
							'idSupplier' => $idSupplier, 
							'idBinType' => 5
						])
						->first();
				if (!is_null($check)){
					$serviceOptions = new tblbinserviceoptions();
					$serviceOptions::where(['idUser' => $idUser, 'idSupplier' => $idSupplier, 'idBinType' => $idBinType])
					->update([
						'extraHireagePrice' => $extraHireage, 
						'extraHireageDays' => $extraHireageDays, 
						'excessWeightPrice' => $excessWeight
					]);
					if ($serviceOptions){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating rates optional data');
						return redirect()->route('dirt_waste');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update rates optional data failed');
						return redirect()->route('dirt_waste');
					}
				} else {
					$serviceOptions = new tblbinserviceoptions();
					$serviceOptions->idUser = $idUser;
					$serviceOptions->idSupplier = $idSupplier;
					$serviceOptions->idBinType = 5;
					$serviceOptions->extraHireagePrice = $extraHireage;
					$serviceOptions->extraHireageDays = $extraHireageDays;
					$serviceOptions->excessWeightPrice = $excessWeight;
					if ($serviceOptions->save()){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating rates optional data');
						return redirect()->route('dirt_waste');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update rates optional data failed');
						return redirect()->route('dirt_waste');
					}
				}
			}
		}
	}

	public function editNonDeliveryDays(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$date[] = $request['date'];
		$nonDelivery[] = $request['nondelivery'];

		$nondeliverydays = array('date' => $date, 'nondelivery' => $nonDelivery);
		/** Delete first **/
		$tblbinnondelivery = new tblbinnondelivery();
		//check for deletion first
		if (is_null($nondeliverydays['nondelivery'][0])){
			foreach($nondeliverydays['date'] as $k => $v){
				foreach($v as $r => $d){
					$check_1 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 1,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check_1)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 1,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
					
					$check_2 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 2,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check_2)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 2,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
					
					$check_3 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 3,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check_3)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
					
					$check_4 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 4,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check_4)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 4,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
					
					$check_5 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 5,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check_5)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 5,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}

					if($deletedRows){
						$result['status'] = 'success';
						$result['message'] = 'Success updating non delivery data';
					} else {
						$result['status'] = 'notif';
						$result['message'] = 'Update non delivery days failed';
					}
				}
			}
		} else{
			foreach($nondeliverydays['date'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 1,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 1,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
					
					$check2 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 2,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check2)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 2,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
					
					$check3 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 3,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check3)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
					
					$check4 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 4,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check4)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 4,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
					
					$check5 = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 5,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check5)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 5,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
				}
			}
			
			/** Then insert **/
			foreach($nondeliverydays['nondelivery'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
							->select('date')
							->where([
								'idSupplier' => $idSupplier,
								'idUser' => $idUser,
								'idBinType' => 1,
								'date' => $nondeliverydays['date'][$k][$r]
							])
							->first();
					if (!is_null($check)){
						$result['status'] = 'warning';
						$result['message'] = 'Date exists';
					} else{
						$tblbinnondelivery = new tblbinnondelivery();
						$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
						$tblbinnondelivery->idSupplier = $idSupplier;
						$tblbinnondelivery->idUser = $idUser;
						$tblbinnondelivery->idBinType = 1;
						if($tblbinnondelivery->save()){
							$result['status'] = 'success';
							$result['message'] = 'Success updating non delivery data';
						} else {
							$result['status'] = 'notif';
							$result['message'] = 'Update non delivery days failed';
						}
					}
					
					$check2 = DB::table('tblbinnondelivery')
							->select('date')
							->where([
								'idSupplier' => $idSupplier,
								'idUser' => $idUser,
								'idBinType' => 2,
								'date' => $nondeliverydays['date'][$k][$r]
							])
							->first();
					if (!is_null($check2)){
						$result['status'] = 'warning';
						$result['message'] = 'Date exists';
					} else{
						$tblbinnondelivery = new tblbinnondelivery();
						$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
						$tblbinnondelivery->idSupplier = $idSupplier;
						$tblbinnondelivery->idUser = $idUser;
						$tblbinnondelivery->idBinType = 2;
						if($tblbinnondelivery->save()){
							$result['status'] = 'success';
							$result['message'] = 'Success updating non delivery data';
						} else {
							$result['status'] = 'notif';
							$result['message'] = 'Update non delivery days failed';
						}
					}
					
					$check3 = DB::table('tblbinnondelivery')
							->select('date')
							->where([
								'idSupplier' => $idSupplier,
								'idUser' => $idUser,
								'idBinType' => 3,
								'date' => $nondeliverydays['date'][$k][$r]
							])
							->first();
					if (!is_null($check3)){
						$result['status'] = 'warning';
						$result['message'] = 'Date exists';
					} else{
						$tblbinnondelivery = new tblbinnondelivery();
						$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
						$tblbinnondelivery->idSupplier = $idSupplier;
						$tblbinnondelivery->idUser = $idUser;
						$tblbinnondelivery->idBinType = 3;
						if($tblbinnondelivery->save()){
							$result['status'] = 'success';
							$result['message'] = 'Success updating non delivery data';
						} else {
							$result['status'] = 'notif';
							$result['message'] = 'Update non delivery days failed';
						}
					}
					
					$check4 = DB::table('tblbinnondelivery')
							->select('date')
							->where([
								'idSupplier' => $idSupplier,
								'idUser' => $idUser,
								'idBinType' => 4,
								'date' => $nondeliverydays['date'][$k][$r]
							])
							->first();
					if (!is_null($check4)){
						$result['status'] = 'warning';
						$result['message'] = 'Date exists';
					} else{
						$tblbinnondelivery = new tblbinnondelivery();
						$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
						$tblbinnondelivery->idSupplier = $idSupplier;
						$tblbinnondelivery->idUser = $idUser;
						$tblbinnondelivery->idBinType = 4;
						if($tblbinnondelivery->save()){
							$result['status'] = 'success';
							$result['message'] = 'Success updating non delivery data';
						} else {
							$result['status'] = 'notif';
							$result['message'] = 'Update non delivery days failed';
						}
					}
					
					$check5 = DB::table('tblbinnondelivery')
							->select('date')
							->where([
								'idSupplier' => $idSupplier,
								'idUser' => $idUser,
								'idBinType' => 5,
								'date' => $nondeliverydays['date'][$k][$r]
							])
							->first();
					if (!is_null($check5)){
						$result['status'] = 'warning';
						$result['message'] = 'Date exists';
					} else{
						$tblbinnondelivery = new tblbinnondelivery();
						$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
						$tblbinnondelivery->idSupplier = $idSupplier;
						$tblbinnondelivery->idUser = $idUser;
						$tblbinnondelivery->idBinType = 5;
						if($tblbinnondelivery->save()){
							$result['status'] = 'success';
							$result['message'] = 'Success updating non delivery data';
						} else {
							$result['status'] = 'notif';
							$result['message'] = 'Update non delivery days failed';
						}
					}
				}
			}
			
		}

		return redirect()->route('dirt.waste.schedule', ['status' => $result['status'], 'message' => $result['message']]);
	}

	public function editBinServicePrice(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$idBinSize = $request['idBinSize'];
		$basePrice = $request['basePrice'];
		$date[] = $request['date'];
		$binPrice[] = $request['binPrice'];
		$baseStock = $request['baseStock'];
		$stock[] = $request['stock'];

		/* Main pricing data */
		$mainData = array(
			'date' => $date,
			'binPrice' => $binPrice,
			'stock' => $stock
		);
		

		if ($request->isMethod('POST') == 'POST'){
			
			$error = $this->validatePricing($binPrice, $stock);
			if(count($error) > 0){
				$result['status'] = 'danger';
				$result['message'] = $error;
				return redirect()->route('dirt.waste.pricing.form', ['status' => $result['status'], 'message' => $result['message']]);
			} else {
				/* check base price first */
				
				$checkBasePrice = DB::table('tblbinservice')
							->select('idBinService', 'price', 'stock')
							->where([
								'idSupplier' => $idSupplier,
								'idBinSize' => $idBinSize,
								'idBinType' => $idBinType
							])
							->first();

				if (!is_null($checkBasePrice)){
					$validate = Validator::make($request->all(), [
						'basePrice' => 'required|numeric',
						'baseStock' => 'required|numeric',
					],[
						'basePrice.numeric' => 'The bin base price should be on numeric value',
						'baseStock.numeric' => 'The bin base stock should be on numeric value',
					]);
					
					if($validate->fails()){
						return Redirect::route('dirt.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
					}
					
					$binservice = new tblbinservice();

					$updates = $binservice::where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType,
					])
					->update(['price' => $basePrice, 'stock' => $baseStock,'default_stock' => $baseStock]);

					$checkupdatesSuccessful = DB::table('tblbinservice')
						->select('idBinService', 'price', 'stock')
						->where([
							'idSupplier' => $idSupplier,
							'idBinSize' => $idBinSize,
							'idBinType' => $idBinType,
							'price' => $basePrice,
							'default_stock' => $baseStock
						])
						->first();
					if(!is_null($checkupdatesSuccessful)){
						$tempResultBasePrice = 'success';
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating bin base rate data');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update bin base price failed');
						return redirect()->route('dirt_waste');
					}

					/*fetch last id */
					$lastid = DB::table('tblbinservice')
					->select('idBinService', 'price', 'stock')
					->where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType
					])
					->first();

				} else {
					$validate = Validator::make($request->all(), [
						'basePrice' => 'required|numeric',
						'baseStock' => 'required|numeric',
					],[
						'basePrice.numeric' => 'The bin base price should be on numeric value',
						'baseStock.numeric' => 'The bin base stock should be on numeric value',
					]);
					if($validate->fails()){
						return Redirect::route('dirt.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
					}
					
					$binservice = new tblbinservice();
					$binservice->idSupplier = $idSupplier;
					$binservice->idBinType = $idBinType;
					$binservice->idBinSize = $idBinSize;
					$binservice->price = $basePrice;
					$binservice->stock = $baseStock;
					$binservice->default_stock = $baseStock;

					if ($binservice->save()){
						$tempResultBasePrice = 'success';
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating bin base rate data');
						/*fetch last id */
						$lastid = DB::table('tblbinservice')
							->select('idBinService', 'price', 'stock')
							->orderBy('idBinService', 'desc')
							->first();
					} else {
						$tempResultBasePrice = 'danger';
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update bin base rate failed');
						return redirect()->route('dirt_waste');
					}
				}
				
				/* validate numeric discount value */
				foreach ($mainData['binPrice'] as $k => $v){
					foreach($v  as $d){
						$array_binprice[] = $d;
					}
					
				}
				foreach ($mainData['stock'] as $k => $v){
					foreach($v  as $d){
						$array_stock[] = $d;
					}
				}
				/* price */
				$validate = Validator::make(compact('array_binprice'), [
					'array_binprice' => 'array',
					'array_binprice.*' => 'numeric',
				],[
					'array_binprice.*.numeric' => 'The bin price * should has numeric value',
				]);
				if($validate->fails()){
					return Redirect::route('dirt.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
					/* stock */
				$validate = Validator::make(compact('array_stock'), [
					'array_stock' => 'array',
					'array_stock.*' => 'numeric',
				],[
					'array_stock.*.numeric' => 'The bin stock * should has numeric value',
				]);
				if($validate->fails()){
					return Redirect::route('dirt.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
				
				/* check and update the discount on specific date*/
				if (!is_null($mainData['binPrice'])){
					foreach($mainData['binPrice'] as $k => $v){
						foreach($v as $r => $d){
							if(!is_null($mainData['binPrice'][$k][$r])){
								$checkDisc = DB::table('tblbinserviceupdates')
									->select('idBinService', 'price', 'stock')
									->where([
										'idBinService' => $lastid->idBinService,
										'date' => $mainData['date'][$k][$r],
									])
								->first();

								if (!is_null($checkDisc)){
									$binserviceupdates = new tblbinserviceupdates();
									$update = $binserviceupdates::where([
										'idBinService' => $lastid->idBinService,
										'date' => $mainData['date'][$k][$r],
									])
									->update(['price' => $mainData['binPrice'][$k][$r], 'stock' => $mainData['stock'][$k][$r]]);

									if ($update) {
										$request->session()->flash('status', 'success');
										$request->session()->flash('message', 'Success updating rate data');
									} else {
										$request->session()->flash('status', 'danger');
										$request->session()->flash('message', 'Update rate data failed');
									}
								} else {
									if(!is_null($checkBasePrice)){
										if(($checkBasePrice->price != $mainData['binPrice'][$k][$r]) || ($checkBasePrice->stock != $mainData['stock'][$k][$r])){
											$binserviceupdates = new tblbinserviceupdates();
											$binserviceupdates->idBinService = $lastid->idBinService;
											$binserviceupdates->price = $mainData['binPrice'][$k][$r];
											$binserviceupdates->stock = $mainData['stock'][$k][$r];
											$binserviceupdates->date = $mainData['date'][$k][$r];
											if ($binserviceupdates->save()){
												$request->session()->flash('status', 'success');
												$request->session()->flash('message', 'Success updating rate data');
											} else {
												$request->session()->flash('status', 'danger');
												$request->session()->flash('message', 'Update rate data failed');
											}
										} else {
											$request->session()->flash('status', 'success');
											$request->session()->flash('message', 'Success updating rate data');
										}
									}
								}
								
								$checkStock = DB::table('tblbinservicestok')
									->select('idBinService',  'stock')
									->where([
										'idBinService' => $lastid->idBinService,
										'date' => $mainData['date'][$k][$r],
									])
								->first();

								if (!is_null($checkStock)){
									$binservicestok = new tblbinservicestok();
									$update = $binservicestok::where([
										'idBinService' => $lastid->idBinService,
										'date' => $mainData['date'][$k][$r],
									])
									->update(['stock' => $mainData['stock'][$k][$r]]);

									if ($update) {
										$request->session()->flash('status', 'success');
										$request->session()->flash('message', 'Success updating rate data');
									
									} else {
										$request->session()->flash('status', 'danger');
										$request->session()->flash('message', 'Update rate data failed');
									}
								} else {
									if(!is_null($checkBasePrice)){
										if(($checkBasePrice->price != $mainData['binPrice'][$k][$r]) || ($checkBasePrice->stock != $mainData['stock'][$k][$r])){
											$binservicestok = new tblbinservicestok();
											$binservicestok->idBinService = $lastid->idBinService;
											$binservicestok->stock = $mainData['stock'][$k][$r];
											$binservicestok->date = $mainData['date'][$k][$r];
											if ($binservicestok->save()){
												$request->session()->flash('status', 'success');
												$request->session()->flash('message', 'Success updating rate data');
											} else {
												$request->session()->flash('status', 'danger');
												$request->session()->flash('message', 'Update rate data failed');
											}
										} else {
											$request->session()->flash('status', 'success');
											$request->session()->flash('message', 'Success updating rate data');
										}
									}
								}
							}
						}
					}
				}
				

				if ($tempResultBasePrice == 'success'){
					$result['status'] = 'success';
					$result['message'] = 'Success updating rates data';
				} else {
					$result['status'] = 'notif';
					$result['message'] = 'Update rates data failed';
					
				}
				return redirect()->route('dirt_waste');
			}
		}
	}

	public function resetRates(Request $request){
		$idUser = session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$idBinType = $request['bintype'];
		$idBinSize = $request['binsize'];

		$binservice = new tblbinservice();
		$binserviceupdates = new tblbinserviceupdates();
		$orderservice = new tblorderservice();

		$getbinservice = DB::table('tblbinservice')
						->select('idBinService')
						->where([
							'idSupplier' => $supplierData->idSupplier,
							'idBinSize' => $idBinSize,
							'idBinType' => $idBinType
						])
						->first();
		if (!is_null($getbinservice)){
			$deletedRowsBinUpdates = DB::table('tblbinserviceupdates')
			->where([
				'idBinService' => $getbinservice->idBinService
			])->delete();
			
			$deletedRowsBinStok = DB::table('tblbinservicestok')
			->where([
				'idBinService' => $getbinservice->idBinService
			])->delete();
			
			$deletedRowsBin = $binservice::where([
				'idSupplier' => $supplierData->idSupplier,
				'idBinSize' => $idBinSize,
				'idBinType' => $idBinType
			])->update(['price' => 0, 'stock' => 0, 'default_stock' => 0]);

			if ($deletedRowsBin){
				$request->session()->flash('status', 'success');
				$request->session()->flash('message', 'Success deleting rates data');
			} else {
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Delete rates data failed');
			}
		} else {
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'No data selected');
		}
		return redirect()->route('dirt_waste');
	}
}
