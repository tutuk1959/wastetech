<?php

namespace App\Http\Controllers\driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\tbldrivers;
use Validator;
use URL;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Log;

class driverController extends Controller
{
	public function __construct(){
		
	}
	
	/** 
	* Main Listing
	*/
	public function index(){
		$list = $this->get_all_drivers();
		return view('driver.list', ['drivers' => $list]);
	}
	
	/** 
	* Add driver view
	*/
	public function add_view(){
		return view('driver.add');
	}
	
	
	/**
	* Get all list of drivers
	*/
	private function get_all_drivers(){
		$drivers = DB::table('tbldrivers')
		->get();
		
		return $drivers;
	}
	
	/**
	* Add row method 
	*/
	public function add_driver(Request $request){
		$validate = Validator::make($request->all(), [
			'name' => 'required',
			'license' => 'required',
			'username' => 'required',
			'password' => 'required',
			'confirm_password' => 'same:password',
			'email_address' => 'required',
		]);
		
		if($validate->fails()){
			return Redirect::back()->withErrors($validate)->withInput($request->all());
		} else {
			$tbldrivers = new tbldrivers();
			$tbldrivers->name = $request['name'];
			$tbldrivers->licence = $request['license'];
			$tbldrivers->username = $request['username'];
			$tbldrivers->password = $request['password'];
			$tbldrivers->status = 1;
			$tbldrivers->email_address = $request['email_address'];
			if($tbldrivers->save()){
				$request->session()->flash('status', 'success');
				$request->session()->flash('message', sprintf('Save driver success'));
				return redirect()->route('drivers_index');
			} else {
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', sprintf('Save driver failed'));
				return redirect()->back()->withInput($request->all());
			}
		}
	}
}
