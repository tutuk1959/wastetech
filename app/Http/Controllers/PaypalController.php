<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;
use App\tblcustomer;
use App\tblorderservice;
use Illuminate\Support\Facades\Mail;

//-------------------------
//All Paypal Details class
//-------------------------
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaypalController extends Controller{
    private $_api_context;

    public function __construct(){
        //------------------------   
        //setup PayPal api context
        //------------------------
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function addPayment(Request $request){
        $idpaymenttemp = $request['idpaymenttemp'];

        $paid = DB::table('tblpaymenttemp')
                ->select('paid')
                ->where(['idPaymentTemp' => $idpaymenttemp])
                ->first();
		
		 if (is_null($paid)){
            \Session::put('error','No purchase traced');
            return Redirect::route('paymentstatus');
        }
		
        if ($paid->paid == 1){
            \Session::put('error','Payment failed. This purchase has already closed');
            return Redirect::route('paymentstatus');
        }

        $purchasedetails = DB::table('tblpaymenttemp')
                        ->leftJoin('tblbinservice','tblpaymenttemp.idBinHire','=','tblbinservice.idBinService')
                        ->select('tblbinservice.idBinService','tblbinservice.idSupplier','tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price',
                            'tblpaymenttemp.deliveryDate', 'tblpaymenttemp.collectionDate', 'tblpaymenttemp.zipcode')
                        ->where([
                                'tblpaymenttemp.idPaymentTemp' => $idpaymenttemp
                            ])
                        ->orderBy('tblbinservice.idBinService','tblbinservice.idSupplier','tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price',
                            'tblpaymenttemp.deliveryDate', 'tblpaymenttemp.collectionDate', 'tblpaymenttemp.zipcode')
                        ->first();

        if (!is_null($purchasedetails)){
			$deliveryYear = date('Y', strtotime($purchasedetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($purchasedetails->deliveryDate));
			$deliveryDay = date('d', strtotime($purchasedetails->deliveryDate));
			
			$collectionYear = date('Y', strtotime($purchasedetails->collectionDate));
			$collectionMonth = date('m', strtotime($purchasedetails->collectionDate));
			$collectionDay = date('d', strtotime($purchasedetails->collectionDate));
			
			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$daysmargin = $startdate->diffInDays($finishdate, false);
			
			$binhire = DB::table('tblbinservice')
                    ->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
                    ->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
                    ->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
                    ->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
                    ->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
                    ->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
                    ->where(['tblbinservice.idBinService' => $purchasedetails->idBinService])
                    ->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
                    ->orderBy('tblbinservice.price','asc')
                    ->first();

             $binhireoptions = DB::table('tblbinserviceoptions')
                        ->leftJoin('tblbinservice', function ($query){
                             $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                ->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                        })
                        ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                        ->where([
                            'tblbinserviceoptions.idBinType' => $purchasedetails->idBinType,
                            'tblbinserviceoptions.idSupplier' =>$purchasedetails->idSupplier
                            ])
                        ->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                        ->first();
			$bookingprice = $this->get_bookingprice();
            if ($daysmargin > $binhireoptions->extraHireageDays){
				$exactplusdays = $daysmargin - $binhireoptions->extraHireageDays;
				$totalprice = $bookingprice->price + $binhire->price + ($exactplusdays * $binhireoptions->extraHireagePrice);
			} else {
				$totalprice = $bookingprice->price + $binhire->price;
			}
            $servicearea = DB::table('tblservicearea')
                        ->select('area')
                        ->where(['zipcode' => $purchasedetails->zipcode])
                        ->first();
			
           
            return view('paywithpaypal',['purchasedetails' => $purchasedetails , 'binhire' => $binhire, 'binhireoptions' => $binhireoptions, 'daysmargin' => $daysmargin, 'idpaymenttemp' => $idpaymenttemp, 'servicearea' => $servicearea,'bookingprice' => $bookingprice ,'totalprice' => $totalprice ]);      
        } else {
            return view('paywithpaypal',['purchasedetails' => $purchasedetails, 'idpaymenttemp' => $idpaymenttemp]);
        }
    }

    public function paymentStatus(Request $request){
        return view('paymentstatus');
    }

    public function postPaymentWithpaypal(Request $request){
        $idpaymenttemp = $request['idpaymenttemp'];
        Session::put('idpaymenttemp',$idpaymenttemp);
		$totalprice = $request['totalprice'];
		Session::put('totalprice', $totalprice);
        $validate = Validator::make($request->all(), [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'address' => 'required|string',
            'suburb' => 'required|string|max:100',
            'phone' => 'required|string|max:12',
            'email' => 'required|email',
            'agree' => 'required',
            'postal_code' => 'required|same:zipcode'
        ], [
            'first_name.required' => 'First name is required',
            'last_name.required' => 'Last name is required',
            'address.required' => 'Delivery Address is required',
            'suburb.required' => 'Suburb is required',
            'phone.required' => 'Phone is required',
            'email.required' => 'Email is required',
            'agree.required' => 'Tick agree to proceed',
            'postal_code.same' => 'Typed address mismatch from selected postal code'
        ]);
        
        if($validate->fails()){
            return Redirect::route('addPayment', ['idpaymenttemp' =>$request['idpaymenttemp']])->withErrors($validate)->withInput();
        }
        
        /* Fetch item and order details */
        $order = DB::table('tblpaymenttemp')
                ->select('idpaymenttemp', 'idBinHire', 'zipcode', 'deliveryDate', 'collectionDate')
                ->where(['idpaymenttemp' => $idpaymenttemp])
                ->first();

        $binservice = DB::table('tblbinservice')
                    ->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
                    ->leftJoin('tblsize', 'tblbinservice.idBinSize', '=', 'tblsize.idSize')
                    ->select('tblbinservice.idBinService', 'tblbinservice.idSupplier', 'tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price', 'tblbinservice.stock'
                        , 'tblbintype.name', 'tblbintype.description', 'tblsize.size')
                    ->where([
                        'idBinService' => $order->idBinHire
                        ])
                    ->orderBy('tblbinservice.idBinService', 'tblbinservice.idSupplier', 'tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price', 'tblbinservice.stock'
                        , 'tblbintype.name', 'tblbintype.description', 'tblsize.size')
                    ->first();

        $insertpaymentform = DB::table('tblpaymentformtemp')->insertGetId(
                [
                    'idPaymentTemp' => $idpaymenttemp, 
                    'first_name' => $request['first_name'],
                    'last_name' => $request['last_name'],
                    'address' => $request['address'],
                    'suburb' => $request['suburb'],
                    'zipcode' => $request['zipcode'],
                    'phone' => $request['phone'],
                    'email' => $request['email'],
                    'special_note' => $request['note']
                ]
        );
        Session::put('idpaymentform',$insertpaymentform);
        
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $itemname = $binservice->name.' - '.$binservice->size;

        $item_1->setName($itemname) //item name
            ->setCurrency('AUD')
            ->setQuantity(1)
            ->setPrice($totalprice); //unit price

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('AUD')
            ->setTotal($totalprice);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription($binservice->name);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status')) //Specify return URL
            ->setCancelUrl(URL::route('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try 
        {
            $payment->create($this->_api_context);
        } 
        catch (\PayPal\Exception\PPConnectionException $ex) 
        {
            if (\Config::get('app.debug')) 
            {
                \Session::put('error','Connection timeout');
                 return Redirect::route('addPayment', ['idpaymenttemp' =>$request['idpaymenttemp']]);
            } 
            else 
            {
                \Session::put('error','Some error occur, sorry for inconvenient');
                 return Redirect::route('addPayment', ['idpaymenttemp' =>$request['idpaymenttemp']]);
            }
        }

        foreach($payment->getLinks() as $link) 
        {
            if($link->getRel() == 'approval_url') 
            {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) 
        {
            return Redirect::away($redirect_url);
        }

        \Session::put('error','Unknown error occurred');
         return Redirect::route('addPayment', ['idpaymenttemp' =>$request['idpaymenttemp']]);
    }

    public function getPaymentStatus(Request $request)
    {
        //----------------------------------------
        // Get the payment ID before session clear
        //----------------------------------------
        $payment_id = Session::get('paypal_payment_id');
        $idpaymenttemp = Session::get('idpaymenttemp');
        $idpaymentform = Session::get('idpaymentform');
		$totalprice = Session::get('totalprice');

        Session::forget('paypal_payment_id');
        Session::forget('idpaymenttemp');
        Session::forget('idpaymentform');
		Session::forget('totalprice');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) 
        {
            \Session::put('error','Payment failed');
            return Redirect::route('paymentstatus');
        }
        $payment = Payment::get($payment_id, $this->_api_context);

        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') { 
            
            //----------------
            // Here Write your database logic like that insert record or value in database if you want 
            //----------------
            if (!is_null($idpaymenttemp)){
                $purchasedetails = DB::table('tblpaymenttemp')
                        ->leftJoin('tblbinservice','tblpaymenttemp.idBinHire','=','tblbinservice.idBinService')
                        ->select('tblbinservice.idBinService','tblbinservice.idSupplier','tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price',
                            'tblpaymenttemp.deliveryDate', 'tblpaymenttemp.collectionDate', 'tblpaymenttemp.zipcode')
                        ->where([
                                'tblpaymenttemp.idPaymentTemp' => $idpaymenttemp
                            ])
                        ->orderBy('tblbinservice.idBinService','tblbinservice.idSupplier','tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price',
                            'tblpaymenttemp.deliveryDate', 'tblpaymenttemp.collectionDate', 'tblpaymenttemp.zipcode')
                        ->first();

                $paymentform = DB::table('tblpaymentformtemp')
                                ->select('idpaymenttemp', 'first_name', 'last_name', 'address', 'suburb', 'zipcode', 'phone', 'email', 'special_note')
                                ->where(['idPaymentForm' => $idpaymentform])
                                ->first();

                 if (!is_null($purchasedetails)){
                     if (!is_null($paymentform)){
                        //insert order
                        $idcustomer = DB::table('tblcustomer')->insertGetId([
                            'name' => $paymentform->first_name.' '.$paymentform->last_name,
                            'address' => $paymentform->address, 
                            'suburb' => $paymentform->suburb,
                            'zipcode' => $paymentform->zipcode,
                            'phone' => $paymentform->phone,
                            'email' => $paymentform->email
                        ]);
                        if (!is_null($idcustomer)){
                        $datenow = date('Y-m-d', strtotime('now'));
                        $insertOrder = DB::table('tblorderservice')->insertGetId([
                            'idBinService' => $purchasedetails->idBinService,
                            'idConsumer' => $idcustomer,
                            'paymentUniqueCode' => $this->generateUniqueID(),
                            'deliveryDate' => $purchasedetails->deliveryDate,
                            'collectionDate' => $purchasedetails->collectionDate,
                            'deliveryAddress' => $paymentform->address,
                            'deliveryComments' => $paymentform->special_note,
                            'idSupplier' => $purchasedetails->idSupplier,
                            'orderDate' => $datenow,
                            'totalServiceCharge' => $totalprice
                            ]);

                            if(!is_null($insertOrder)){
                                //update paid status
                                $updatepaid = DB::table('tblpaymenttemp')
                                            ->where(['idPaymentTemp' => $idpaymenttemp])
                                            ->update(['paid' => 1]);


                                $invoiceDetails = DB::table('tblorderservice')
                                                ->select('paymentUniqueCode', 'totalServiceCharge', 'deliveryDate', 'collectionDate', 'deliveryAddress', 'deliveryComments', 'orderDate')
                                                ->where(['idOrderService' => $insertOrder])
                                                ->first();

                                //insertorderstatushere
                                $currenttimestamps = date('Y-m-d H:i:s',strtotime('now'));
                                $insertOrderStatus = DB::table('tblorderstatus')->insertGetId([
                                    'idOrder' => $insertOrder,
                                    'status' => 1,
									'created_at' => $currenttimestamps,
                                    'updated_at' => $currenttimestamps,
                                    'operator' => 11,
                                ]);
								
								//update skip bin stock here
								$stock = DB::table('tblbinservice')
											->select('stock')
											->where(['idBinService' => $purchasedetails->idBinService])
											->first();
								$updatedvalue = $stock->stock - 1;
								$updatestock = DB::table('tblbinservice')
                                            ->where(['idBinService' => $purchasedetails->idBinService])
                                            ->update(['stock' => $updatedvalue]);
											
                                $customerdetails = DB::table('tblcustomer')
                                                ->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
                                                ->where(['idCustomer' => $idcustomer])
                                                ->first();

                                $binhire = DB::table('tblbinservice')
                                    ->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
                                    ->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
                                    ->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
                                    ->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
                                    ->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
                                    ->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
                                    ->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
                                    ->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblsize.size')
                                    ->where(['tblbinservice.idBinService' => $purchasedetails->idBinService])
                                    ->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblsize.size')
                                    ->orderBy('tblbinservice.price','asc')
                                ->first();

                                $supplierdetails = DB::table('tblsupplier')
                                                ->select('name', 'contactName', 'phonenumber', 'email', 'mobilePhone', 'fullAddress', 'email2', 
                                                    'customerServiceContact', 'customerServicePhone', 'customerServiceMobile')
                                                ->where(['idSupplier' => $binhire->idSupplier])
                                                ->first();

                                $binhireoptions = DB::table('tblbinserviceoptions')
                                    ->leftJoin('tblbinservice', function ($query){
                                        $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                        ->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                                    })
                                    ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                                    ->where([
                                        'tblbinserviceoptions.idBinType' => $purchasedetails->idBinType,
                                        'tblbinserviceoptions.idSupplier' =>$purchasedetails->idSupplier
                                    ])
                                    ->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                                    ->first();
								
								$bookingprice = $this->get_bookingprice();
                                $tomail = $customerdetails->email;
                                $tosender = $customerdetails->name;
                                $subject = "Ezy Skip Bin Payment Confirmation";
                                Mail::send('mails.mail_invoice', ['invoiceDetails' => $invoiceDetails, 'customerdetails' => $customerdetails, 'supplierdetails' => $supplierdetails, 'binhire' => $binhire, 'binhireoptions' => $binhireoptions, 'bookingprice' => $bookingprice],
                                function($mail) use ($tomail, $tosender, $subject){
                                    $mail->from(getenv('FROM_EMAIL_ADDRESS'), "demo@ihubcrm.com");
                                    $mail->to($tomail,  $tosender);
                                    $mail->subject($subject);
                                });

                                $suppliertomail = $supplierdetails->email;
                                $suppliertosender = $supplierdetails->name;
                                $suppliersubject = "Ezy Skip Bin Bin Hire Quote";
                                Mail::send('mails.mail_receipt', ['invoiceDetails' => $invoiceDetails, 'customerdetails' => $customerdetails, 'binhire' => $binhire, 'binhireoptions' => $binhireoptions,'bookingprice' => $bookingprice],
                                function($mail) use ($suppliertomail, $suppliertosender, $suppliersubject){
                                    $mail->from(getenv('FROM_EMAIL_ADDRESS'), "demo@ihubcrm.com");
                                    $mail->to($suppliertomail,  $suppliertosender);
                                    $mail->subject($suppliersubject);
                                });

                            }
                        }       
                    }
                }
            }
            \Session::put('success','Payment success');
            return Redirect::route('paymentstatus');
        }
        \Session::put('error','Payment failed');

        return Redirect::route('paymentstatus');
    }

    public function generateUniqueID(){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pin = mt_rand(10000, 99999)
            . mt_rand(10000, 99999)
            . $characters[rand(0, strlen($characters) - 1)];
        $rand = str_shuffle($pin);
        return $rand;
    }
	
	private function get_bookingprice(){
		$bookingprice = DB::table('tblbookingprice')
					->select('price')
					->first();
		return $bookingprice;
	}
    public function away(){
        return redirect()->away('http://somsdevone.com/ezyskipbin');
    }

}
