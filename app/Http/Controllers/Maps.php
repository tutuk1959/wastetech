<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GeneaLabs\LaravelMaps\Facades\Map;
//use Cornford\Googlemapper\Facades\MapperFacade as Mapper;
//use FarhanWazir\GoogleMaps\GMaps;


class Maps extends Controller
{

        public function maps_view(){
        		$driver_sheet=$this->show_driver();

                return view('vmaps',[ 'driver_sheet' => $driver_sheet]);
        }
        public function show_driver(){
        	$orders = DB::table('bin_logistic')
			->leftJoin('routes','bin_logistic.route_id', '=', 'routes.id')
			->leftJoin('route_types', 'routes.route_type_id','=', 'route_types.id')
			->leftJoin('tbltrucks', 'bin_logistic.truck_id', '=', 'tbltrucks.id')
			->leftJoin('service_area_points','routes.sa_point_id','=','service_area_points.id')
			->select('tbltrucks.name AS truck','tbltrucks.driver', 'tbltrucks.color_code','routes.datetime', 'route_types.type',
				'service_area_points.address'
				)
			 ->orderBy('bin_logistic.id', 'desc')
			->get();
		return $orders;
        }

        private function insert_get_id_routes($route_type,$sa_point_id)
		{
			 $id = DB::table('routes')->insertGetId(
			    ['datetime' => '2020-02-17 09:20:30','route_type_id' => $route_type, 'sa_point_id' => $sa_point_id]
			);
			return $id;
		}

		private function insert_logistic($id_routes,$truck_id)
		{
			 $logg = DB::table('bin_logistic')->insert(
			    ['datetime' => '2020-02-17','truck_id' => $truck_id, 'route_id' => $id_routes]
			);
			return $logg;
		}

        public function get_the_result(Request $request){
 			
 			$route_type = $request['selecttype'];
 			$truck_id = $request['selecttruck'];
 			$sa_point_id = $request['selectlocation'];

 			$id_routes=$this->insert_get_id_routes($route_type,$sa_point_id);
 			$log=$this->insert_logistic($id_routes,$truck_id);
 			$driver_sheet=$this->show_driver();
 			$map = Map::create_map();
 			return view('vmaps',[ 'driver_sheet' => $driver_sheet])->with('map', $map);


        }

        
}
