<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Redirect;
use PDF;
use Illuminate\Support\Facades\Mail;

class Order_Summary_BinType extends Controller
{

	public function order_summary_bin_view(){
		$bintypedata = $this->get_bin_type_data();
		$bintypedata_byid = null;
		$binsizedata = $this->get_bin_size_data();
		$binsizedata_byid = null;
		$suppliesdata = null;
		return view('order_summary_bin_view', [ 'bintypedata_byid' => $bintypedata_byid, 'bintypedata' => $bintypedata, 'binsizedata' => $binsizedata ,'suppliesdata' => $suppliesdata]);
	}

	private function get_bin_type_data(){
		$bintypedata = DB::table('tblbintype')
			->select('tblbintype.idBinType', 'tblbintype.name')
			->get();
		return $bintypedata;
	}

	private function get_bin_type_data_by_id($idBinType){
		$bintypedata_byid = DB::table('tblbintype')
			->select('tblbintype.idBinType', 'tblbintype.name')
			->where([ 
				'tblbintype.idBinType' =>  $idBinType,
			])
			->get();
		return $bintypedata_byid;
	}


	private function get_bin_size_data()
	{
		$binsizedata = DB::table('tblsize')
			->select('tblsize.IdSize','tblsize.size')
			->get();
		return $binsizedata;
	}

	private function get_bin_size_data_by_id($idBinSize)
	{
		$binsizedata = DB::table('tblsize')
			->select('tblsize.IdSize','tblsize.size')
			->where([ 
				'tblsize.IdSize' =>  $idBinSize,
			])
			->get();
		return $binsizedata;
	}
	private function get_order_data($idBinType, $idBinSize, $start, $end){
		$orders = DB::table('tblorderservice')
			->leftJoin('tblcustomer','tblorderservice.idConsumer', '=', 'tblcustomer.idCustomer')
			->leftJoin('tblsupplier', 'tblorderservice.idSupplier','=', 'tblsupplier.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblbinservice','tblbinservice.idBinService','=','tblorderservice.idBinService')
			->leftJoin('tblbintype','tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize','tblbinservice.idBinSize','=','tblsize.idSize')
			->leftJoin('tblorderstatus', 'tblorderstatus.idOrder', '=', 'tblorderservice.idOrderService')
			->select('tblcustomer.name AS customerName','tblcustomer.company AS customerCompanyName', 'tblcustomer.email AS customerEmail','tblcustomer.phone AS customerPhone',
				'tblbintype.name AS bintypename','tblbintype.CodeType as bintypecode','tblbintype.description AS bintypedescription',
				'tblsize.size AS binsize', 'tblorderservice.idOrderService','tblorderservice.paymentUniqueCode',
				'tblorderservice.orderDate', 'tblorderservice.deliveryDate', 'tblorderservice.collectionDate','tblorderservice.deliveryAddress', 
				 'tblorderservice.totalServiceCharge', 'tblorderservice.subtotal', 'tblorderservice.gst','tblorderservice.bookingfee',
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService'
				)
			->where([ 
				'tblbintype.idBinType' =>  $idBinType,
			])
			->where([ 
				'tblsize.idSize' =>  $idBinSize,
			])

			->whereBetween('collectionDate',[$start,$end])
			->where(function($query){
        		$query->where( 'tblorderstatus.status', '=', 1 );
    		})
			->groupBy('customerName','customerCompanyName','customerEmail','customerPhone','bintypename','bintypecode','bintypedescription',
				'binsize','tblorderservice.idOrderService','tblorderservice.paymentUniqueCode','tblorderservice.orderDate', 'tblorderservice.deliveryDate', 
				'tblorderservice.collectionDate', 'tblorderservice.deliveryAddress',
				 'tblorderservice.totalServiceCharge', 'tblorderservice.subtotal', 'tblorderservice.gst','tblorderservice.bookingfee',
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService'
				)
			->get();
		return $orders;
	}
	

	public function fetch_bin_type(Request $request){

        $idBinType = $request['selectbintype'];
        $idBinSize = $request['selectbinsize'];
        $start = $request['start_order_date'];
        $end = $request['end_order_date'];

        $bintypedata=$this->get_bin_type_data();
        $binsizedata = $this->get_bin_size_data();
		$bintypedata_byid=$this->get_bin_type_data_by_id($idBinType);
		$binsizedata_byid=$this->get_bin_size_data_by_id($idBinSize);

        $actualStartDate = date('d-m-Y', strtotime($start));
        $actualEndDate = date('d-m-Y', strtotime($end));

        $suppliesdata = $this->get_order_data($idBinType, $idBinSize, $start, $end);
        //$sum = $this->sum_the_price($idSupplier, $start, $end);
		//$bookingprice = $this->get_bookingprice();
       	return view('order_summary_bin_view', [ 'binsizedata_byid' => $binsizedata_byid, 'bintypedata_byid' => $bintypedata_byid, 'suppliesdata' => $suppliesdata, 'bintypedata' => $bintypedata, 'binsizedata' => $binsizedata,'actualStartDate' => $actualStartDate, 'actualEndDate' => $actualEndDate]);
	}


}
