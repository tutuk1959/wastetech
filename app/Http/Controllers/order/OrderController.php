<?php

namespace App\Http\Controllers\order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use URL;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;
use Carbon\Carbon;
use App\tblcustomer;
use App\tblorderservice;
use App\tblorderstatus;
use App\tblordersitem;
use App\tblbinserviceupdates;
use App\tblbinservicestok;
use App\tbluse;
use App\tbljobcards;
use App\tbltasks;
use App\tblorganization;
use App\tbldelivery_address;
use App\tblprojectsites;

use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
	private $user;
	private $user_id;
	public function __construct(){
		//$this->user_id = session('idUser');
		//$this->user = tbluse::find(session('idUser'));
	}
	
	/**
	* Show order listing
	*/
	private function show_order_list($start, $end){
		
		$orders = DB::table('tblorderservice')
		->leftJoin('tblproject_sites','tblorderservice.idproject_sites', '=', 'tblproject_sites.id')
		->leftJoin('tblorganization', 'tblorderservice.idConsumer','=', 'tblorganization.id')
		->select('tblorderservice.*', 'tblproject_sites.*', 'tblorganization.*')
		->whereBetween('tblorderservice.orderDate',[$start,$end])
		->orderBy('tblorderservice.orderDate', 'DESC')
		->get();
		
		return $orders;
		
	}
	
	/** 
	* Index view
	**/
	public function index(){
		$suppliesdata = null;
		$selected_supplierdata = null;
	
		return view('order.order_all', ['suppliesdata' => $suppliesdata, 
		'selected_supplierdata' => $selected_supplierdata]);
	}
	
	/** 
	* Find order based on Date
	*/
	public function findorder_ondate(Request $request){
		$validate = Validator::make($request->all(), [
			'start_order_date' => 'required|date',
			'end_order_date' => 'required|date'
		]);
	
		if($validate->fails()){
			$supplierdata = $this->get_supplier_data();
			return Redirect::route('orders')->withErrors($validate)->withInput();
		}
		
	
		$start = $request['start_order_date'];
		$end = $request['end_order_date'];
		
		$suppliesdata = $this->show_order_list($start, $end);
	
		return view('order.order_all', ['suppliesdata' => $suppliesdata]);
	}
	
	/**
	* Check if the same orders exists
	*/
	private function check_orders($uniqueid){
		$orders = DB::table('tblorderservice')
		->where([
			'paymentUniqueCode' => $uniqueid
		])
		->first();
		
		return $orders;
	}
	
	/**
	* Get booking price
	**/
	private function get_bookingprice(){
		$bookingprice = DB::table('tblbookingprice')
					->select('price')
					->first();
		return $bookingprice;
	}
	
	public function api_return(Request $request){
		
		return '12345';
		
	}
	/**
	* Show add form 
	*/
	public function add_orders_view(){
		$uniqueid = $this->generateUniqueID();
		$suppliers = $this->get_supplier_data();
		$organizations = $this->get_organizations();
		$project_sites = $this->get_projectsites();
		$waste_type = $this->get_bintype();
		$binsize = $this->get_binsize();
		$address = $this->find_delivery_address_all();
		return view('order.add_order', ['uniqueid' => $uniqueid, 
		'suppliers' => $suppliers, 'organizations' => $organizations, 
		'project_sites' => $project_sites, 'waste_type' => $waste_type, 'binsize' => $binsize,'address' => $address]);
	}
	
	
	/**
	* Add form submission
	*/
	public function add_orders(Request $request){
		foreach($request['idbinservice'] as $k=>$v){
			$orders_item[$k] = array(
				'idbinservice' => $request['idbinservice'][$k], 
				'wastetype' => $request['wastetype'][$k], 
				'size' => $request['binsize'][$k], 
				'qty' => $request['qty'][$k],
				'address' => $request['address'][$k],
				'price' => $request['price'][$k],
				'subtotal' => $request['subtotal'][$k],
				'delivery_date' => $request['delivery_date'][$k],
				'collection_date' => $request['collection_date'][$k],
				'delivery_comments' => $request['delivery_comments'][$k],
			);
		}

		$validate = Validator::make($request->all(), [
			'supplier' => 'required',
			'organization' => 'required',
			'date' => 'required',
			'project_site' => 'required',
		]);
		
		
		if($validate->fails()){
			
			return Redirect::back()->withErrors($validate)->withInput($request->all());
		} else {
			$check_orders = $this->check_orders($request['unique_id']);
			
			if(is_null($check_orders)){
				$bookingprice = $this->get_bookingprice();
				$subtotal = $request['all_total'] + $bookingprice->price;
				$tblorderservice = new tblorderservice();
				$tblorderservice->idConsumer = $request['organization'];
				$tblorderservice->idproject_sites = $request['project_site'];
				$tblorderservice->paymentUniqueCode = $request['unique_id'];
				$tblorderservice->subtotal = $request['all_total'];
				$tblorderservice->gst = $subtotal /110  * 10;
				$tblorderservice->totalServiceCharge = $subtotal;
				$tblorderservice->bookingfee = $bookingprice->price;
				$tblorderservice->idSupplier = $request['supplier'];
				$tblorderservice->orderDate = date('Y-m-d', strtotime($request['date']));
				$tblorderservice->movetojobs = 1;
				
				if($tblorderservice->save()){
					$lastid = $tblorderservice->id;
					$tbljobcards = new tbljobcards();
					$tbljobcards->delivery_date = date('Y-m-d', strtotime($request['date']));
					$tbljobcards->orderid = $lastid;
					$tbljobcards->status = 1;
					$tbljobcards->save();
					
					$jobcardid = $tbljobcards->id;
					foreach($orders_item as $k => $v){
						$tblordersitem = new tblordersitem();
						$tblordersitem->idorder = $lastid;
						$tblordersitem->idbinservice = $orders_item[$k]['idbinservice'];
						$tblordersitem->idsize =  $orders_item[$k]['size'];
						$tblordersitem->iddelivery_address = $orders_item[$k]['address'];
						$tblordersitem->delivery_date = date('Y-m-d', strtotime( $orders_item[$k]['delivery_date']));
						$tblordersitem->collection_date = date('Y-m-d', strtotime($orders_item[$k]['collection_date']));
						$tblordersitem->subtotal =$orders_item[$k]['subtotal'];
						$tblordersitem->attribute = 1;
						$tblordersitem->quantity = $orders_item[$k]['qty'];
						$tblordersitem->delivery_comments = $orders_item[$k]['delivery_comments'];
						
						
						
						if($tblordersitem->save()){
							$lastorderitems = $tblordersitem->id;
							$tblorderstatus = new tblorderstatus();
							$tblorderstatus->idOrder = $lastid;
							$tblorderstatus->status = 1;
							$tblorderstatus->save();
							
							$del_date = date('Y-m-d', strtotime( $orders_item[$k]['delivery_date']));
							$col_date =  date('Y-m-d', strtotime($orders_item[$k]['collection_date']));
							$price = $orders_item[$k]['subtotal']/$orders_item[$k]['qty'];
							
							//$this->update_stocking($orders_item[$k]['idbinservice'], $del_date, $col_date, $price);
							$tbltasks = new tbltasks();
							$tbltasks->idjobcard = $jobcardid;
							$tbltasks->job_type = 1;
							$tbltasks->status = 1;
							$tbltasks->starttime = date('Y-m-d h:m:s',strtotime( $orders_item[$k]['delivery_date']));
							$tbltasks->assignedto = session('idUser');
							$tbltasks->idorderitems = $lastorderitems;
							$tbltasks->orderitem = $lastorderitems;
							$tbltasks->note = $orders_item[$k]['delivery_comments'];
							$tbltasks->driver_updated = 0;
							$tbltasks->save();
							
							$tbltasks = new tbltasks();
							$tbltasks->idjobcard = $jobcardid;
							$tbltasks->job_type = 2;
							$tbltasks->status = 1;
							$tbltasks->starttime = date('Y-m-d h:m:s',strtotime($orders_item[$k]['collection_date']));
							$tbltasks->assignedto = session('idUser');
							$tbltasks->idorderitems = $lastorderitems;
							$tbltasks->orderitem = $lastorderitems;
							$tbltasks->note = $orders_item[$k]['delivery_comments'];
							$tbltasks->driver_updated = 0;
							$tbltasks->save();
							
							$request->session()->flash('status', 'success');
							$request->session()->flash('message', sprintf('Insert order success'));
							
						} else {
							$request->session()->flash('status', 'danger');
							$request->session()->flash('message', sprintf('Insert order failed'));
							
						}
						
					}
					return redirect()->route('add_orders_view');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', sprintf('Insert order failed'));
					return redirect()->route('add_orders_view')->withInput($request->all());
				}
			} else{
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', sprintf('Duplicate order ID'));
				return redirect()->route('add_orders_view')->withInput($request->all());
			}
		}
	}
	
	/** 
	* Order Data
	*/
	private function get_order_data($idorders, $paymentuniquecode){
		$orders = DB::table('tblorderservice')
			->where([
				'idOrderService' => $idorders,
				'paymentUniqueCode' => $paymentuniquecode
			])
			->first();
		
		return $orders;
	}
	
	/** 
	* Organization Data
	*/
	private function get_organization_data($idorganization){
		$organization = DB::table('tblorganization')
			->where([
				'id' => $idorganization,
			])
			->first();
		return $organization;
	}
	
	/** 
	* Project Site Data
	*/
	private function get_projectsite_data($idproject_sites){
		$projectsite = DB::table('tblproject_sites')
			->where([
				'id' => $idproject_sites,
			])
			->first();
		return $projectsite;
	}
	
	/** 
	* Supplier Data
	*/
	private function get_supplier_data_id($idsupplier){
		$supplier = DB::table('tblsupplier')
			->where([
				'idSupplier' => $idsupplier,
			])
			->first();
		return $supplier;
	}
	
	/** 
	* Orders Item Data
	*/
	private function get_orderitems_data($idorder){
		$fetch_order_items = DB::table('tblorder_items')
		->where([
			'idorder' => $idorder
		])
		->get();
		
		return $fetch_order_items;
	}
	
	/**
	* Public static order data
	*/
	public static function orders_item_id($idorderitems){
		$fetch_order_items = DB::table('tblorder_items')
		->where([
			'id' => $idorderitems
		])
		->first();
		
		if(!is_null($fetch_order_items)){
			if(!is_null($fetch_order_items->isbinupdate) && $fetch_order_items->isbinupdate == '1'){
				$detail = DB::table('tblorder_items')
				->leftJoin('tblbinserviceupdates', 'tblbinserviceupdates.idBinServiceUpdates', '=', 'tblorder_items.idbinservice')
				->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblbinserviceupdates.idBinService')
				->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
				->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
				->leftJoin('tbldelivery_address', 'tbldelivery_address.iddelivery_address', '=', 'tblorder_items.iddelivery_address')
				->select(
					'tblbinserviceupdates.price', 'tblbintype.*', 'tblbintype.name AS bintype_name', 'tblbintype.description AS bintype_description', 
					'tblbintype.description2 AS bintype_description_2', 'tblsize.size', 'tbldelivery_address.*', 
					'tblorder_items.*'
				)
				->where(
					[
						'tblorder_items.id' => $fetch_order_items->id
					]
				)
				
				->first();
				return $detail;
			} else {
				$detail = DB::table('tblorder_items')
				->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
				->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
				->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
				->leftJoin('tbldelivery_address', 'tbldelivery_address.iddelivery_address', '=', 'tblorder_items.iddelivery_address')
				->select(
					'tblbinservice.price', 'tblbintype.name AS bintype_name','tblbintype.description AS bintype_description', 
					'tblbintype.description2 AS bintype_description_2', 'tblsize.size', 'tbldelivery_address.*', 
					'tblorder_items.*', 'tblbintype.*'
				)
				->where(
					[
						'tblorder_items.id' => $fetch_order_items->id
					]
				)
				
				->first();
				
				return $detail;
			}
		}
	}
	/**
	* Order Details
	*/
	
	public function orders_detail(Request $request){
		
		$order_detail = $this->get_order_data($request['idorders'], $request['payment']);
		
		if(!is_null($order_detail)){
			
			$organization = $this->get_organization_data($order_detail->idConsumer);
			$projectsite = $this->get_projectsite_data($order_detail->idproject_sites);
			$supplierdata = $this->get_supplier_data_id($order_detail->idSupplier);
			$orderitem = $this->get_orderitems_data($order_detail->idOrderService);
			
			return view('order.order_detail', ['order_detail' => $order_detail, 'organization' => $organization, 'projectsite' => $projectsite, 'supplierdata' => $supplierdata, 'orderitem' => $orderitem]);
		}
	}
	
	/**
	* Update view
	*/
	
	public function update_orders_view(Request $request){
		$order_detail = $this->get_order_data($request['idorders'], $request['payment']);
		$suppliers = $this->get_supplier_data();
		$organizations = $this->get_organizations();
		$project_sites = $this->get_projectsites();
		$waste_type = $this->get_bintype();
		$binsize = $this->get_binsize();
		$address = $this->find_delivery_address_all();
		
		if(!is_null($order_detail)){
			$organization = $this->get_organization_data($order_detail->idConsumer);
			$projectsite = $this->get_projectsite_data($order_detail->idproject_sites);
			$supplierdata = $this->get_supplier_data_id($order_detail->idSupplier);
			$orderitem = $this->get_orderitems_data($order_detail->idOrderService);
			
			return view('order.update_order', ['order_detail' => $order_detail, 'organization' => $organization, 'projectsite' => $projectsite, 'supplierdata' => $supplierdata, 'orderitem' => $orderitem, 'suppliers' => $suppliers, 'organizations' => $organizations, 'project_sites' => $project_sites, 'waste_type' => $waste_type, 'binsize' => $binsize, 'address' => $address]);
		}
	}
	
	/**
	* Update form submission
	*/
	public function update_orders(Request $request){
		foreach($request['idbinservice'] as $k=>$v){
			$orders_item[$k] = array(
				'idorderitem' => $request['idorder_item'][$k],
				'idbinservice' => $request['idbinservice'][$k], 
				'wastetype' => $request['wastetype'][$k], 
				'size' => $request['binsize'][$k], 
				'qty' => $request['qty'][$k],
				'address' => $request['address'][$k],
				'price' => $request['price'][$k],
				'subtotal' => $request['subtotal'][$k],
				'delivery_date' => $request['delivery_date'][$k],
				'collection_date' => $request['collection_date'][$k],
				'delivery_comments' => $request['delivery_comments'][$k],
			);
		}

		$validate = Validator::make($request->all(), [
			'supplier' => 'required',
			'organization' => 'required',
			'date' => 'required',
			'project_site' => 'required',
		]);
		
		if($validate->fails()){
			return Redirect::back()->withErrors($validate)->withInput($request->all());
		} else {
			$check_orders = $this->check_orders($request['unique_id']);
			if(!is_null($check_orders)){
				$bookingprice = $this->get_bookingprice();
				$subtotal = $request['all_total'] + $bookingprice->price;
				try{
					tblorderservice::where([
						'paymentUniqueCode' => $request['unique_id'],
						'idOrderService' => $request['idordermain']
					])
					->update([
						'idConsumer' => $request['organization'],
						'idproject_sites' => $request['project_site'],
						'subtotal' => $request['all_total1'],
						'gst' => $subtotal /110  * 10,
						'totalServiceCharge' => $subtotal,
						'bookingfee' => $bookingprice->price,
						'idSupplier' => $request['supplier'],
						'orderDate' => date('Y-m-d', strtotime($request['date']))
					]);
					
					try{
						foreach($orders_item as $k => $v){
							tblordersitem::where([
								'id' => $orders_item[$k]['idorderitem'],
							])
							->update([
								'idorder' => $request['idordermain'],
								'idbinservice' => $orders_item[$k]['idbinservice'],
								'idsize' => $orders_item[$k]['size'],
								'iddelivery_address' => $orders_item[$k]['address'],
								'delivery_date' => date('Y-m-d', strtotime( $orders_item[$k]['delivery_date'])),
								'collection_date' =>  date('Y-m-d', strtotime($orders_item[$k]['collection_date'])),
								'subtotal' => $orders_item[$k]['subtotal'],
								'attribute' => 1,
								'quantity' => $orders_item[$k]['qty'],
								'delivery_comments' => $orders_item[$k]['delivery_comments']
							]);
							
							$request->session()->flash('status', 'success');
							$request->session()->flash('message', sprintf('Update order success'));
						}
					} catch (Exception $ex){
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', sprintf('Update order failed'));
						return redirect()->route('update_orders_view', ['payment' => $request['unique_id'], 'idorders' => $request['idordermain']])->withInput($request->all());
					}
				} catch (Exception $ex){
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', sprintf('Update order failed'));
					return redirect()->route('update_orders_view', ['payment' => $request['unique_id'], 'idorders' => $request['idordermain']])->withInput($request->all());
				}
				return redirect()->route('update_orders_view',['payment' => $request['unique_id'], 'idorders' => $request['idordermain']]);
			} else {
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', sprintf('No order ID detected'));
				return redirect()->route('add_orders_view')->withInput($request->all());
			}
		}
	}
	
	private function update_stocking($binservice,$delivery_date, $collection_date,$price){
		$stock = DB::table('tblbinservice')
		->select('stock')
		->where(['idBinService' => $binservice])
		->first();
					
		$updatedvalue = $stock->stock - 1;
		$updatestock = DB::table('tblbinservice')
			->where(['idBinService' => $binservice])
			->update(['stock' => $updatedvalue]);
		
		//insert to stock update table 
		$deliveryYear = date('Y', strtotime($delivery_date));
		$deliveryMonth = date('m', strtotime($delivery_date));
		$deliveryDay = date('d', strtotime($delivery_date));
		
		$collectionYear = date('Y', strtotime($collection_date));
		$collectionMonth = date('m', strtotime($collection_date));
		$collectionDay = date('d', strtotime($collection_date));
		
		$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
		$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
		
		$dates = [];
		for($date = $startdate; $date->lte($finishdate); $date->addDay()) {
			$dates[] = $date->format('Y-m-d');
			$checkDisc = DB::table('tblbinservicestok')
					->select('idBinService',  'stock')
					->where([
						'idBinService' => $binservice,
						'date' => $date->format('Y-m-d'),
					])
				->first();
			if (!is_null($checkDisc)){
				$tblbinservicestok = new tblbinservicestok();
				$update = $tblbinservicestok::where([
					'idBinService' =>  $binservice,
					'date' => $date->format('Y-m-d'),
				])
				->update(['stock' => $updatedvalue]);
			}else{
				$tblbinservicestok = new tblbinservicestok();
				$tblbinservicestok->idBinService = $binservice;
				$tblbinservicestok->stock = $updatedvalue;
				$tblbinservicestok->date =  $date->format('Y-m-d');
				$tblbinservicestok->save();
				
			}
			
			$checkUpdates = DB::table('tblbinserviceupdates')
					->select('idBinService', 'price', 'stock')
					->where([
						'idBinService' => $binservice,
						'date' => $date->format('Y-m-d'),
					])
				->first();
			if (!is_null($checkUpdates)){
				$tblbinserviceupdates = new tblbinserviceupdates();
				$update = $tblbinserviceupdates::where([
					'idBinService' =>  $binservice,
					'date' => $date->format('Y-m-d'),
				])
				->update(['stock' => $updatedvalue]);
			}else{
				$tblbinserviceupdates = new tblbinserviceupdates();
				$tblbinserviceupdates->idBinService = $binservice;
				$tblbinserviceupdates->price = $price;
				$tblbinserviceupdates->stock = $updatedvalue;
				$tblbinserviceupdates->date =  $date->format('Y-m-d');
				$tblbinserviceupdates->save();
			}
		}
	}
	/********************************************************
	* Form data fillup
	***************************************************/
	/** Get supplier **/
	private function get_supplier_data(){
		$supplierdata = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where(['tbluser.role' => 2])
			->orWhere(['tbluser.role' => 1])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->get();
		return $supplierdata;
	}
	
	private function get_organizations(){
		$organization = DB::table('tblorganization')
		->get();
		
		return $organization;
	}
	
	private function get_binsize(){
		$sizes = DB::table('tblsize')
		->get();
		
		return $sizes;
	}
	
	private function get_bintype(){
		$bintype = DB::table('tblbintype')
		->get();
		
		return $bintype;
	}
	
	private function get_projectsites(){
		$projectsites = DB::table('tblproject_sites')
		->get();
		
		return $projectsites;
	}
	
	public function find_delivery_address(Request $request){
		if ($request->has('q')) {
			$search = $request->q;
			
			$data = DB::table('tbldelivery_address')
			->where('street_name', 'LIKE', '%'.$search.'%')
			->orWhere('post_code', 'LIKE', '%'.$search.'%')
			->get();
			return response()->json($data);
		}
	}
	
	private function find_delivery_address_all(){
		$data = DB::table('tbldelivery_address')
		->get();
		
		return $data;
	}
	
	private function find_binsize_based_supplierid(Request $request){
		if ($request->has('supplierid') && $request->has('date')) {
			$supplierid = $request->supplierid;
			$date = $request->date;
			
			$data = DB::table('tblbinservice')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblbinservice.idBinSize')
			->select('tblsize.*')
			->where([
				'tblbinservice.idSupplier' => $supplierid,
				'tbluser.userStatus' => '1'
			])
			->where('tblbinserviceupdates.stock','>',0)
			->where('tblbinserviceupdates.price','>',0)
			->get();
			
			return response()->json($data);
		}
	}
	
	private function find_wastetype_based_supplierid(Request $request){
		if ($request->has('supplierid') && $request->has('date') && $request->has('binsize')) {
			$supplierid = $request->has('supplierid');
			$data = DB::table('tblbinservice')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
			->select('tblbintype.*')
			->where([
				'tblbinservice.idSupplier' => $supplierid,
				'tbluser.userStatus' => '1'
			])
			->where('tblbinserviceupdates.stock','>',0)
			->where('tblbinserviceupdates.price','>',0)
			->get();
			
			return response()->json($data);
		}
	}
	
	private function generateUniqueID(){
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$pin = mt_rand(10000, 99999)
			. mt_rand(10000, 99999)
			. $characters[rand(0, strlen($characters) - 1)];
		$rand = str_shuffle($pin);
		return $rand;
    }
	
	private function get_postcode($id){
		$postcode = DB::table('tbldelivery_address')
		->where('iddelivery_address',$id)
		->first();
		
		return $postcode;
	}
	
	public function get_price(Request $request){
		if(!is_null($request['id'])){
			$address = $this->get_postcode($request['id']);
			$postcode = $address->post_code;
		} else {
			$postcode = $request['postcode'];
		}
		
		$date1 = date('Y-m-d', strtotime($request['date']));
		$args = array(
			'tblbinservice.idBinType' => 2,
			'tblbinservice.idBinSize' => $request['binsize'],
			'tblservicearea.zipcode' => $postcode,
			'tblsupplier.idSupplier' => 11,
			'tbluser.userStatus' => '1'
			
		);
		$binhire = DB::table('tblbinservice')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->select('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
			->where($args)
			->where('tblbinserviceupdates.price','>',0)
			->whereColumn('tblbinserviceupdates.price','<>','tblbinservice.price')
			->where('tblbinserviceupdates.date','=', $date1)
			->groupBy('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
			->orderBy('tblbinserviceupdates.price','asc')
			->get();
		$queryLog[1] = DB::getQueryLog();
		if($binhire->count() != 0){
			/**
			* Kalo ada price update, cek lagi stoknya
			*/
			$nondeliveryfilter = null;
			$update = 0;
			foreach($binhire as $k){
				$binhire_send = DB::table('tblbinservice')
				->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
				->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
				->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
				->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
				->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
				->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
				->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
				->select('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
				->where($args)
				->where('tblbinserviceupdates.stock','>',0)
				->where('tblbinserviceupdates.price','>',0)
				->whereColumn('tblbinserviceupdates.price','<>','tblbinservice.price')
				->where('tblbinserviceupdates.date','=', $date1)
				->groupBy('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
				->orderBy('tblbinserviceupdates.price','asc')
				->first();
		
				$queryLog[10] = DB::getQueryLog();
				
				if(!is_null($binhire_send)){
					$binhire_compare = DB::table('tblbinservice')
					->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
					->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
					->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
					->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
					->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
					->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
					->leftJoin('tblbinnondelivery', 'tblsupplier.idSupplier','=', 'tblsupplier.idSupplier')
					->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->where($args)
					->where('tblbinservice.stock','>=',0)
					->where('tblbinservice.price','>',0)
						->where('tblbinservice.idSupplier','<>',$binhire_send->idSupplier)
					->where('tblbinnondelivery.date', '<>' , $date1)
					
					->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->orderBy('tblbinservice.price','asc')
					->first();
		
					$queryLog[11] = DB::getQueryLog();
					
					if(!is_null($binhire_compare)){
						
						if($binhire_compare->price <= $binhire_send->price){
							
							/**FILTER 1 (FIND NON DELIVERY DATA)**/
							$binnondeliverydays = DB::table('tblbinnondelivery')
							->select('date','idSupplier','idBinType')
							->where([
								'idSupplier' => $binhire_compare->idSupplier, 
								'idBinType' => $request['wastetype'],
							])
							//->whereBetween('date',[$date1,$date2])
							->where('date', '=', $date1)
							->get();
							
							
							if($binnondeliverydays->count() == 0){
								/** GET THE WANTED VALUE **/
								$nondeliveryfilter = $binhire_compare;
								$update = 0;
								break;
							} else {
								$nondeliveryfilter = null;
								$update = 0;
								
							}
							
							
			
						} else {
							/**FILTER 1 (FIND NON DELIVERY DATA)**/
							$binnondeliverydays = DB::table('tblbinnondelivery')
							->select('date','idSupplier','idBinType')
							->where([
								'idSupplier' => $binhire_compare->idSupplier, 
								'idBinType' => $request['wastetype'],
							])
							//->whereBetween('date',[$date1,$date2])
							->where('date', '=', $date1)
							->get();
							
							
							if($binnondeliverydays->count() == 0){
								/** GET THE WANTED VALUE **/
								$nondeliveryfilter = $k;
								break;
							} else {
								$nondeliveryfilter = null;
								
							}
							$update = 1;
						}
					}
					
				}
			}
			$data = array(
				'status' => 1,
				'result' => $nondeliveryfilter,
				'isupdate' => $update
			);
			return response()->json($data);
		} else{
			$update = 0;
			$binhire = DB::table('tblbinservice')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->leftJoin('tblsupplier', 'tblbinservice.idSupplier','=','tblsupplier.idSupplier')
			->leftJoin('tblbinnondelivery', 'tblsupplier.idSupplier','=', 'tblbinnondelivery.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
			->where($args)
			->where('tblbinservice.stock','>',0)
			->where('tblbinservice.price','>',0)
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
			->orderBy('tblbinservice.price','asc')
			->get();
			
			
			$queryLog[12] = DB::getQueryLog();
		
			$nondeliveryfilter = null;
			foreach($binhire as $k){
				$binhire_send = DB::table('tblbinservice')
				->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
				->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
				->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
				->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
				->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
				->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
				->leftJoin('tblbinnondelivery', 'tblsupplier.idSupplier','=', 'tblsupplier.idSupplier')
				->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
				->where($args)
				->where('tblbinservice.stock','>=',0)
				->where('tblbinservice.price','>',0)
				->where('tblbinnondelivery.date', '<>' , $date1)
				
				->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
				->orderBy('tblbinservice.price','asc')
				->first();
		
				$queryLog[13] = DB::getQueryLog();
				
				if(!is_null($binhire_send)){
					//FILTER 1 (FIND NON DELIVERY DATA)
					$binnondeliverydays = DB::table('tblbinnondelivery')
					->select('date','idSupplier','idBinType')
					->where([
						'idSupplier' => $k->idSupplier, 
						'idBinType' => $request['wastetype']
					])
					//->whereBetween('date',[$date1,$date2])
					->where('date', '=', $date1)
					->get();
					
					
					if($binnondeliverydays->count() == 0){
						// GET THE WANTED VALUE
						$nondeliveryfilter = $k;
						$update = 0;
						break;
					} else {
						$nondeliveryfilter = null;
						$update = 0;
						
					}
					
					
				}
			}
			$data = array(
				'status' => 1,
				'result' => $nondeliveryfilter,
				'isupdate' => $update
			);
			return response()->json($data);
		}
	}
	
	/** NEW WORLD ORDER : AJAX **/
	public function find_organization_based_phone(Request $request){
		$phone = $request['phone'];
		
		$org = DB::table('tblproject_sites')
		->leftJoin('tbldelivery_address', 'tblproject_sites.id', '=', 'tbldelivery_address.idproject_site')
		->leftJoin('tblorganization', 'tblproject_sites.idorganization', '=', 'tblorganization.id')
		->select('tblorganization.id AS idorg', 'tbldelivery_address.unit_number',
		'tbldelivery_address.lot_number', 'tbldelivery_address.iddelivery_address','tbldelivery_address.street_name', 'tbldelivery_address.suburb', 
		'tbldelivery_address.post_code', 'tbldelivery_address.state')
		->where(function ($query) use ($phone) {
			$query->where('tblorganization.phone', '=' , $phone)
				->orWhere('tblorganization.mobile_phone', '=', $phone);
		})
		->groupBy('idorg', 'tbldelivery_address.unit_number',
		'tbldelivery_address.lot_number','tbldelivery_address.iddelivery_address', 'tbldelivery_address.street_name', 'tbldelivery_address.suburb', 
		'tbldelivery_address.post_code', 'tbldelivery_address.state')
		->orderBy('tbldelivery_address.street_name', 'ASC')
		->get();

		
		if(!is_null($org) && $org->count() > 0){
			$data = array(
				'status' => 1, 
				'result' => $org
			);
		} else {
			$data = array(
				'status' => 0, 
				'result' => null
			);
		}
		
		return response()->json($data);
	}
	
	public function find_orders_based_phone(Request $request){
		$idorg = $request['idorg'];
		$idaddress = $request['idaddress'];
		
		$orders = DB::table('tblorder_items')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tblorder_items.idOrder')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->select('tblorderservice.idOrderService', 'tblorder_items.id AS idorderitems', 'tblorderservice.is_quote',
		'tblorderservice.paymentUniqueCode', 'tblorderservice.orderDate', 
		'tblorder_items.quantity', 'tblorder_items.subtotal', 'tblsize.size','tblorderservice.is_paid')
		->where([
			'tblorderservice.idConsumer' => $idorg, 
			'tblorder_items.iddelivery_address' => $idaddress, 
		])
		->groupBy('tblorderservice.idOrderService','idorderitems', 'tblorderservice.is_quote','tblorderservice.paymentUniqueCode', 'tblorderservice.orderDate',
		'tblorder_items.quantity', 'tblorder_items.subtotal', 'tblsize.size','tblorderservice.is_paid')
		->get();
		
		if(!is_null($orders)){
			$data = array(
				'status' => 1, 
				'result' => $orders
			);
		} else {
			$data = array(
				'status' => 0, 
				'result' => null
			);
		}
		
		return response()->json($data);
		
	}
	
	/* get order data after order loaded */
	public function find_order_items_id(Request $request){
		$idorderitems = $request['idorderitems'];
		$iddelivery_address = $request['iddelivery_address'];
		$get_order_detail = DB::table('tblorder_items')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tblorder_items.idorder')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.idorganization', '=', 'tblorganization.id')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->select('tblbinservice.idBinSize', 'tblorganization.organization_name',
		'tblorganization.address','tblorganization.email_address','tblorganization.phone',
		'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tbldelivery_address.post_code','tbldelivery_address.suburb','tblorder_items.subtotal', 'tblorder_items.id as idorderitems')
		->groupBy('tblbinservice.idBinSize', 'tblorganization.organization_name',
		'tblorganization.address','tblorganization.email_address','tblorganization.phone',
		'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tbldelivery_address.post_code','tbldelivery_address.suburb','tblorder_items.subtotal',  'idorderitems')
		->where([
			'tblorder_items.id' => $idorderitems,
			'tbldelivery_address.iddelivery_address' => $iddelivery_address
		])
		->first();
		
		if(!is_null($get_order_detail)){
			$data = array(
				'status' => 1,
				'result' => $get_order_detail,
			);
		} else {
			$data = array(
				'status' => 0, 
				'result' => null
			);
		}
		return response()->json($data);
	}
	
	/** Fetch delivery address and organization details  */
	public function fetch_delivery_address(Request $request){
		$details = DB::table('tbldelivery_address')
		->leftJoin('tblproject_sites', 'tbldelivery_address.idproject_site', '=', 'tblproject_sites.id')
		->leftJoin('tblorganization', 'tblproject_sites.idorganization', '=', 'tblorganization.id')
		->select('tblorganization.organization_name', 'tblorganization.address',
		'tblorganization.email_address', 'tblorganization.phone', 
		'tbldelivery_address.street_name', 'tbldelivery_address.unit_number', 
		'tbldelivery_address.post_code', 'tbldelivery_address.suburb'
		)
		->where([
			'tbldelivery_address.iddelivery_address' => $request['iddelivery_address']
		])
		->groupBy('tblorganization.organization_name', 'tblorganization.address',
		'tblorganization.email_address', 'tblorganization.phone', 
		'tbldelivery_address.street_name', 'tbldelivery_address.unit_number', 
		'tbldelivery_address.post_code', 'tbldelivery_address.suburb')
		->first();
		
		if(!is_null($details)){
			$data = array(
				'status' => 1, 
				'result' => $details
			);
		} else {
			$data = array(
				'status' => 0, 
				'result' => null
			);
		}
		return response()->json($data);
	}
	
	/** Check organization based on phone */
	private function check_org($phone){
		$check = DB::table('tblorganization')
		->where([
			'phone' => $phone
		])
		->first();
		
		return $check;
	}
	/** Add new organization when taking order */
	private function add_new_org($phone, $email_address, $bill_address, $client_name){
		$check = $this->check_org($phone);
		if(!is_null($check)){
			$org = $check->id;
			return $org;
		} else {
			$tblorganization = new tblorganization();
			try{
				$tblorganization->organization_name = $client_name;
				$tblorganization->address = $bill_address;
				$tblorganization->cashonly = 0;
				$tblorganization->email_address = $email_address;
				$tblorganization->phone = $phone;
				$tblorganization->mobile_phone = 0;
				
				if($tblorganization->save()){
					$org = $tblorganization->id;
					return $org;
				} else {
					return 0;
				}
			} catch(Exception $ex){
				return 0;
			}
		}
	}
	
	/** Add new project site & delivery address when taking order */
	private function add_new_delivery($street_number, $street_name, $postcode, $suburb,$organization, $client_name){
		$to_check = $client_name.' '.$street_name;
		$check = DB::table('tblproject_sites')
		->where([
			'sitename' => $to_check, 
			'idorganization' => $organization
		])
		->first();
		
		if(!is_null($check)){
			$lastid = $check->id;
			$tbldelivery_address = new tbldelivery_address();
			try{
				$tbldelivery_address->unit_number = $street_number;
				$tbldelivery_address->lot_number = 0;
				$tbldelivery_address->street_name = $street_name;
				$tbldelivery_address->suburb = $suburb;
				$tbldelivery_address->post_code = $postcode;
				$tbldelivery_address->state = 'WA';
				$tbldelivery_address->latitude = 0;
				$tbldelivery_address->longitude = 0;
				$tbldelivery_address->contact = $client_name;
				$tbldelivery_address->idproject_site = $lastid;
				
				if($tbldelivery_address->save()){
					$delivery = $tbldelivery_address->id;
					return $delivery;
				} else {
					return 0;
				}
			}catch(Exception $ex){
				return 0;
			}
		} else {
			$tblprojectsites = new tblprojectsites();
			try{
				$tblprojectsites->idorganization = $organization;
				$tblprojectsites->sitename = $client_name.' '.$street_name;
				$tblprojectsites->contact = $client_name;
				$tblprojectsites->gate = 0;
				$tblprojectsites->notes = '';
				
				if($tblprojectsites->save()){
					$lastid = $tblprojectsites->id;
					$tbldelivery_address = new tbldelivery_address();
					try{
						$tbldelivery_address->unit_number = $street_number;
						$tbldelivery_address->lot_number = 0;
						$tbldelivery_address->street_name = $street_name;
						$tbldelivery_address->suburb = $suburb;
						$tbldelivery_address->post_code = $postcode;
						$tbldelivery_address->state = 'WA';
						$tbldelivery_address->latitude = 0;
						$tbldelivery_address->longitude = 0;
						$tbldelivery_address->contact = $client_name;
						$tbldelivery_address->idproject_site = $lastid;
						
						if($tbldelivery_address->save()){
							$delivery = $tbldelivery_address->id;
							return $delivery;
						} else {
							return 0;
						}
					}catch(Exception $ex){
						return 0;
					}
				} else {
					return 0;
				}
			}catch(Exception $ex){
				return 0;
			}
		}
	}
	
	/** Save to order **/
	public function convert_to_order(Request $request){
		$unique_id = $request['uniqueid'];
		$organization = $request['idorg'];
		$iddelivery_address = $request['iddelivery_address'];
		$is_quote = $request['is_quote'];
		$idsupplier = 11;
		$idproject_site = null;
		$date = $request['date'];
		$idbinservice = $request['idbinservice'];
		$isupdate = $request['isupdate'];
		$binsize = $request['binsize'];
		$delivery_date = $request['delivery_date'];
		$collection_date = $request['collection_date'];
		$price = $request['price'];
		$delivery_comments = $request['delivery_comments'];
		
		$phone = $request['work_phone'];
		$email_address = $request['email_address'];
		$bill_address = $request['bill_address'];
		$client_name = $request['client_name'];
		$street_number = $request['street_number'];
		$street_name = $request['street_name'];
		$postcode = $request['postcode'];
		$suburb = $request['suburb'];
		
		$validate = Validator::make($request->all(), [
			'work_phone' => 'required',
			'email_address' => 'required',
			'bill_address' => 'required',
			'client_name' => 'required',
			'street_number' => 'required',
			'street_name' => 'required',
			'postcode' => 'required',
			'binsize' => 'required',
		]);
		
		
		if($validate->fails()){
			$data = array(
				'status' => 0,
				'tblorderservice' => null,
				'tblorder_items' => null
			);
			return response()->json($data);
		} 
		
		$organization = $this->add_new_org($phone, $email_address, $bill_address, $client_name);
		//if(is_null($organization)){
		//	$organization = $this->add_new_org($phone, $email_address, $bill_address, $client_name);
		//} 
		
		if(is_null($iddelivery_address) && !is_null($organization)){
			$iddelivery_address = $this->add_new_delivery($street_number, $street_name, $postcode, $suburb, $organization, $client_name);
		}
		
		if(!is_null($iddelivery_address)){
			$idproject_site = DB::table('tbldelivery_address')
			->select('tbldelivery_address.idproject_site')
			->where([
				'iddelivery_address' => $iddelivery_address
			])
			->first();
		}
		
		$check_orders = $this->check_orders($unique_id);
		
		if(is_null($check_orders) && $check_orders == null){
			$bookingprice = $this->get_bookingprice();
			$tblorderservice = new tblorderservice();
			$tblorderservice->idConsumer = $organization;
			$tblorderservice->idproject_sites = $idproject_site->idproject_site;
			$tblorderservice->paymentUniqueCode = $unique_id;
			$tblorderservice->subtotal = 0;
			$tblorderservice->gst = 0;
			$tblorderservice->totalServiceCharge = 0;
			$tblorderservice->bookingfee = $bookingprice->price;
			$tblorderservice->idSupplier = $idsupplier;
			$tblorderservice->orderDate = date('Y-m-d', strtotime($date));
			$tblorderservice->movetojobs = 1;
			$tblorderservice->is_quote = $is_quote;
			$tblorderservice->is_paid = 0;
			
			if($tblorderservice->save()){
				/* insert to jobcard */
				$lastid = $tblorderservice->id;
				$tbljobcards = new tbljobcards();
				$tbljobcards->delivery_date = date('Y-m-d', strtotime($date));
				$tbljobcards->orderid = $lastid;
				$tbljobcards->status = 1;
				$tbljobcards->is_paid = 0;
				$tbljobcards->save();
				
				$jobcardid = $tbljobcards->id;
				$unique = $this->generateUniqueID();
				/* insert to orderitem */
				$tblordersitem = new tblordersitem();
				$tblordersitem->idorder = $lastid;
				$tblordersitem->idbinservice = $idbinservice;
				$tblordersitem->idsize =  $binsize;
				$tblordersitem->iddelivery_address = $iddelivery_address;
				$tblordersitem->delivery_date = date('Y-m-d', strtotime($delivery_date));
				$tblordersitem->collection_date = date('Y-m-d', strtotime($collection_date));
				$tblordersitem->subtotal =$price;
				$tblordersitem->attribute = 1;
				$tblordersitem->quantity = 1;
				$tblordersitem->delivery_comments = $delivery_comments;
				$tblordersitem->unique_id = $unique;
				
				if($tblordersitem->save()){
					/* insert to orderstatus */
					$lastorderitems = $tblordersitem->id;
					$tblorderstatus = new tblorderstatus();
					$tblorderstatus->idOrder = $lastid;
					$tblorderstatus->status = 1;
					$tblorderstatus->save();
					
					$del_date = date('Y-m-d', strtotime( $delivery_date));
					$col_date =  date('Y-m-d', strtotime($collection_date));
					$price = $price;
					
					$task_1 = $this->generateUniqueID();
					$task_2 = $this->generateUniqueID();
					//$this->update_stocking($orders_item[$k]['idbinservice'], $del_date, $col_date, $price);
					$tbltasks = new tbltasks();
					$tbltasks->idjobcard = $jobcardid;
					$tbltasks->job_type = 1;
					$tbltasks->jobtype_word = 'Delivery';
					$tbltasks->status = 1;
					$tbltasks->starttime = date('Y-m-d h:m:s',strtotime( $delivery_date));
					$tbltasks->assignedto = session('idUser');
					$tbltasks->idorderitems = $lastorderitems;
					$tbltasks->orderitem = $lastorderitems;
					$tbltasks->note = $delivery_comments;
					$tbltasks->driver_updated = 0;
					$tbltasks->is_paid = 0;
					$tbltasks->active_task = 0;
					$tbltasks->taskname = "D-".$task_1;
					$tbltasks->unique_id = "D-".$task_1;
					$tbltasks->driver_notice  = 0;
					$tbltasks->save();
					
					$tbltasks = new tbltasks();
					$tbltasks->idjobcard = $jobcardid;
					$tbltasks->job_type = 2;
					$tbltasks->jobtype_word = 'Collection';
					$tbltasks->status = 1;
					$tbltasks->starttime = date('Y-m-d h:m:s',strtotime($collection_date));
					$tbltasks->assignedto = session('idUser');
					$tbltasks->idorderitems = $lastorderitems;
					$tbltasks->orderitem = $lastorderitems;
					$tbltasks->note = $delivery_comments;
					$tbltasks->driver_updated = 0;
					$tbltasks->is_paid = 0;
					$tbltasks->active_task = 0;
					$tbltasks->taskname = "C-".$task_2;
					$tbltasks->unique_id = "C-".$task_2;
					$tbltasks->driver_notice  = 0;
					$tbltasks->save();
					
				} else {
					$data = array(
						'status' => 0,
						'tblorderservice' => null,
						'tblorder_items' => null
					);
				}
				
				$data = array(
					'status' => 1,
					'tblorderservice' => $lastid,
					'tblorder_items' => $lastorderitems
				);
				return response()->json($data);
			} else {
				$data = array(
					'status' => 0,
					'tblorderservice' => null,
					'tblorder_items' => null
				);
			}
		} elseif(!is_null($check_orders) && !is_null($check_orders->idOrderService)){
			
			$lastid = $check_orders->idOrderService;
			
			/* check jobcard first */
			$jobcard = DB::table('tbljobcards')
			->where([
				'orderid' => $lastid
			])
			->first();
			
			if(!is_null($jobcard)){
				$jobcardid = $jobcard->id;
			} else {
				/* if null then insert jobcard */
				$tbljobcards = new tbljobcards();
				$tbljobcards->delivery_date = date('Y-m-d', strtotime($date));
				$tbljobcards->orderid = $lastid;
				$tbljobcards->status = 1;
				$tbljobcards->is_paid = 0;
				$tbljobcards->save();
				$jobcardid = $tbljobcards->id;
			}
			
			$orderitem_unique_id = $this->generateUniqueID();
			/* insert to orderitem */
			$tblordersitem = new tblordersitem();
			$tblordersitem->idorder = $lastid;
			$tblordersitem->idbinservice = $idbinservice;
			$tblordersitem->idsize =  $binsize;
			$tblordersitem->iddelivery_address = $iddelivery_address;
			$tblordersitem->delivery_date = date('Y-m-d', strtotime( $delivery_date));
			$tblordersitem->collection_date = date('Y-m-d', strtotime($collection_date));
			$tblordersitem->subtotal =$price;
			$tblordersitem->attribute = 1;
			$tblordersitem->quantity = 1;
			$tblordersitem->delivery_comments = $delivery_comments;
			$tblordersitem->unique_id = $orderitem_unique_id;
			
			if($tblordersitem->save()){
				/* insert to orderstatus */
				$lastorderitems = $tblordersitem->id;
				$tblorderstatus = new tblorderstatus();
				$tblorderstatus->idOrder = $lastid;
				$tblorderstatus->status = 1;
				$tblorderstatus->save();
				
				$del_date = date('Y-m-d', strtotime( $delivery_date));
				$col_date =  date('Y-m-d', strtotime($collection_date));
				$price = $price;
				
				//$this->update_stocking($orders_item[$k]['idbinservice'], $del_date, $col_date, $price);
				$task_1 = $this->generateUniqueID();
				$task_2 = $this->generateUniqueID();
				$tbltasks = new tbltasks();
				$tbltasks->idjobcard = $jobcardid;
				$tbltasks->job_type = 1;
				$tbltasks->status = 1;
				$tbltasks->starttime = date('Y-m-d h:m:s',strtotime( $delivery_date));
				$tbltasks->assignedto = session('idUser');
				$tbltasks->idorderitems = $lastorderitems;
				$tbltasks->orderitem = $lastorderitems;
				$tbltasks->note = $delivery_comments;
				$tbltasks->driver_updated = 0;
				$tbltasks->taskname = "D-".$task_1;
				$tbltasks->is_paid = 0;
				$tbltasks->active_task = 0;
				$tbltasks->unique_id = "D-".$task_1;
				$tbltasks->driver_notice  = 0;
				$tbltasks->save();
				
				$tbltasks = new tbltasks();
				$tbltasks->idjobcard = $jobcardid;
				$tbltasks->job_type = 2;
				$tbltasks->status = 1;
				$tbltasks->starttime = date('Y-m-d h:m:s',strtotime($collection_date));
				$tbltasks->assignedto = session('idUser');
				$tbltasks->idorderitems = $lastorderitems;
				$tbltasks->orderitem = $lastorderitems;
				$tbltasks->note = $delivery_comments;
				$tbltasks->driver_updated = 0;
				$tbltasks->taskname = "C-".$task_2;
				$tbltasks->is_paid = 0;
				$tbltasks->active_task = 0;
				$tbltasks->unique_id = "C-".$task_2;
				$tbltasks->driver_notice  = 0;
				$tbltasks->save();

			} else {
				$data = array(
					'status' => 0,
					'tblorderservice' => null,
					'tblorder_items' => null
				);
			}
			
			$data = array(
				'status' => 1,
				'tblorderservice' => $lastid,
				'tblorder_items' => $lastorderitems
			);
			return response()->json($data);
		}
	}
	
	/* get master order data */
	public function load_order_based_id(Request $request){
		$idorder = $request['idorder'];
		$get_order_detail = DB::table('tblorderservice')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.idorganization', '=', 'tblorganization.id')
		->leftJoin('tbldelivery_address', 'tblproject_sites.id', '=', 'tbldelivery_address.idproject_site')
		->select('tblorderservice.idOrderService', 'tblorderservice.paymentUniqueCode',
		'tblorderservice.orderDate','tblorganization.id as idorg','tblorganization.organization_name','tblorganization.address',
		'tblorganization.email_address','tblorganization.phone')
		->groupBy('tblorderservice.idOrderService', 'tblorderservice.paymentUniqueCode',
		'tblorderservice.orderDate','idorg','tblorganization.organization_name','tblorganization.address',
		'tblorganization.email_address','tblorganization.phone')
		->where([
			'tblorderservice.idOrderService' => $idorder,
		])
		->first();
		
		if(!is_null($get_order_detail)){
			$data = array(
				'status' => 1,
				'result' => $get_order_detail,
			);
		} else {
			$data = array(
				'status' => 0, 
				'result' => null
			);
		}
		return response()->json($data);
	}
	
	/* get items order detail */
	public function load_orderitems_based_id(Request $request){
		$idorderitems = $request['idorderitems'];
		$idorder = $request['idorder'];
		$get_order_detail = DB::table('tblorder_items')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tblorder_items.idorder')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.idorganization', '=', 'tblorganization.id')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->select('tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tbldelivery_address.post_code','tbldelivery_address.suburb','tblorder_items.subtotal', 
		'tblorder_items.id as idorderitems','tblorder_items.id as idorderitems',
		'tblorder_items.delivery_date','tblorder_items.collection_date','tblsize.size','tblorder_items.unique_id'
		)
		->groupBy('tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tbldelivery_address.post_code','tbldelivery_address.suburb','tblorder_items.subtotal', 
		'idorderitems','tblorder_items.delivery_date',
		'tblorder_items.collection_date','tblsize.size','tblorder_items.unique_id')
		->where([
			'tblorderservice.idOrderService' => $idorder,
		])
		->get();
		
		if(!is_null($get_order_detail)){
			$data = array(
				'status' => 1,
				'result' => $get_order_detail,
			);
		} else {
			$data = array(
				'status' => 0, 
				'result' => null
			);
		}
		return response()->json($data);
	}
	
	/** Calculating totals **/
	public function calculating_total(Request $request){
		$ordermain_payment = $request['ordermain_payment'];
		$bookingprice = $this->get_bookingprice();
		$total = 0;
		$subtotal = 0;
		$gst = 0;
		
		/** get order details based on order master */
		$order_detail = DB::table('tblorder_items')
		->where([
			'tblorder_items.idorder' => $ordermain_payment
		])
		->get();
		/********************************************/
		if(!is_null($order_detail)){
			foreach($order_detail as $value){
				$subtotal += $value->subtotal;
				$total += $value->subtotal;
			}
		} else {
			$data = array(
				'status' => 0, 
				'total' => 0,
				'gst' => 0,
				'subtotal' => 0,
				'bookingfee' => $bookingprice->price
			);
			return response()->json($data);
		}
		
		$total = $total + $bookingprice->price;
		$gst = $total /110  * 10;
		
		tblorderservice::where([
			'idOrderService' => $ordermain_payment
		])
		->update([
			'totalServiceCharge' => $total,
			'gst' => $gst,
			'subtotal' => $subtotal,
			'bookingfee' => $bookingprice->price
		]);
		
		$data = array(
			'status' => 1, 
			'total' => $total,
			'gst' => $gst,
			'subtotal' => $subtotal,
			'bookingfee' => $bookingprice->price
		);
		return response()->json($data);
	}
	
	/** Take Payments**/
	public function take_payments(Request $request){
		$ordermain_payment = $request['ordermain_payment'];
		
		/** get jobcards based on the id order */
		$jobcard = DB::table('tbljobcards')
		->where([
			'orderid' => $ordermain_payment
		])
		->first();
		/********************************************/
		if(!is_null($jobcard)){
			tblorderservice::where([
				'idOrderService' => $ordermain_payment
			])
			->update([
				'is_paid' => 1, 
				'is_quote' => 0,
			]);

			
			tblorderstatus::where([
				'idOrder' => $ordermain_payment
			])
			->update([
				'status' => 3
			]);
			
			tbljobcards::where([
				'id' => $jobcard->id
			])
			->update([
				'is_paid' => 1
			]);
			
			tbltasks::where([
				'idjobcard' => $jobcard->id
			])
			->update([
				'is_paid' => 1,
				'active_task' => 0,
				'driver_notice' => 1
			]);
			
		} else {
			$data = array(
				'status' => 0, 
			);
			return response()->json($data);
		}
		
		
		$data = array(
			'status' => 1
		);
		return response()->json($data);
	}
}
