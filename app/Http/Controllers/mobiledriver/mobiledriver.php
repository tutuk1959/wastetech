<?php

namespace App\Http\Controllers\mobiledriver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\tblbintype;
use App\tblsize;
use App\tblservicearea;
use App\tblsupplier;
use App\tbluse;
use App\User;
use App\user_confirmation;
use App\tbltask_has_status;
use App\tbltask_has_time;
use Validator;
use App\Transformer\taskMasterTransformer;
use App\Transformer\taskProgressTransformer;
use App\Transformer\driverTransformer;
use App\Transformer\statusTransformer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\tbltasks;
use Carbon\Carbon;

class mobiledriver extends Controller
{
	public $successStatus = 200;

	public function __construct(Response $response){
        $this->response = $response;
    }

	/**
	* Login
	* Sent :
		1. Email
		2. Password
	**/

	public function login(Request $request){

		$email = $request['email'];
		$password = $request['password'];
		if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
			$user = Auth::user();
			$success['token'] =  $user->createToken('MyApp')-> accessToken;
			return response()->json(['success' => $success], $this-> successStatus);
		}
		else{
			return response()->json(['error'=>'Unauthorised']);
		}
    }

	/**
	* Register
	* Sent :
		1. organizationName
		2. contactName
		3. phoneNumber
		4. mobilePhone
		5. emailAddress
		6. username
		7. password
		8. retype_password
	**/

	public function register(Request $request){
		$organizationName = $request['organizationName'];
		$contactName = $request['contactName'];
		$phoneNumber = $request['daytimePhoneNumber'];
		$mobilePhone = $request['mobilePhoneNumber'];
		$emailAddress = $request['emailAddress'];
		$username = $request['username'];
		$password = $request['password'];
		$retypepassword = $request['retype_password'];

		$validate = Validator::make($request->all(), [
			'contactName' => 'required',
			'daytimePhoneNumber' => 'required',
			'mobilePhoneNumber' => 'required',
			'organizationName' => 'required',
			'emailAddress' => 'required|email',
			'username' => 'required',
			'password' => 'required',
			'retype_password' => 'required|same:password'
		], [
			'emailAddress.required' => 'Please input a valid email address',
		]);

		if($validate->fails()){
			return response()->json(['error'=>'Validate errors']);
		}

		if ($request->isMethod('post') == 'POST'){
			$countSupplier = DB::table('tblsupplier')
			->leftJoin('tbluser', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier')
			->where('tbluser.username',$username)
			->first();
			if(!is_null($countSupplier)){
				return response()->json(['error'=>'Same user or company exists on our system. Please try another one  or login to continue']);
			} else {
				$tblsupplier = new tblsupplier();
				/** ADD SUPPLIER FIRST **/
				$tblsupplier->name = $organizationName;
				$tblsupplier->contactName = $contactName;
				$tblsupplier->phonenumber = $phoneNumber;
				$tblsupplier->mobilePhone = $mobilePhone;
				$tblsupplier->email = $emailAddress;
				$tblsupplier->comments = '';
				$tblsupplier->isOpenSaturday = 0;
				$tblsupplier->isOpenSunday = 0;
				$tblsupplier->abn = '';

				if ($tblsupplier->save()){
					$lastId = DB::table('tblsupplier')
						->select('idSupplier', 'name', 'contactName', 'email')
						->orderBy('idSupplier', 'DESC')
						->first();
				}

				$countUser = DB::table('tbluser')
					->select('idUser', 'idSupplier', 'username','password')
					->where('idSupplier', $lastId->idSupplier)
					->first();
				if(!is_null($countUser)){
					return response()->json(['error'=>'One organization should has 1 account']);
				} else {
					$tbluser = new tbluse();
					$tbluser->idSupplier = $lastId->idSupplier;
					$tbluser->username = $username;
					$tbluser->password = md5($password);
					$tbluser->role = '2';
					$tbluser->isActive = '0';
					$tbluser->adminApproved = '0';
					$tbluser->userStatus = '2';
					$todaydate = date('Y-m-d', strtotime('now'));
					$tbluser->registerDate = $todaydate;
					$tbluser->adminNotifications = '1';

					if ($tbluser->save()){
						$lastId = DB::table('tbluser')
						->select('idUser', 'idSupplier','username', 'role')
						->orderBy('idUser', 'DESC')
						->first();

						$webmasterId = DB::table('tblsupplier')
						->leftJoin('tbluser', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
						->select('tblsupplier.name', 'tblsupplier.email')
						->where([
							'tbluser.role' => 1,
							'tbluser.adminNotifications' => 1
						])
						->first();

						$verification_code = str_random(30);
						$user_confirmation = new user_confirmation;
						$user_confirmation->idSupplier = $lastId->idSupplier;
						$user_confirmation->idUser = $lastId->idUser;
						$user_confirmation->token = $verification_code;

						if ($user_confirmation->save()){
							$input = array();
							$input['idUser'] = $lastId->idUser;
							$input['idSupplier'] = $lastId->idSupplier;
							$input['email'] = $emailAddress;
							$input['username'] = $username;
							$input['password'] = bcrypt($input['password']);
							$input['role'] = '2';
							$input['isActive'] = '0';
							$input['adminApproved'] = '0';
							$input['userStatus'] = '2';
							$todaydate = date('Y-m-d', strtotime('now'));
							$input['registerDate'] = $todaydate;
							$input['adminNotifications'] = '1';

							$user = User::create($input);
							$success['token'] =  $user->createToken('nApp')->accessToken;
							$success['id'] =  $user->id;
							$success['foreginId'] =  $user->idUser;
							$success['idSupplier'] =  $user->idSupplier;
							$success['organization'] =  $organizationName;
							$success['username'] =  $username;
							$success['verificationCode'] = $verification_code;

							return response()->json(['success'=>$success], $this->successStatus);
						} else {
							return response()->json(['error'=>'Internal server error'], 500);
						}
					} else {
						return response()->json(['error'=>'Internal server error'], 500);
					}
				}
			}
		}
	}

	/**
	* Change Password
	* Sent :
		1. ID User
		2. Password
	**/

	public function change_password(Request $request){
		$iduser = Auth::id();
		$password = $request['password'];
		$confirmpassword = $request['password'];
		$validate = Validator::make($request->all(), [
			'password' => 'required',
			'retype_password' => 'required|same:password'
		], [
			'emailAddress.required' => 'Please input a valid email address',
		]);

		if($validate->fails()){
			return response()->json(['error'=> $validator->messages()]);
		}

		User::where('id', '=', $iduser)->update([
			'password' => bcrypt($password)
		]);

		$success['iduser'] = $iduser;
		$success['message'] = 'Password Changed';
		return response()->json(['success'=>$success], $this->successStatus);
    }

	/**
	* Task Master Belongs to Driver
	* Sent :
		1. Id Driver
	**/
	public function task_master(Request $request){
		$iddriver = Auth::user()->driver->id;
		$records = DB::table('tbltasks')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.id', '=', 'tblorderservice.idproject_sites')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->leftJoin('tblbinservice', 'tblorder_items.idbinservice', '=', 'tblbinservice.idBinService')
		->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblorder_items.idsize', '=', 'tblsize.idSize')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblstatus', 'tblstatus.id', '=','tbltasks.status')
		->select('tbltasks.id as idtask', 'tbltasks.taskname as tasktitle',  'tbltasks.jobtype_word as jobtype',
		'tblstatus.status as taskstatus',
		'tbltasks.note as tasknote', 'tbltasks.status as taskstatusid',
		'tbljobcards.is_paid as jobcards_orderstatus',
		'tbljobcards.id as idjobcard',

		'tblsupplier.name AS suppliername', 'tblsupplier.email AS supplieremail',
		'tblsupplier.phoneNumber AS supplierPhoneNumber','tblsupplier.mobilePhone AS supplierMobile',

		'tblorganization.organization_name AS customername', 'tblorganization.phone AS customernlandline',
		'tblorganization.phone AS customerphone',

		'tblproject_sites.sitename AS sitename',

		'tblorder_items.id as idorderitem', 'tblorder_items.unique_id as paymentrefrence',
		'tblorder_items.idbinservice as orderitemidbinservice',
		'tblorder_items.quantity as orderquantity','tblorder_items.delivery_comments as orderdeliverycomments',
		'tblorder_items.attribute as orderattribute','tblorder_items.delivery_date','tblorder_items.collection_date',

		'tbldelivery_address.unit_number', 'tbldelivery_address.lot_number',
		'tbldelivery_address.street_name','tbldelivery_address.suburb', 'tbldelivery_address.post_code',
		'tbldelivery_address.state', 'tbldelivery_address.latitude', 'tbldelivery_address.longitude', 'tbldelivery_address.contact',

		'tblbintype.name AS bintype', 'tblsize.size',
		'tbldrivers.id AS idDriver', 'tbldrivers.name as drivername','tbldrivers.onDuty',
		'tbltrucks.id as idtrucks','tbltrucks.name as truckname'
		)
		->where([
			'tbltasks.driver' => $iddriver,
			'tbljobcards.is_paid' => 1,
			'tbltasks.is_paid' => 1
		])
		->get();

		if(!$records->isEmpty()){
			return $this->response->withCollection($records, new  taskMasterTransformer());
		} else {
			return response()->json([]);
		}
	}


	/**
	* Task Pending Belongs to Driver (In-Active)
	* Sent :
		1. Id Driver
	**/
	public function task_pending(Request $request){
		$iddriver = Auth::user()->driver->id;
		$records = DB::table('tbltasks')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.id', '=', 'tblorderservice.idproject_sites')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->leftJoin('tblbinservice', 'tblorder_items.idbinservice', '=', 'tblbinservice.idBinService')
		->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblorder_items.idsize', '=', 'tblsize.idSize')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblstatus', 'tblstatus.id', '=','tbltasks.status')
		->select('tbltasks.id as idtask', 'tbltasks.taskname as tasktitle',  'tbltasks.jobtype_word as jobtype',
		'tblstatus.status as taskstatus',
		'tbltasks.note as tasknote', 'tbltasks.status as taskstatusid',
		'tbljobcards.is_paid as jobcards_orderstatus',
		'tbljobcards.id as idjobcard',

		'tblsupplier.name AS suppliername', 'tblsupplier.email AS supplieremail',
		'tblsupplier.phoneNumber AS supplierPhoneNumber','tblsupplier.mobilePhone AS supplierMobile',

		'tblorganization.organization_name AS customername', 'tblorganization.phone AS customernlandline',
		'tblorganization.phone AS customerphone',

		'tblproject_sites.sitename AS sitename',

		'tblorder_items.id as idorderitem', 'tblorder_items.unique_id as paymentrefrence',
		'tblorder_items.idbinservice as orderitemidbinservice',
		'tblorder_items.quantity as orderquantity','tblorder_items.delivery_comments as orderdeliverycomments',
		'tblorder_items.attribute as orderattribute','tblorder_items.delivery_date','tblorder_items.collection_date',

		'tbldelivery_address.unit_number', 'tbldelivery_address.lot_number',
		'tbldelivery_address.street_name','tbldelivery_address.suburb', 'tbldelivery_address.post_code',
		'tbldelivery_address.state', 'tbldelivery_address.latitude', 'tbldelivery_address.longitude', 'tbldelivery_address.contact',

		'tblbintype.name AS bintype', 'tblsize.size',
		'tbldrivers.id AS idDriver', 'tbldrivers.name as drivername','tbldrivers.onDuty',
		'tbltrucks.id as idtrucks','tbltrucks.name as truckname'
		)
		->where([
			'tbltasks.driver' => $iddriver,
			'tbljobcards.is_paid' => 1,
			'tbltasks.is_paid' => 1
		])
		->where('tbltasks.status', '<>', 2)
		->where('tbltasks.status', '<>', 3)
		->where('tbltasks.status', '<>', 4)
		->get();

		if(!$records->isEmpty()){
			return $this->response->withCollection($records, new  taskMasterTransformer());
		} else {
			return response()->json([]);
		}
	}

	/**
	* Task Non-Pending Belongs to Driver (Active Task)
	* Sent :
		1. Id Driver
	**/
	public function task_nonpending(Request $request){
		$iddriver = Auth::user()->driver->id;
		$records = DB::table('tbltasks')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.id', '=', 'tblorderservice.idproject_sites')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->leftJoin('tblbinservice', 'tblorder_items.idbinservice', '=', 'tblbinservice.idBinService')
		->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblorder_items.idsize', '=', 'tblsize.idSize')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblstatus', 'tblstatus.id', '=','tbltasks.status')
		->select('tbltasks.id as idtask', 'tbltasks.taskname as tasktitle',  'tbltasks.jobtype_word as jobtype',
		'tblstatus.status as taskstatus',
		'tbltasks.note as tasknote', 'tbltasks.status as taskstatusid',
		'tbljobcards.is_paid as jobcards_orderstatus',
		'tbljobcards.id as idjobcard',

		'tblsupplier.name AS suppliername', 'tblsupplier.email AS supplieremail',
		'tblsupplier.phoneNumber AS supplierPhoneNumber','tblsupplier.mobilePhone AS supplierMobile',

		'tblorganization.organization_name AS customername', 'tblorganization.phone AS customernlandline',
		'tblorganization.phone AS customerphone',

		'tblproject_sites.sitename AS sitename',

		'tblorder_items.id as idorderitem', 'tblorder_items.unique_id as paymentrefrence',
		'tblorder_items.idbinservice as orderitemidbinservice',
		'tblorder_items.quantity as orderquantity','tblorder_items.delivery_comments as orderdeliverycomments',
		'tblorder_items.attribute as orderattribute','tblorder_items.delivery_date','tblorder_items.collection_date',

		'tbldelivery_address.unit_number', 'tbldelivery_address.lot_number',
		'tbldelivery_address.street_name','tbldelivery_address.suburb', 'tbldelivery_address.post_code',
		'tbldelivery_address.state', 'tbldelivery_address.latitude', 'tbldelivery_address.longitude', 'tbldelivery_address.contact',

		'tblbintype.name AS bintype', 'tblsize.size',
		'tbldrivers.id AS idDriver', 'tbldrivers.name as drivername','tbldrivers.onDuty',
		'tbltrucks.id as idtrucks','tbltrucks.name as truckname'
		)
		->where([
			'tbltasks.driver' => $iddriver,
			'tbljobcards.is_paid' => 1,
			'tbltasks.is_paid' => 1
		])
		->where('tbltasks.status', '<>', 1)
		->where('tbltasks.status', '<>', 7)
		->where('tbltasks.status', '<>', 6)
		->where('tbltasks.status', '<>', 5)
		->get();

		if(!$records->isEmpty()){
			return $this->response->withCollection($records, new  taskMasterTransformer());
		} else {
			return response()->json([]);
		}
	}

	/**
	* Task Master Belongs to Driver (Based on Date)
	* Sent :
		1. Id Driver
		2. Start Range
		3. End Range
	**/
	public function task_master_date(Request $request){

		$iddriver = $request['iddriver'];
		$start_range = $request['start_date'];
		$end_range = $request['end_date'];

		$records = DB::table('tbltasks')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.id', '=', 'tblorderservice.idproject_sites')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->leftJoin('tblbinservice', 'tblorder_items.idbinservice', '=', 'tblbinservice.idBinService')
		->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblorder_items.idsize', '=', 'tblsize.idSize')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblstatus', 'tblstatus.id', '=','tbltasks.status')
		->select('tbltasks.id as idtask', 'tbltasks.taskname as tasktitle',  'tbltasks.jobtype_word as jobtype',
		'tblstatus.status as taskstatus',
		'tbltasks.note as tasknote', 'tbltasks.status as taskstatusid',
		'tbljobcards.is_paid as jobcards_orderstatus',
		'tbljobcards.id as idjobcard',

		'tblsupplier.name AS suppliername', 'tblsupplier.email AS supplieremail',
		'tblsupplier.phoneNumber AS supplierPhoneNumber','tblsupplier.mobilePhone AS supplierMobile',

		'tblorganization.organization_name AS customername', 'tblorganization.phone AS customernlandline',
		'tblorganization.phone AS customerphone',

		'tblproject_sites.sitename AS sitename',

		'tblorder_items.id as idorderitem', 'tblorder_items.unique_id as paymentrefrence',
		'tblorder_items.idbinservice as orderitemidbinservice',
		'tblorder_items.quantity as orderquantity','tblorder_items.delivery_comments as orderdeliverycomments',
		'tblorder_items.attribute as orderattribute','tblorder_items.delivery_date','tblorder_items.collection_date',

		'tbldelivery_address.unit_number', 'tbldelivery_address.lot_number',
		'tbldelivery_address.street_name','tbldelivery_address.suburb', 'tbldelivery_address.post_code',
		'tbldelivery_address.state', 'tbldelivery_address.latitude', 'tbldelivery_address.longitude', 'tbldelivery_address.contact',

		'tblbintype.name AS bintype', 'tblsize.size',
		'tbldrivers.id AS idDriver', 'tbldrivers.name as drivername','tbldrivers.onDuty',
		'tbltrucks.id as idtrucks','tbltrucks.name as truckname'
		)
		->where([
			'tbltasks.driver' => $iddriver,
			'tbljobcards.is_paid' => 1,
			'tbltasks.is_paid' => 1
		])
		->whereBetween('tbltasks.starttime', [$start_range, $end_range])
		->get();

		if(!$records->isEmpty()){
			return $this->response->withCollection($records, new  taskMasterTransformer());
		} else {
			return response()->json([]);
		}
	}

	/**
	* Task Master Belongs to Driver (Based on Uncompleted)
	* Sent :
		1. Id Driver
	**/
	public function task_master_uncompleted(Request $request){

		$iddriver = $request['iddriver'];

		$records = DB::table('tbltasks')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.id', '=', 'tblorderservice.idproject_sites')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->leftJoin('tblbinservice', 'tblorder_items.idbinservice', '=', 'tblbinservice.idBinService')
		->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblorder_items.idsize', '=', 'tblsize.idSize')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblstatus', 'tblstatus.id', '=','tbltasks.status')
		->select('tbltasks.id as idtask', 'tbltasks.taskname as tasktitle',  'tbltasks.jobtype_word as jobtype',
		'tblstatus.status as taskstatus',
		'tbltasks.note as tasknote', 'tbltasks.status as taskstatusid',
		'tbljobcards.is_paid as jobcards_orderstatus',
		'tbljobcards.id as idjobcard',

		'tblsupplier.name AS suppliername', 'tblsupplier.email AS supplieremail',
		'tblsupplier.phoneNumber AS supplierPhoneNumber','tblsupplier.mobilePhone AS supplierMobile',

		'tblorganization.organization_name AS customername', 'tblorganization.phone AS customernlandline',
		'tblorganization.phone AS customerphone',

		'tblproject_sites.sitename AS sitename',

		'tblorder_items.id as idorderitem', 'tblorder_items.unique_id as paymentrefrence',
		'tblorder_items.idbinservice as orderitemidbinservice',
		'tblorder_items.quantity as orderquantity','tblorder_items.delivery_comments as orderdeliverycomments',
		'tblorder_items.attribute as orderattribute','tblorder_items.delivery_date','tblorder_items.collection_date',

		'tbldelivery_address.unit_number', 'tbldelivery_address.lot_number',
		'tbldelivery_address.street_name','tbldelivery_address.suburb', 'tbldelivery_address.post_code',
		'tbldelivery_address.state', 'tbldelivery_address.latitude', 'tbldelivery_address.longitude', 'tbldelivery_address.contact',

		'tblbintype.name AS bintype', 'tblsize.size',
		'tbldrivers.id AS idDriver', 'tbldrivers.name as drivername','tbldrivers.onDuty',
		'tbltrucks.id as idtrucks','tbltrucks.name as truckname'
		)
		->where([
			'tbltasks.driver' => $iddriver,
			'tbljobcards.is_paid' => 1,
			'tbltasks.is_paid' => 1
		])
		->where('tbltasks.status', '<>', 5)
		->where('tbltasks.status', '<>', 6)
		->get();

		if(!$records->isEmpty()){
			return $this->response->withCollection($records, new  taskMasterTransformer());
		} else {
			return response()->json([]);
		}
	}

	/**
	* Task Master Belongs to Driver (Based on Completed)
	* Sent :
		1. Id Driver
	**/
	public function task_master_completed(Request $request){

		$iddriver = $request['iddriver'];
		$start_range = $request['start_date'];
		$end_range = $request['end_date'];

		$records = DB::table('tbltasks')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.id', '=', 'tblorderservice.idproject_sites')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->leftJoin('tblbinservice', 'tblorder_items.idbinservice', '=', 'tblbinservice.idBinService')
		->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblorder_items.idsize', '=', 'tblsize.idSize')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblstatus', 'tblstatus.id', '=','tbltasks.status')
		->select('tbltasks.id as idtask', 'tbltasks.taskname as tasktitle',  'tbltasks.jobtype_word as jobtype',
		'tblstatus.status as taskstatus',
		'tbltasks.note as tasknote', 'tbltasks.status as taskstatusid',
		'tbljobcards.is_paid as jobcards_orderstatus',
		'tbljobcards.id as idjobcard',

		'tblsupplier.name AS suppliername', 'tblsupplier.email AS supplieremail',
		'tblsupplier.phoneNumber AS supplierPhoneNumber','tblsupplier.mobilePhone AS supplierMobile',

		'tblorganization.organization_name AS customername', 'tblorganization.phone AS customernlandline',
		'tblorganization.phone AS customerphone',

		'tblproject_sites.sitename AS sitename',

		'tblorder_items.id as idorderitem', 'tblorder_items.unique_id as paymentrefrence',
		'tblorder_items.idbinservice as orderitemidbinservice',
		'tblorder_items.quantity as orderquantity','tblorder_items.delivery_comments as orderdeliverycomments',
		'tblorder_items.attribute as orderattribute','tblorder_items.delivery_date','tblorder_items.collection_date',

		'tbldelivery_address.unit_number', 'tbldelivery_address.lot_number',
		'tbldelivery_address.street_name','tbldelivery_address.suburb', 'tbldelivery_address.post_code',
		'tbldelivery_address.state', 'tbldelivery_address.latitude', 'tbldelivery_address.longitude', 'tbldelivery_address.contact',

		'tblbintype.name AS bintype', 'tblsize.size',
		'tbldrivers.id AS idDriver', 'tbldrivers.name as drivername','tbldrivers.onDuty',
		'tbltrucks.id as idtrucks','tbltrucks.name as truckname'
		)
		->where([
			'tbltasks.driver' => $iddriver,
			'tbljobcards.is_paid' => 1,
			'tbltasks.is_paid' => 1
		])
		->where('tbltasks.status', '=', 5)
		->get();

		if(!$records->isEmpty()){
			return $this->response->withCollection($records, new  taskMasterTransformer());
		} else {
			return response()->json([]);
		}
	}

	/**
	* Task Progress Based on Task Status Changes and Task Times
	* Sent :
		1. ID Task
	**/
	public function task_progress(Request $request){
		$idtask = $request['idtask'];
		$records = DB::table('tbltasks')
		->join('tbltask_has_status', 'tbltasks.id', '=', 'tbltask_has_status.task_id')
		->leftJoin('tblstatus', 'tbltask_has_status.status_id', '=', 'tblstatus.id')
		->select(
			'tbltasks.id AS idtask',
			'tbltask_has_status.id as hastatusid',
			'tbltask_has_status.total_time as totaltime',
			'tblstatus.status'
		)
		->where('tbltasks.id', $idtask)
		->get();

		$records = $records->map(function ($item) {
			$item->has_times = DB::table('tbltask_has_time')
								->select('tbltask_has_time.time as progresstime','tbltask_has_time.type_id as typeid')
								->where('task_status_id', $item->hastatusid)
								->get();
			return $item;
		});

		if ($records->count() > 0) {
			return $this->response->withCollection($records, new  taskProgressTransformer());
		} else {
			return response()->json([]);
		}
	}

	/**
	* Task Detail Based on Task Id
	* Sent :
		1. ID Task
	**/
	public function task_detail_id(Request $request){

		$idtask = $request['idtask'];

		$records = DB::table('tbltasks')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorganization.id', '=', 'tblorderservice.idConsumer')
		->leftJoin('tblproject_sites', 'tblproject_sites.id', '=', 'tblorderservice.idproject_sites')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->leftJoin('tblbinservice', 'tblorder_items.idbinservice', '=', 'tblbinservice.idBinService')
		->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblorder_items.idsize', '=', 'tblsize.idSize')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblstatus', 'tblstatus.id', '=','tbltasks.status')
		->select('tbltasks.id as idtask', 'tbltasks.taskname as tasktitle',  'tbltasks.jobtype_word as jobtype',
		'tblstatus.status as taskstatus',
		'tbltasks.note as tasknote', 'tbltasks.status as taskstatusid',
		'tbljobcards.is_paid as jobcards_orderstatus',
		'tbljobcards.id as idjobcard',

		'tblsupplier.name AS suppliername', 'tblsupplier.email AS supplieremail',
		'tblsupplier.phoneNumber AS supplierPhoneNumber','tblsupplier.mobilePhone AS supplierMobile',

		'tblorganization.organization_name AS customername', 'tblorganization.phone AS customernlandline',
		'tblorganization.phone AS customerphone',

		'tblproject_sites.sitename AS sitename',

		'tblorder_items.id as idorderitem', 'tblorder_items.unique_id as paymentrefrence',
		'tblorder_items.idbinservice as orderitemidbinservice',
		'tblorder_items.quantity as orderquantity','tblorder_items.delivery_comments as orderdeliverycomments',
		'tblorder_items.attribute as orderattribute','tblorder_items.delivery_date','tblorder_items.collection_date',

		'tbldelivery_address.unit_number', 'tbldelivery_address.lot_number',
		'tbldelivery_address.street_name','tbldelivery_address.suburb', 'tbldelivery_address.post_code',
		'tbldelivery_address.state', 'tbldelivery_address.latitude', 'tbldelivery_address.longitude', 'tbldelivery_address.contact',

		'tblbintype.name AS bintype', 'tblsize.size',
		'tbldrivers.id AS idDriver', 'tbldrivers.name as drivername','tbldrivers.onDuty',
		'tbltrucks.id as idtrucks','tbltrucks.name as truckname'
		)
		->where([
			'tbltasks.id' => $idtask,
		])
		->get();

		if(!$records->isEmpty()){
			return $this->response->withCollection($records, new  taskMasterTransformer());
		} else {
			return response()->json([]);
		}
	}

	/**
	* Bin Size Master
	* Sent :
		None
	**/
	public function bin_size(Request $request){

		$size = tblsize::paginate();
		if (!$size) {
			return $this->response->errorNotFound('No bin size data');
		} else {
			return $this->response->withPaginator($size, new  sizeTransformer());
		}
	}

	/**
	* Bin Type Master
	* Sent :
		None
	**/
	public function bin_type(Request $request){
		$bintype = tblbintype::where('CodeType','<>', '1212')->paginate();
		if (!$bintype) {
			return $this->response->errorNotFound('No bin type data');
		} else {
			return $this->response->withPaginator($bintype, new  bintypeTransformer());
		}
	}

	/**
	* Driver Detail
	* Sent :
		1. ID Driver
	**/
	public function driver_detail(Request $request){
		$iddriver = Auth::user()->driver->id;
		$records = DB::table('tbldrivers')
		->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tbldrivers.idSupplier')
		->select('tbldrivers.name AS driverName', 'tbldrivers.hours', 'tbldrivers.licence',
		'tbldrivers.status', 'tbldrivers.notes', 'tbldrivers.onDuty','tbldrivers.email_address','tbldrivers.mobilePhone',
		'tblsupplier.name AS supplierName', 'tblsupplier.fullAddress AS fullAddress')
		->where([
			'tbldrivers.id' => $iddriver
		])
		->first();

		if(!is_null($records)){
			return $this->response->withItem($records, new  driverTransformer());
		} else {
			return response()->json([]);
		}
	}

	/**
	* Fetch All Statuses
	* Sent :
		None
	**/
	public function all_status(Request $request){
		$records = DB::table('tblstatus')
		->get();

		if(!$records->isEmpty()){
			return $this->response->withCollection($records, new  driverTransformer());
		} else {
			return response()->json([]);
		}
	}


	public function updateTaskStatus(Request $request)
	{
		$task_id = $request['idtask'];
		$status_id = $request['status'];

		// $task = tbltasks::with(['statuses', 'statuses.hasTimes'])
		// 			->whereId($task_id)
		// 			->first();
		//
		// return $task;

		// only this task statuses have 2 time (start n stop)
		// 2	In Progress
		// 3	On Route
		// 4	On Site
		// 7	Paused
		$tasksThatHaveTypeId2 = collect([2, 3, 4, 7]);

		$task = tbltasks::whereId($task_id)
					->first();

		// if it's not the same status then the previous one
		if ($task->status != $status_id) {
			$taskHasStatus = $task->statuses->sortBy('id')->last();

			// if this is null (usually from 1 to 2)
			if (!is_null($taskHasStatus)) {
				// stop the last status
				$taskHasTime = tbltask_has_time::firstOrCreate([
									'task_status_id' => $taskHasStatus->id,
									'type_id' => 2
								], ['time' => Carbon::now()]);
			}

			// create tbltask_has_status, status history
			$taskHasStatus = tbltask_has_status::create([
									'task_id' => $task_id,
									'status_id' => $status_id,
									'total_time' => 0,
								]);
		} else {
			$taskHasStatus = $task->statuses->sortBy('id')->last();
		}

		// create tbltask_has_status
		$hasStart = tbltask_has_time::where([
							'task_status_id' => $taskHasStatus->id,
							'type_id' => 1
						])
						->first();

		// Start
		$taskHasTime = tbltask_has_time::create([
							'task_status_id' => $taskHasStatus->id,
							'type_id' => 1,
							'time' => Carbon::now()
						]);

		// update task status
		$task->status = $status_id;
		$task->save();

		// task with status & with time
		$task = tbltasks::with(['statuses', 'statuses.hasTimes'])
					->whereId($task_id)
					->first();

		return $task;
	}


	/**
	* Update Task Status and Update Actual Start Time and Actual End Time for both status In-Progress and Completed
	* Sent :
		1. ID Task
	**/
	public function update_task_status(Request $request){
		$idtask = $request['idtask'];
		$status = $request['status'];
		if(!is_null($idtask)){
			if($status <> 2 && $status <> 5){
				try{
					tbltasks::where([
						'id' => $request['idtask']
					])
					->update([
						'status' =>  $status
					]);

					$success['idtask'] = $idtask;
					$success['message'] = 'Status updated';
					$success['code'] = 1;

					return response()->json(['success' => $success], $this-> successStatus);
				} catch (Exception $ex){
					return response()->json([]);
				}
			} elseif($status == 2){
				try{
					tbltasks::where([
						'id' => $request['idtask']
					])
					->update([
						'status' =>  $status,
						'actual_tasktime' => time()
					]);

					$success['idtask'] = $idtask;
					$success['message'] = 'Status updated';
					$success['code'] = 1;

					return response()->json(['success' => $success], $this-> successStatus);
				} catch (Exception $ex){
					return response()->json([]);
				}
			} elseif($status == 5){
				try{
					tbltasks::where([
						'id' => $request['idtask']
					])
					->update([
						'status' =>  $status,
						'actual_endtime' => time()
					]);

					$success['idtask'] = $idtask;
					$success['message'] = 'Status updated';
					$success['code'] = 1;

					return response()->json(['success' => $success], $this-> successStatus);
				} catch (Exception $ex){
					return response()->json([]);
				}
			}

		} else {
			return response()->json([]);
		}
	}

	/**
	* Insert task has status after task master status updated
	* Sent :
		1. ID Task
		2. Status
	**/
	public function update_task_has_status(Request $request){
		$idtask = $request['idtask'];
		$status = $request['status'];
		if(!is_null($idtask)){
			$checkstatus = DB::table('tbltasks')
			->where([
				'id' => $idtask
			])
			->first();

			if(!is_null($checkstatus)){
				if($checkstatus->status != $status){
					try{
						/** check if table task has status exists with the current status change **/
						$checkhasstatus = DB::table('tbltask_has_status')
						->where([
							'task_id ' => $idtask,
							'status_id' => $status
						])
						->first();

						if(is_null($checkhasstatus)){
							$tbltasks = new tbltask_has_status();
							$tbltasks->task_id = $idtask;
							$tbltasks->status_id = $status;
							$tbltasks->total_time = 0;

							if($tbltasks->save()){
								$success['idhasstatus'] = $checkhasstatus->id;
								return response()->json(['success' => $success], $this-> successStatus);

							} else {
								return response()->json([]);
							}
						} else {
							return response()->json(['error' => 'No status changed. Status has existed']);
						}

					} catch(Exception $ex){
						return response()->json(['error' => 'No status changed']);
					}
				} else {
					return response()->json(['error' => 'No status changed']);
				}
			} else {
				return response()->json(['error' => 'Task not found']);
			}
		} else {
			return response()->json([]);
		}
	}

	/**
	* Insert / Update task has time once task status changed
	* Sent :
		1. ID Task
		2. Current Status
	**/
	public function update_task_has_time(Request $request){
		$idtask = $request['idtask'];
		$status = $request['status'];

		if(!is_null($idtask)){
			/** check tblhasstatus if the task has status log **/
			$checkstatus = DB::table('tbltask_has_status')
			->where([
				'task_id' => $idtask,
				'status_id' => $status
			])
			->first();

			if(!is_null($checkstatus)){

				/** check tblhastime if the task has time log **/
				$checkhastime = DB::table('tbltask_has_time')
				->where([
					'task_status_id' => $checkstatus->id
				])
				->first();

				if(is_null($checkhastime)){
					$tbltasks = new tbltask_has_time();
					$tbltasks->task_status_id = $checkstatus->id;
					$tbltasks->type_id = 1;
					$tbltasks->time = current_time();


					if($tbltasks->save()){

						$success['idtaskhasstatus'] = $checkstatus->id;;
						return response()->json(['success' => $success], $this-> successStatus);
					} else {
						return response()->json([]);
					}

				} else {
					/** if status Completed and Cancelled , return **/
					if($status == 1 || $status == 5 || $status == 6){
						return response()->json([]);
					} else {
						/** if tblhastime exists and type id equal 1 , insert its end phase **/
						if($checkhastime->type_id == 1){
							$tbltasks = new tbltask_has_time();
							$tbltasks->task_status_id = $checkstatus->id;
							$tbltasks->type_id = 2;
							$tbltasks->time = current_time();

							if($tbltasks->save()){
								$success['idtaskhasstatus'] = $checkstatus->id;;
								return response()->json(['success' => $success], $this-> successStatus);
							} else {
								return response()->json([]);
							}
						} else {
							return response()->json([]);
						}
					}
				}
			} else {
				return response()->json(['error' => 'Task not found']);
			}
		} else {
			return response()->json([]);
		}
	}

	/**
	* Update Task Geo Location based on Status Change
	* Sent :
		1. ID Task
		2. Latitude & Longitude
	**/
	public function update_task_geo_information(Request $request){
		$idtask = $request['idtask'];
		$latitude = $request['latitude'];
		$longitude = $request['longitude'];

		if(!is_null($idtask)){
			/** check tbltask if the task existed **/
			$checkstatus = DB::table('tbltasks')
			->where([
				'id' => $idtask,
			])
			->first();

			if(!is_null($checkstatus)){

				if($status==2){
					try{
						tbltasks::where([
							'id' => $request['idtask']
						])
						->update([
							'start_latitude' => $latitude,
							'start_longitude ' => $longitude,
						]);

						$success['idtask'] = $idtask;
						$success['message'] = 'Geo information updated';
						$success['code'] = 1;

						return response()->json(['success' => $success], $this-> successStatus);
					} catch (Exception $ex){
						return response()->json([]);
					}
				} elseif($status==3){
					try{
						tbltasks::where([
							'id' => $request['idtask']
						])
						->update([
							'onroute_latitude ' => $latitude,
							'onroute_longitude ' => $longitude,
						]);

						$success['idtask'] = $idtask;
						$success['message'] = 'Geo information updated';
						$success['code'] = 1;

						return response()->json(['success' => $success], $this-> successStatus);
					} catch (Exception $ex){
						return response()->json([]);
					}
				} elseif($status==4){
					try{
						tbltasks::where([
							'id' => $request['idtask']
						])
						->update([
							'stop_latitude ' => $latitude ,
							'stop_longitude ' => $longitude,
						]);

						$success['idtask'] = $idtask;
						$success['message'] = 'Geo information updated';
						$success['code'] = 1;

						return response()->json(['success' => $success], $this-> successStatus);
					} catch (Exception $ex){
						return response()->json([]);
					}
				}

			} else {
				return response()->json(['error' => 'Task not found']);
			}
		} else {
			return response()->json([]);
		}
	}

	/**
	* Update Task Distance
	* Sent :
		1. ID Task
		2. Final Distance
	**/
	public function update_task_geo_distance(Request $request){
		$idtask = $request['idtask'];
		$distance = $request['distance'];

		if(!is_null($idtask)){
			/** check tbltask if the task existed **/
			$checkstatus = DB::table('tbltasks')
			->where([
				'id' => $idtask,
			])
			->first();

			if(!is_null($checkstatus)){
				try{
					tbltasks::where([
						'id' => $request['idtask']
					])
					->update([
						'distance ' => $distance,
					]);

					$success['idtask'] = $idtask;
					$success['message'] = 'Distance updated';
					$success['code'] = 1;

					return response()->json(['success' => $success], $this-> successStatus);
				} catch (Exception $ex){
					return response()->json([]);
				}
			} else {
				return response()->json(['error' => 'Task not found']);
			}
		} else {
			return response()->json([]);
		}
	}
}
