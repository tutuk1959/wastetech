<?php

namespace App\Http\Controllers\scheduling;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\scheduling\jobcards;
use Validator;
use URL;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;
use Carbon\Carbon;
use App\tblcustomer;
use App\tblorderservice;
use App\tblorderstatus;
use App\tblordersitem;
use App\tblbinserviceupdates;
use App\tblbinservicestok;
use App\tbluse;
use App\tbljobcards;
use App\tbltasks;
use App\tbldrivers;
use App\tbltrucks;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Log;

class tasks extends Controller
{
	public function __construct(){
		
	}
	
	/**
	* Show tasks list
	*/
	public function list_index(){
		$new_uncompleted_orders = $this->count_new_uncompleted_tasks();
		$past_uncompleted_orders = $this->count_past_uncompleted_tasks();
		$completed_orders = $this->count_completed_tasks();
		$all_orders = $this->count_all_tasks();
		
		return view('scheduling.tasks_index', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders, 'all_orders' => $all_orders]);
	}
	
	/**
	* Show past uncompleted tasks view
	*/
	public function past_uncompleted_tasks(){
		$new_uncompleted_orders = $this->count_new_uncompleted_tasks();
		$past_uncompleted_orders = $this->count_past_uncompleted_tasks();
		$completed_orders = $this->count_completed_tasks();
		$all_orders = $this->count_all_tasks();
		$sizes = $this->getBinSize();
		$supplier = session('idSupplier');
		$tasks = $this->get_past_uncompleted_tasks();
		
		$statuses = $this->fetch_status();
		$drivers = $this->get_available_drivers();
		
		$baseprice_heavymixed = $this->getbinservicebaseprice_generalwaste($supplier, 2);
		
		foreach ($baseprice_heavymixed as $k => $v){
			$binservicestok_heavymixed[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_heavymixed[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,2);
		}
		
		return view('scheduling.tasks_past_uncompleted_tasks', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders,
		'drivers' => $drivers,'all_orders' => $all_orders,'tasks' => $tasks, 'sizes' => $sizes, 'statuses' => $statuses,
		'baseprice_heavymixed' => $baseprice_heavymixed,
		'binservicestok_heavymixed' => $binservicestok_heavymixed, 
		'binservicebaseprice_heavymixed' => $binservicebaseprice_heavymixed]);
	}
	
	/**
	* Show new uncompleted orders view
	*/
	public function today_new_tasks(){
		$new_uncompleted_orders = $this->count_new_uncompleted_tasks();
		$past_uncompleted_orders = $this->count_past_uncompleted_tasks();
		$completed_orders = $this->count_completed_tasks();
		$all_orders = $this->count_all_tasks();
		$sizes = $this->getBinSize();
		$supplier = session('idSupplier');
		$tasks = $this->get_new_uncompleted_tasks();
		$statuses = $this->fetch_status();
		$drivers = $this->get_available_drivers();
		
		$baseprice_heavymixed = $this->getbinservicebaseprice_generalwaste($supplier, 2);
		
		foreach ($baseprice_heavymixed as $k => $v){
			$binservicestok_heavymixed[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_heavymixed[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,2);
		}
		
		return view('scheduling.tasks_new_uncompleted_tasks', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders,
		'drivers' => $drivers,'all_orders' => $all_orders,'tasks' => $tasks, 'sizes' => $sizes, 'statuses' => $statuses,
		'baseprice_heavymixed' => $baseprice_heavymixed,
		'binservicestok_heavymixed' => $binservicestok_heavymixed, 
		'binservicebaseprice_heavymixed' => $binservicebaseprice_heavymixed]);
	}
	
	/**
	* Show new uncompleted orders view
	*/
	public function completed_tasks(){
		$new_uncompleted_orders = $this->count_new_uncompleted_tasks();
		$past_uncompleted_orders = $this->count_past_uncompleted_tasks();
		$completed_orders = $this->count_completed_tasks();
		$all_orders = $this->count_all_tasks();
		
		$tasks = $this->get_completed_tasks();
		
		return view('scheduling.tasks_completed_tasks', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders,  'all_orders' => $all_orders,'tasks' => $tasks]);
	}
	
	/**
	* Show master zone
	*/
	private function master_zone(){
		$servicearea = DB::table('tblservicearea')
		->where([
			'parentareacode' => 0000
		])
		->where('idArea','<>',1)
		->get();
		
		return $servicearea;
	}
	
	/**
	* Show child zone
	*/
	private function child_zone($masterzone){
		$servicearea = DB::table('tblservicearea')
		->where([
			'parentareacode' => $masterzone
		])
		->groupBy('zipcode')
		->get();
		
		return $servicearea;
	}
	
	/**
	* Show new zone orders view
	*/
	public function zone_tasks(){
		$new_uncompleted_orders = $this->count_new_uncompleted_tasks();
		$past_uncompleted_orders = $this->count_past_uncompleted_tasks();
		$completed_orders = $this->count_completed_tasks();
		$all_orders = $this->count_all_tasks();
		$sizes = $this->getBinSize();
		$supplier = session('idSupplier');
		$tasks = $this->get_zone_tasks();
		$statuses = $this->fetch_status();
		$drivers = $this->get_available_drivers();
		$master_zone = $this->master_zone();
		
		foreach($master_zone as $v){
			$child_zone[$v->zipcode] = $this->child_zone($v->zipcode);
		}

		return view('scheduling.tasks_zone_tasks', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders,
		'drivers' => $drivers,'all_orders' => $all_orders,
		'master_zone' => $master_zone, 'child_zone' => $child_zone,
		'tasks' => $tasks, 'sizes' => $sizes, 'statuses' => $statuses]);
	}
	
	/**
	* Show new date orders view
	*/
	public function date_tasks(){
		$new_uncompleted_orders = $this->count_new_uncompleted_tasks();
		$past_uncompleted_orders = $this->count_past_uncompleted_tasks();
		$completed_orders = $this->count_completed_tasks();
		$all_orders = $this->count_all_tasks();
		$sizes = $this->getBinSize();
		$supplier = session('idSupplier');
		$statuses = $this->fetch_status();
		$drivers = $this->get_available_drivers();
		$master_zone = $this->master_zone();
		$tasks = null;
		$fromdate = null;
		$todate = null;
		$baseprice_heavymixed = $this->getbinservicebaseprice_generalwaste($supplier, 2);
		
		foreach ($baseprice_heavymixed as $k => $v){
			$binservicestok_heavymixed[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_heavymixed[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,2);
		}
		
		return view('scheduling.tasks_date_tasks', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders,
		'drivers' => $drivers,'all_orders' => $all_orders,
		'tasks' => $tasks,'fromdate' => $fromdate, 'todate' => $todate,
		'sizes' => $sizes, 'statuses' => $statuses,
		'baseprice_heavymixed' => $baseprice_heavymixed, 
		'binservicestok_heavymixed' => $binservicestok_heavymixed, 
		'binservicebaseprice_heavymixed' => $binservicebaseprice_heavymixed]);
	}
	
	/** Date tasks zone process **/
	public function date_tasks_process(Request $request){
		$fromdate = $request['start_order_date'];
		$todate = $request['end_order_date'];
		
		$new_uncompleted_orders = $this->count_new_uncompleted_tasks();
		$past_uncompleted_orders = $this->count_past_uncompleted_tasks();
		$completed_orders = $this->count_completed_tasks();
		$all_orders = $this->count_all_tasks();
		$sizes = $this->getBinSize();
		$supplier = session('idSupplier');
		$statuses = $this->fetch_status();
		$drivers = $this->get_available_drivers();
		$master_zone = $this->master_zone();
		
		$baseprice_heavymixed = $this->getbinservicebaseprice_generalwaste($supplier, 2);
		
		foreach ($baseprice_heavymixed as $k => $v){
			$binservicestok_heavymixed[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_heavymixed[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,2);
		}
		
		$tasks = $this->get_date_tasks($fromdate,$todate);
		
		return view('scheduling.tasks_date_tasks', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders,
		'drivers' => $drivers,'all_orders' => $all_orders,
		'tasks' => $tasks, 'fromdate' => $fromdate, 'todate' => $todate,
		'sizes' => $sizes, 'statuses' => $statuses,
		'baseprice_heavymixed' => $baseprice_heavymixed, 
		'binservicestok_heavymixed' => $binservicestok_heavymixed, 
		'binservicebaseprice_heavymixed' => $binservicebaseprice_heavymixed]);
		
	}
	
	/**
	* Show tasks that belong to a selected Job
	*/
	public function tasks_by_job(Request $request){
		$idjobcards = $request['idjobcards'];
		$jobcards = jobcards::get_jobcards_by_id($idjobcards);
		$tasks = $this->fetch_task_by_job($idjobcards);
		$sizes = $this->getBinSize();
		$supplier = session('idSupplier');
		$drivers = $this->getDrivers();
		$trucks = $this->getTrucks();
		
		$baseprice_generalwaste = $this->getbinservicebaseprice_generalwaste($supplier, 1);
		
		foreach ($baseprice_generalwaste as $k => $v){
			$binservicestok_generalwaste[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_generalwaste[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,1);
		}
		
		$baseprice_heavymixed = $this->getbinservicebaseprice_generalwaste($supplier, 2);
		
		foreach ($baseprice_heavymixed as $k => $v){
			$binservicestok_heavymixed[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_heavymixed[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,2);
		}
		
		$baseprice_green = $this->getbinservicebaseprice_generalwaste($supplier, 4);
		
		foreach ($baseprice_green as $k => $v){
			$binservicestok_green[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_green[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,4);
		}
		
		$baseprice_dirt = $this->getbinservicebaseprice_generalwaste($supplier, 5);
		
		foreach ($baseprice_dirt as $k => $v){
			$binservicestok_dirt[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_dirt[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,5);
		}
		
		return view('scheduling.tasks_jobcard_id', ['jobcards' => $jobcards, 'tasks' => $tasks, 'sizes' => $sizes,
		'drivers' => $drivers, 'trucks' => $trucks,
		'baseprice_generalwaste' => $baseprice_generalwaste, 
		'binservicestok_generalwaste' => $binservicestok_generalwaste, 
		'binservicebaseprice_generalwaste' => $binservicebaseprice_generalwaste,
		'baseprice_heavymixed' => $baseprice_heavymixed, 
		'binservicestok_heavymixed' => $binservicestok_heavymixed, 
		'binservicebaseprice_heavymixed' => $binservicebaseprice_heavymixed,
		'baseprice_green' => $baseprice_green, 
		'binservicestok_green' => $binservicestok_green, 
		'binservicebaseprice_green' => $binservicebaseprice_green,
		'baseprice_dirt' => $baseprice_dirt, 
		'binservicestok_dirt' => $binservicestok_dirt, 
		'binservicebaseprice_dirt' => $binservicebaseprice_dirt
		]);
	}
	
	/** 
	* Task Assigning
	*/
	public function tasks_assign_view(Request $request){
		$idtask = $request['idtask'];
		$tasks = $this->fetch_task_by_id($idtask);
		
		$jobcards = jobcards::get_jobcards_by_id($tasks->idjobcard);
		$supplierdetail = $this->fetch_supplier_iduser($tasks->assignedto);
		$sizes = $this->getBinSize();
		$supplier = session('idSupplier');
		$drivers = $this->getDrivers();
		$trucks = $this->getTrucks();
		$statuses = $this->fetch_status();
		
		$baseprice_heavymixed = $this->getbinservicebaseprice_generalwaste($supplier, 2);
		
		foreach ($baseprice_heavymixed as $k => $v){
			$binservicestok_heavymixed[$v->idBinService] = $this->getstockupdatedprice_generalwaste($v->idBinService);
			$binservicebaseprice_heavymixed[$v->idBinSize] = $this->getspecificbaseprice_generalwaste($supplier,$v->idBinSize,2);
		}
		
		return view('scheduling.tasks_assigning', ['jobcards' => $jobcards, 'tasks' => $tasks,
		'sizes' => $sizes, 'supplier' => $supplierdetail,'statuses' => $statuses,
		'drivers' => $drivers, 'trucks' => $trucks,
		'baseprice_heavymixed' => $baseprice_heavymixed, 
		'binservicestok_heavymixed' => $binservicestok_heavymixed, 
		'binservicebaseprice_heavymixed' => $binservicebaseprice_heavymixed
		]);
	}
	
	/** 
	* Task Assigning
	*/
	public function update_status(Request $request){
		$idtask = $request['idtask_taskstatus'];
		tbltasks::where([
			'id' => $idtask
		])
		->update([
			'status' => $request['status']
		]);
		
		return redirect()->route('tasks_assign_view', ['idtask' => $idtask]);
	}
	
	/**
	* Fetch task detail by job cards
	*/
	private function fetch_task_by_job($idjobcards){
		$task = DB::table('tbltasks')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->select('tbltasks.id as idtask', 'tbltasks.*', 'tbljobcards.*', 'tblorder_items.*', 'tbltasks.status as taskstatus')
		->where([
			'tbltasks.idjobcard' => $idjobcards
		])
		->orderBy('tbltasks.job_type','ASC')
		->get();
		
		return $task;
	}
	
	/**
	* Fetch task detail by task id
	*/
	private function fetch_task_by_id($idtask){
		$task = DB::table('tbltasks')
		->leftJoin('tblstatus', 'tbltasks.status', '=', 'tblstatus.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tbljobcards.orderid', '=', 'tblorderservice.idOrderService')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->leftJoin('tblproject_sites', 'tbldelivery_address.idproject_site', '=', 'tbldelivery_address.idproject_site')
		->leftJoin('tblorganization', 'tblorderservice.idConsumer', '=', 'tblorganization.id')
		->select('tbltasks.id as idtask', 'tbltasks.*', 'tbljobcards.*', 'tbltasks.status as taskstatus',
		'tbljobcards.id as idjobcard', 'tblorder_items.*', 
		'tblorder_items.id as idorderitem', 'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tblorderservice.is_quote','tbldelivery_address.post_code', 'tblsize.size',
		'tbldrivers.id as iddriver', 'tblstatus.status as taskstatusword',
		'tbldrivers.*', 'tbldrivers.name as drivername',
		'tbltrucks.*', 'tbltrucks.id as idtrucks','tblorganization.organization_name',
		'tbltrucks.name as truckname')
		->where([
			'tbltasks.id' => $idtask
		])
		->first();
		
		return $task;
	}
	
	/**
	* Fetch order items detail by order items id
	*/
	public static function fetch_order_items_id($idorderitem){
		$fetch_order_items = DB::table('tblorder_items')
		->where([
			'id' => $idorderitem
		])
		->first();
		
		if(!is_null($fetch_order_items)){
			if(!is_null($fetch_order_items->isbinupdate) && $fetch_order_items->isbinupdate == '1'){
				$detail = DB::table('tblorder_items')
				->leftJoin('tblbinserviceupdates', 'tblbinserviceupdates.idBinServiceUpdates', '=', 'tblorder_items.idbinservice')
				->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblbinserviceupdates.idBinService')
				->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
				->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
				->leftJoin('tbldelivery_address', 'tbldelivery_address.iddelivery_address', '=', 'tblorder_items.iddelivery_address')
				->select(
					'tblbinserviceupdates.price', 'tblbintype.*', 'tblbintype.name AS bintype_name', 'tblbintype.description AS bintype_description', 
					'tblbintype.description2 AS bintype_description_2', 'tblsize.size', 'tbldelivery_address.*', 
					'tblorder_items.*'
				)
				->where(
					[
						'tblorder_items.id' => $fetch_order_items->id
					]
				)
				->first();
				return $detail;
			} else {
				$detail = DB::table('tblorder_items')
				->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
				->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
				->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
				->leftJoin('tbldelivery_address', 'tbldelivery_address.iddelivery_address', '=', 'tblorder_items.iddelivery_address')
				->select(
					'tblbinservice.price', 'tblbintype.name AS bintype_name','tblbintype.description AS bintype_description', 
					'tblbintype.description2 AS bintype_description_2', 'tblsize.size', 'tbldelivery_address.*', 
					'tblorder_items.*', 'tblbintype.*'
				)
				->where(
					[
						'tblorder_items.id' => $fetch_order_items->id
					]
				)
				
				->first();
				
				return $detail;
			}
		}
		return $detail;
	}
	
	/** 
	* Get Bin Sizes
	*/
	private function getBinSize (){
		$sizes = DB::table('tblsize')
				->select('idSize','size', 'dimensions')
				->orderBy('idSize', 'asc')
				->get();
		return $sizes;
	}
	
	/** 
	* Get Available Drivers
	*/
	private function getDrivers (){
		$drivers = DB::table('tbldrivers')
				->get();
		return $drivers;
	}
	
	/** 
	* Get Available Trucks
	*/
	private function getTrucks(){
		$trucks = DB::table('tbltrucks')
				->where('in_service', '<>', '1')
				->get();
		return $trucks;
	}
	
	/** 
	* Get Available Trucks
	*/
	private function fetch_supplier_iduser($iduser){
		$user = DB::table('tbluser')
		->leftJoin('tblsupplier', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
		->select('tbluser.*', 'tblsupplier.*')
		->where([
			'tbluser.idUser' => $iduser
		])
		->first();
		
		return $user;
	}
	
	/**
	* Fetch bin stock 
	* 1. General Waste
	*/
	private function getbinservicebaseprice_generalwaste($idSupplier, $bin){
		$binserviceprice = DB::table('tblbinservice')
			->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
				,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize')
			->where([
				'tblbinservice.idSupplier' => $idSupplier,
				'tblbinservice.idBinType' => $bin,
				])
			->get();
		return $binserviceprice;
	}
	
	private function getstockupdatedprice_generalwaste($idBinService){
		$binservicestockupdates = DB::table('tblbinservicestok')
				->select('tblbinservicestok.stock','tblbinservicestok.date')
				->where([
					'tblbinservicestok.idBinService' => $idBinService
				])
				->get();
		return $binservicestockupdates;
	}
	
	private function getspecificbaseprice_generalwaste($idSupplier, $idBinSize, $bin){
		$binserviceprice = DB::table('tblbinservice')
			->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
				,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize','tblbinservice.default_stock AS default_stock')
			->where([
				'tblbinservice.idSupplier' => $idSupplier,
				'tblbinservice.idBinSize' => $idBinSize,
				'tblbinservice.idBinType' => $bin,
				])
			->get();
		return $binserviceprice;
	}
	
	/**
	* Show all tasks
	*/
	private function count_all_tasks(){
		$today = date('Y-m-d', strtotime('now'));
		$count = DB::table('tbltasks')
		->count();
		
		return $count;
	}
	
	/**
	* Show count new uncompleted tasks count
	*/
	private function count_new_uncompleted_tasks(){
		$today = date('Y-m-d 00:00:00', strtotime('now'));
		$tommorrow = date('Y-m-d 00:00:00', strtotime('+1 day'));
		$count = DB::table('tbltasks')
		->whereBetween('tbltasks.starttime', [$today, $tommorrow])
		->where('tbltasks.status', '<>', 5)
		->where('tbltasks.status', '<>', 6)
		->count();
		
		return $count;
	}
	
	/**
	* Show count past uncompleted tasks count
	*/
	private function count_past_uncompleted_tasks(){
		$today = date('Y-m-d 00:00:00', strtotime('now'));
		$count = DB::table('tbltasks')
		->where('tbltasks.starttime', '<=', $today)
		->where('tbltasks.status', '<>', 5)
		->where('tbltasks.status', '<>', 6)
		->count();
		
		return $count;
	}
	
	/**
	* Show count complete tasks count
	*/
	private function count_completed_tasks(){
		$today = date('Y-m-d 00:00:00', strtotime('now'));
		$count = DB::table('tbltasks')
		->where('tbltasks.status', '=', 5)
		->count();
		
		return $count;
	}
	
	/**
	* Show statuses
	*/
	private function fetch_status(){
		$status = DB::table('tblstatus')
		->get();
		
		return $status;
	}
	
	/**
	* Show new completed tasks
	*/
	private function get_new_uncompleted_tasks(){
		$today = date('Y-m-d 00:00:00', strtotime('now'));
		$tommorrow = date('Y-m-d 00:00:00', strtotime('+1 day'));
		
		$task = DB::table('tbltasks')
		->leftJoin('tblstatus', 'tbltasks.status', '=', 'tblstatus.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tbljobcards.orderid', '=', 'tblorderservice.idOrderService')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->select('tbltasks.id as idtask', 'tbltasks.*', 'tbljobcards.*', 'tbltasks.status as taskstatus',
		'tbljobcards.id as idjobcard', 'tblorder_items.*', 
		'tblorder_items.id as idorderitem', 'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tblorderservice.is_quote','tbldelivery_address.post_code', 'tblsize.size',
		'tbldrivers.id as iddriver', 'tblstatus.status as taskstatusword',
		'tbldrivers.*', 'tbldrivers.name as drivername',
		'tbltrucks.*', 'tbltrucks.id as idtrucks',
		'tbltrucks.name as truckname')
		->whereBetween('tbltasks.starttime', [$today, $tommorrow])
		->where('tbltasks.status', '<>', 5)
		->where('tbltasks.status', '<>', 6)
		->where([
			'tbljobcards.is_paid' => 1,
			'tblorderservice.is_paid' => 1,
			'tbltasks.driver_updated' => 0
		])
		->get();
		
		return $task;
	}
	
	/**
	* Show  past uncompleted tasks
	*/
	private function get_past_uncompleted_tasks(){
		$today = date('Y-m-d', strtotime('now'));
		$task = DB::table('tbltasks')
		->leftJoin('tblstatus', 'tbltasks.status', '=', 'tblstatus.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tbljobcards.orderid', '=', 'tblorderservice.idOrderService')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->select('tbltasks.id as idtask', 'tbltasks.*', 'tbljobcards.*', 'tbltasks.status as taskstatus',
		'tbljobcards.id as idjobcard', 'tblorder_items.*', 
		'tblorder_items.id as idorderitem', 'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tblorderservice.is_quote','tbldelivery_address.post_code', 'tblsize.size',
		'tbldrivers.id as iddriver', 'tblstatus.status as taskstatusword',
		'tbldrivers.*', 'tbldrivers.name as drivername',
		'tbltrucks.*', 'tbltrucks.id as idtrucks',
		'tbltrucks.name as truckname')
		->where('tbltasks.starttime', '<=' ,$today)
		->where('tbltasks.status', '<>', 5)
		->where('tbltasks.status', '<>', 6)
		->where([
			'tbljobcards.is_paid' => 1,
			'tblorderservice.is_paid' => 1,
			'tbltasks.driver_updated' => 0
		])
		->get();
		
		return $task;
	}
	
	/**
	* Show  completed tasks
	*/
	private function get_completed_tasks(){
		$task = DB::table('tbltasks')
		->leftJoin('tblstatus', 'tbltasks.status', '=', 'tblstatus.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tbljobcards.orderid', '=', 'tblorderservice.idOrderService')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->select('tbltasks.id as idtask', 'tbltasks.*', 'tbltasks.job_type','tbljobcards.*', 'tbltasks.status as taskstatus',
		'tbljobcards.id as idjobcard', 'tblorder_items.*',  
		'tblorder_items.id as idorderitem', 'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tblorderservice.is_quote','tbldelivery_address.post_code', 'tblsize.size',
		'tbldrivers.id as iddriver', 'tblstatus.status as taskstatusword',
		'tbldrivers.*', 'tbldrivers.name as drivername',
		'tbltrucks.*', 'tbltrucks.id as idtrucks',
		'tbltrucks.name as truckname')
		->where('tbltasks.status', '=', 5)
		->where([
			'tbljobcards.is_paid' => 1,
			'tblorderservice.is_paid' => 1,
		])
		->get();
		
		return $task;
	}
	
	/**
	* Show zone tasks
	*/
	private function get_zone_tasks(){
		$task = DB::table('tbltasks')
		->leftJoin('tblstatus', 'tbltasks.status', '=', 'tblstatus.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tbljobcards.orderid', '=', 'tblorderservice.idOrderService')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->select('tbltasks.id as idtask', 'tbltasks.*', 'tbljobcards.*', 'tbltasks.status as taskstatus',
		'tbljobcards.id as idjobcard', 'tblorder_items.*', 
		'tblorder_items.id as idorderitem', 'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tblorderservice.is_quote','tbldelivery_address.post_code', 'tblsize.size',
		'tbldrivers.id as iddriver', 'tblstatus.status as taskstatusword',
		'tbldrivers.*', 'tbldrivers.name as drivername',
		'tbltrucks.*', 'tbltrucks.id as idtrucks',
		'tbltrucks.name as truckname')
		
		->where('tbltasks.status', '<>', 5)
		->where('tbltasks.status', '<>', 6)
		->where([
			'tbljobcards.is_paid' => 1,
			'tblorderservice.is_paid' => 1,
			'tbltasks.driver_updated' => 0
		])
		->get();
		
		return $task;
	}
	
	/**
	* Show date tasks
	*/
	private function get_date_tasks($start, $end){
		$start_range = date('Y-m-d 00:00:00', strtotime($start));
		$end_range = date('Y-m-d 23:59:59', strtotime($end));
		
		$task = DB::table('tbltasks')
		->leftJoin('tblstatus', 'tbltasks.status', '=', 'tblstatus.id')
		->leftJoin('tbljobcards', 'tbltasks.idjobcard', '=', 'tbljobcards.id')
		->leftJoin('tblorderservice', 'tbljobcards.orderid', '=', 'tblorderservice.idOrderService')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tbldrivers', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tbltrucks', 'tbltasks.assets', '=', 'tbltrucks.id')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->select('tbltasks.id as idtask', 'tbltasks.*', 'tbljobcards.*', 'tbltasks.status as taskstatus',
		'tbljobcards.id as idjobcard', 'tblorder_items.*', 
		'tblorder_items.id as idorderitem', 'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tblorderservice.is_quote','tbldelivery_address.post_code', 'tblsize.size',
		'tbldrivers.id as iddriver', 'tblstatus.status as taskstatusword',
		'tbldrivers.*', 'tbldrivers.name as drivername',
		'tbltrucks.*', 'tbltrucks.id as idtrucks',
		'tbltrucks.name as truckname')
		->whereBetween('tbltasks.starttime', [$start_range, $end_range])
		->where('tbltasks.status', '<>', 5)
		->where('tbltasks.status', '<>', 6)
		->where([
			'tbljobcards.is_paid' => 1,
			'tblorderservice.is_paid' => 1,
			'tbltasks.driver_updated' => 0
		])
		->get();
		
		return $task;
	}
	
	/**
	* Fetch drivers
	*/
	private function get_available_drivers(){
		$drivers = DB::table('tbldrivers')
		->leftJoin('tbltrucks', 'tbltrucks.iddriver', '=', 'tbldrivers.id')
		->select('tbldrivers.*', 'tbltrucks.*','tbldrivers.name as drivername', 'tbltrucks.name as truckname','tbldrivers.id as driverid')
		->groupBy('tbldrivers.id')
		->orderBy('tbldrivers.id', 'asc')
		->get();
		
		return $drivers;
	}
	/**
	* Fetch drivers based on their tasks
	*/
	public static function get_available_drivers_task($iddriver){
		$tasks = DB::table('tbldrivers')
		->leftJoin('tbltasks', 'tbltasks.driver', '=', 'tbldrivers.id')
		->leftJoin('tblstatus', 'tbltasks.status', '=', 'tblstatus.id')
		->leftJoin('tbltrucks', 'tbltrucks.iddriver', '=', 'tbldrivers.id')
		->leftJoin('tblorder_items', 'tbltasks.idorderitems', '=', 'tblorder_items.id')
		->leftJoin('tblorderservice', 'tblorder_items.idorder', '=', 'tblorderservice.idOrderService')
		->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorder_items.idbinservice')
		->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
		->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblorder_items.idsize')
		->leftJoin('tbldelivery_address', 'tblorder_items.iddelivery_address', '=', 'tbldelivery_address.iddelivery_address')
		->select('tbltasks.id as idtask', 'tbltasks.*','tbltasks.status as taskstatus','tblorder_items.*', 
		'tblorder_items.id as idorderitem', 'tbldelivery_address.unit_number','tbldelivery_address.street_name',
		'tblorderservice.is_quote','tbldelivery_address.post_code', 'tblsize.size',
		'tbldrivers.id as idreal_driver','tblstatus.status as taskstatusword',
		'tbldrivers.*', 'tbldrivers.name as drivername',
		'tbltrucks.*', 'tbltrucks.id as idtrucks',
		'tbltrucks.name as truckname')
		->where([
			'tblorderservice.is_paid' => 1,
			'tbltasks.is_paid' => 1,
			'tbldrivers.id' => $iddriver
		])
		->where('tbltasks.status', '<>', 5)
		->where('tbltasks.status', '<>', 6)
		->get();
		
		return $tasks;
	}
	
	/**************************************************************************/
	/**
	* Ajax stuffs
	*/
	
	public function task_assignment(Request $request){
		$validate = Validator::make($request->all(), [
			'idtask' => 'required',
		]);
		
		
		if($validate->fails()){
			$data = array(
				'status' => 0,
				'message' => 'Validation error'
			);
			
			return response()->json($data);
		} else {
			try{
				
				if(!is_null($request['driver'])){
					$previous_task = tbltasks::where([
						'id' => $request['idtask']
					])
					->first();
				
					if(!is_null($previous_task)){
						/* set idle to previous driver and truck */
						
						tbltasks::where([
							'id' => $request['idtask']
						])
						
						->update([
							'driver' => 0, 
							'assets' => 0,
							'driver_updated' => 0,
							'driver_notice' => 1
						]);
					
						tbldrivers::where([
							'id' => $previous_task->driver
						])
						->update([
							'onDuty' => 0, 
						]);
						
						tbltrucks::where([
							'id' => $previous_task->assets
						])
						->update([
							'onDuty' => 0, 
						]);
						
						/********************************************/
					}
					/** Get truck asset that associated with that driver and update it */
					$truck = DB::table('tbltrucks')
						->where('iddriver', $request['driver'])
						->first();
						
					tbltrucks::where([
							'id' => $truck->id
						])
						->update([
							'onDuty' => 0, 
						]);
					/**********************************************************/
					
					/* do the new status for the assigned task */
					tbltasks::where([
						'id' => $request['idtask']
					])
					->update([
						'driver' => $request['driver'], 
						'assets' => $truck->id,
						'driver_updated' => 1,
						'driver_notice' => 1
					]);
					
					tbldrivers::where([
						'id' => $request['driver']
					])
					->update([
						'onDuty' => 1, 
					]);
					
					tbltrucks::where([
						'id' => $truck->id
					])
					->update([
						'onDuty' => 1, 
					]);
					
					$data = array(
						'status' => 1,
						'message' => 'Task assigned'
					);
					
					return response()->json($data);
				} else {
					$previous_task = tbltasks::where([
						'id' => $request['idtask']
					])
					->first();
					
					if(!is_null($previous_task)){
						
						/* set idle to previous driver and truck */
						tbltasks::where([
						'id' => $request['idtask']
						])
						->update([
							'driver' => 0, 
							'assets' => 0,
							'driver_updated' => 0,
							'active_task' => 0
						]);
						
						tbldrivers::where([
							'id' => $previous_task->driver
						])
						->update([
							'onDuty' => 0, 
						]);
						
						tbltrucks::where([
							'id' => $previous_task->assets
						])
						->update([
							'onDuty' => 0, 
						]);
					}
					$data = array(
						'status' => 1,
						'message' => 'Task assigned'
					);
					
					return response()->json($data);
				}
			} catch(Exception $ex){
				$data = array(
					'status' => 0,
					'message' => 'Programmatical error'
				);
				
				return response()->json($data);
			}
		}
	}
	/**************************************************************************/
	/**
	* Update end time
	*/
	public function update_endtime(Request $request){
		$idtask = $request['idtask_endtime'];
		$endtime = $request['endtime'];
		try{
			tbltasks::where([
				'id' => $idtask
			])
			->update([
				'endtime' => date('Y-m-d', strtotime($endtime))
			]);
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', sprintf('Update estimated end task time success'));
			return redirect()->back();
		}catch(Exception $ex){
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', sprintf('Update estimated end task time failed'));
			return redirect()->back()->withInput($request->all());
		}
	}
	
	/**
	* Update task name
	*/
	public function update_taskname(Request $request){
		$idtask = $request['idtask_tasktitle'];
		$text = $request['name'];
		try{
			tbltasks::where([
				'id' => $idtask
			])
			->update([
				'taskname' => $text
			]);
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', sprintf('Update task name success'));
			return redirect()->back();
		}catch(Exception $ex){
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', sprintf('Update task name failed'));
			return redirect()->back()->withInput($request->all());
		}
	}
	
	public function assign_driver(Request $request){
		$iddriver = $request['iddriver'];
		$task = $request['task'];
		$validate = Validator::make($request->all(), [
			'iddriver' => 'required',
		]);
		
		
		if($validate->fails()){
			$data = array(
				'status' => '0',
				'message' => 'Validation fails.'
			);
			
			return response()->json($data);
		} else {
			if(!is_null($task) && count($task) > 0){
				/** Reset any task that belong to the selected driver first */
				$tasks = DB::table('tbltasks')
				->where('driver', $iddriver)
				->where('status','<>', 5)
				->where('status','<>', 6)
				->get();
				
				if(!is_null($tasks) && $tasks->count() > 0){
					foreach($tasks as $t){
						tbltasks::where([
							'id' => $t->id
						])
						->where('active_task','<>',1)
						->update([
							'driver' => 0, 
							'assets' => 0,
							'driver_updated' => 0,
						]);
					}
					
					
				}
				tbldrivers::where([
					'id' => $iddriver
				])
				->update([
					'onDuty' => 0, 
				]);
				/**********************************************************/
				/** Get truck asset that associated with that driver and update it */
				$truck = DB::table('tbltrucks')
					->where('iddriver', $iddriver)
					->first();
					
				tbltrucks::where([
						'id' => $truck->id
					])
					->update([
						'onDuty' => 0, 
					]);
				/**********************************************************/
				/** Assign status changes to task */
				foreach($task as $t){
					try{
						/* do the new status for the assigned task */
						tbltasks::where([
							'id' => $t
						])
						->where('active_task','<>',1)
						->update([
							'driver' => $iddriver, 
							'assets' => $truck->id,
							'driver_updated' => 1,
							'driver_notice' => 1
						]);
						
						tbldrivers::where([
							'id' => $iddriver
						])
						->update([
							'onDuty' => 1, 
						]);
						
						tbltrucks::where([
							'id' => $truck->id
						])
						->update([
							'onDuty' => 1, 
						]);
						
						
					} catch(Exception $ex){
						$data = array(
							'status' => '0',
							'message' => $ex
						);
						
						return response()->json($data);
					}
				}
				$data = array(
					'status' => '1',
					'message' => 'Task assigned'
				);
						
				return response()->json($data);
			} else {
				/** Reset any task that belong to the selected driver first */
				$tasks = DB::table('tbltasks')
				->where('driver', $iddriver)
				->where('status','<>', 5)
				->where('status','<>', 6)
				->get();
				
				if(!is_null($tasks) && $tasks->count() > 0){
					foreach($tasks as $t){
						tbltasks::where([
							'id' => $t->id
						])
						->update([
							'driver' => 0, 
							'assets' => 0,
							'driver_updated' => 0,
							'active_task' => 0,
						]);
					}
					
					
				}
				tbldrivers::where([
					'id' => $iddriver
				])
				->update([
					'onDuty' => 0, 
				]);
				/**********************************************************/
				/** Get truck asset that associated with that driver and update it */
				$truck = DB::table('tbltrucks')
					->where('iddriver', $iddriver)
					->first();
					
				tbltrucks::where([
						'id' => $truck->id
					])
					->update([
						'onDuty' => 0, 
					]);
				/**********************************************************/
				$data = array(
					'status' => '1',
					'message' => 'Task assigned'
				);
						
				return response()->json($data);
			}
			
		}
	}
	/**************************************************************************/
	
}
