<?php

namespace App\Http\Controllers\scheduling;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use URL;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;
use Carbon\Carbon;
use App\tblcustomer;
use App\tblorderservice;
use App\tblorderstatus;
use App\tblordersitem;
use App\tblbinserviceupdates;
use App\tblbinservicestok;
use App\tbluse;
use App\tbljobcards;
use App\tbltasks;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Log;

class jobcards extends Controller
{
	public function __construct(){
		
	}
	
	/***
	* Views 
	**/
	/**
	* Show new uncompleted orders, past uncompleted orders, and completed orders
	*/
	public function index(){
		$new_uncompleted_orders = $this->count_new_uncompleted_orders();
		$past_uncompleted_orders = $this->count_past_uncompleted_orders();
		$completed_orders = $this->count_past_completed_orders();
		$all_orders = $this->count_all_orders();
		
		return view('scheduling.jobcards_index', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders, 'all_orders' => $all_orders]);
	}
	
	/**
	* Show past uncompleted orders view
	*/
	public function past_uncompleted_orders(){
		$new_uncompleted_orders = $this->count_new_uncompleted_orders();
		$past_uncompleted_orders = $this->count_past_uncompleted_orders();
		$completed_orders = $this->count_past_completed_orders();
		$all_orders = $this->count_all_orders();
		
		$orders = $this->get_past_uncompleted_orders();
		return view('scheduling.jobcards_past_uncompleted_orders', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders, 'all_orders' => $all_orders,'orders' => $orders]);
	}
	
	/**
	* Show new uncompleted orders view
	*/
	public function new_uncompleted_orders(){
		$new_uncompleted_orders = $this->count_new_uncompleted_orders();
		$past_uncompleted_orders = $this->count_past_uncompleted_orders();
		$completed_orders = $this->count_past_completed_orders();
		$all_orders = $this->count_all_orders();
		
		$orders = $this->get_new_uncompleted_orders();
		return view('scheduling.jobcards_new_uncompleted_orders', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders,  'all_orders' => $all_orders,'orders' => $orders]);
	}
	
	/**
	* Show new uncompleted orders view
	*/
	public function completed_orders(){
		$new_uncompleted_orders = $this->count_new_uncompleted_orders();
		$past_uncompleted_orders = $this->count_past_uncompleted_orders();
		$completed_orders = $this->count_past_completed_orders();
		$all_orders = $this->count_all_orders();
		
		$orders = $this->get_completed_orders();
		return view('scheduling.jobcards_completed_orders', ['new_uncompleted_orders' => $new_uncompleted_orders, 
		'past_uncompleted_orders' => $past_uncompleted_orders,'completed_orders' =>$completed_orders,  'all_orders' => $all_orders,'orders' => $orders]);
	}
	
	/***
	* Data Fetching Methods 
	**/
	
	/**
	* Show all orders
	*/
	private function count_all_orders(){
		$today = date('Y-m-d', strtotime('now'));
		$count = DB::table('tbljobcards')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->count();
		
		return $count;
	}
	
	/**
	* Show count new uncompleted orders count
	*/
	private function count_new_uncompleted_orders(){
		$today = date('Y-m-d', strtotime('now'));
		$count = DB::table('tbljobcards')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->where([
			'tbljobcards.delivery_date' => $today,
			'tbljobcards.status' => 1
		])
		->count();
		
		return $count;
	}
	
	/**
	* Show count past uncompleted orders count
	*/
	private function count_past_uncompleted_orders(){
		$today = date('Y-m-d', strtotime('now'));
		$count = DB::table('tbljobcards')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->where([
			'tbljobcards.status' => 1
		])
		->whereDate('delivery_date', '<', $today)
		->count();
		
		return $count;
	}
	
	/**
	* Show count past completed orders count
	*/
	private function count_past_completed_orders(){
		$today = date('Y-m-d', strtotime('now'));
		$count = DB::table('tbljobcards')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->where([
			'tbljobcards.status' => 3
		])
		->count();
		
		return $count;
	}
	
	/**
	* Show new completed orders
	*/
	private function get_new_uncompleted_orders(){
		$today = date('Y-m-d', strtotime('now'));
		$count = DB::table('tbljobcards')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorderservice.idConsumer', '=', 'tblorganization.id')
		->leftJoin('tblorderstatus', 'tblorderservice.idOrderService', '=', 'tblorderstatus.idOrder')
		->leftJoin('tblproject_sites', 'tblorderservice.idproject_sites', '=', 'tblproject_sites.id')
		->leftJoin('tblsupplier', 'tblorderservice.idSupplier', '=', 'tblsupplier.idSupplier')
		->select('tbljobcards.id as idjobcards', 'tbljobcards.status as jobcard_status', 'tbljobcards.*', 'tblorganization.id as idorganization',
		'tblorganization.*', 'tblproject_sites.id as idprojectsite', 'tblproject_sites.*', 'tblsupplier.*','tblorderstatus.status as orderstatus',
		'tblorderstatus.*','tblorderservice.*')
		->where([
			'tbljobcards.delivery_date' => $today,
			'tbljobcards.status' => 1
		])
		->get();
		
		return $count;
	}
	
	/**
	* Show  past uncompleted orders
	*/
	private function get_past_uncompleted_orders(){
		$today = date('Y-m-d', strtotime('now'));
		$count = DB::table('tbljobcards')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorderservice.idConsumer', '=', 'tblorganization.id')
		->leftJoin('tblorderstatus', 'tblorderservice.idOrderService', '=', 'tblorderstatus.idOrder')
		->leftJoin('tblproject_sites', 'tblorderservice.idproject_sites', '=', 'tblproject_sites.id')
		->leftJoin('tblsupplier', 'tblorderservice.idSupplier', '=', 'tblsupplier.idSupplier')
		->select('tbljobcards.id as idjobcards', 'tbljobcards.status as jobcard_status', 'tbljobcards.*', 'tblorganization.id as idorganization',
		'tblorganization.*', 'tblproject_sites.id as idprojectsite', 'tblproject_sites.*', 'tblsupplier.*','tblorderstatus.status as orderstatus',
		'tblorderstatus.*','tblorderservice.*')
		->where([
			'tbljobcards.status' => 1
		])
		->whereDate('tbljobcards.delivery_date', '<', $today)
		->get();
		
		return $count;
	}
	
	/**
	* Show  completed orders
	*/
	private function get_completed_orders(){
		$today = date('Y-m-d', strtotime('now'));
		$count = DB::table('tbljobcards')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorderservice.idConsumer', '=', 'tblorganization.id')
		->leftJoin('tblorderstatus', 'tblorderservice.idOrderService', '=', 'tblorderstatus.idOrder')
		->leftJoin('tblproject_sites', 'tblorderservice.idproject_sites', '=', 'tblproject_sites.id')
		->leftJoin('tblsupplier', 'tblorderservice.idSupplier', '=', 'tblsupplier.idSupplier')
		->select('tbljobcards.id as idjobcards', 'tbljobcards.status as jobcard_status', 'tbljobcards.*', 'tblorganization.id as idorganization',
		'tblorganization.*', 'tblproject_sites.id as idprojectsite', 'tblproject_sites.*', 'tblsupplier.*','tblorderstatus.status as orderstatus',
		'tblorderstatus.*','tblorderservice.*')
		->where([
			'tbljobcards.status' => 3
		])
		->get();
		
		return $count;
	}
	
	/**
	* Show job cards by id
	*/
	public static function get_jobcards_by_id($idjobcards){
		$count = DB::table('tbljobcards')
		->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tbljobcards.orderid')
		->leftJoin('tblorganization', 'tblorderservice.idConsumer', '=', 'tblorganization.id')
		->leftJoin('tblorderstatus', 'tblorderservice.idOrderService', '=', 'tblorderstatus.idOrder')
		->leftJoin('tblproject_sites', 'tblorderservice.idproject_sites', '=', 'tblproject_sites.id')
		->leftJoin('tblsupplier', 'tblorderservice.idSupplier', '=', 'tblsupplier.idSupplier')
		->select('tbljobcards.id as idjobcards', 'tbljobcards.status as jobcard_status', 'tbljobcards.*', 'tblorganization.id as idorganization',
		'tblorganization.*', 'tblproject_sites.id as idprojectsite', 'tblproject_sites.*', 'tblsupplier.*','tblorderstatus.status as orderstatus',
		'tblorderstatus.*','tblorderservice.*' )
		->where([
			'tbljobcards.id' => $idjobcards
		])
		->first();
		
		return $count;
	}
	
	/**
	* Show job cards by id
	*/
	public function update_primary_contact(Request $request){
		$idjobcards = $request['idjobcards'];
		$primary_contact = $request['primary_contact'];
		
		try{
			tbljobcards::where([
				'id' => $idjobcards
			])
			->update([
				'primarycontact' => $primary_contact
			]);
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', sprintf('Update primary contact success'));
			return redirect()->back();
			
		} catch(Exception $ex){
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', sprintf('Update primary contact failed'));
			return redirect()->back()->withInput($request->all());
		}
		return $count;
	}
}
