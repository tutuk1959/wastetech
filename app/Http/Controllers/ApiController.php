<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\tblbintype;
use App\tblsize;
use App\tblservicearea;
use Validator;
use App\Transformer\bintypeTransformer;
use App\Transformer\sizeTransformer;
use App\Transformer\binhireTransformer;
use App\Transformer\stateTransformer;
use App\Transformer\binhireoptionsTransformer;
use App\Transformer\binhire_dailyTransformer;
use App\Transformer\paymentTransformer;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    protected $respose;
    protected $custZipCode;
    public function __construct(Response $response){
        $this->response = $response;
    }

    /**STORE BIN TYPE & ZIP CODE 
    public function getSizeBintype(Request $request){
        $validate = Validator::make($request->all(), [
            'zipcode' => 'required|string|max:5',
            'bintype' => 'required|string|max:12'
        ]);

        if($validate->fails()){
            return $this->response->errorMethodNotAllowed($validate->messages()->first());
        }

        $this->custZipCode = $request->input['zipcode'];
        return $this->response->withItem($this->custZipCode, new  zipcodeTransformer());
    }**/

    /**GET SIZE CODE **/
    public function showSizes(Request $request){
        if ($request->isMethod('get')){
            $size = tblsize::paginate();
            if (!$size) {
                return $this->response->errorNotFound('No bin size data');
            } else {
                return $this->response->withPaginator($size, new  sizeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }

    public function findSize(Request $request){
        if ($request->isMethod('get')){
            $idSize = $request['idSize'];
            $size = tblsize::where(['idSize' => $idSize])->first();
            if (!$size) {
                return $this->response->errorNotFound('No bin size data');
            } else {
                return $this->response->withItem($size, new  sizeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }

    /** GET BIN TYPE **/
    public function showBinType(Request $request){
        if ($request->isMethod('get')){
            $bintype = tblbintype::where('CodeType','<>', '1212')->paginate();
            if (!$bintype) {
                return $this->response->errorNotFound('No bin type data');
            } else {
                return $this->response->withPaginator($bintype, new  bintypeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }

     public function findBinType(Request $request){
        if ($request->isMethod('get')){
            $idBinType = $request['idBinType'];
            $bintype = tblbintype::where(['idBinType' => $idBinType])->first();
            if (!$bintype) {
                return $this->response->errorNotFound('No bin type data');
            } else {
                return $this->response->withItem($bintype, new  bintypeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }

    /** GET BIN HIRE **/
    public function showBinHire(Request $request){
        $idBinType = $request['idBinType'];
        $idBinSize = $request['idBinSize'];
        $zipcode = $request['zipcode'];
        $date1= $request['deliverydate'];
        $date2 = $request['collectiondate'];
		
        if ($request->isMethod('get')){

        	$nondeliveryfilter = $this->_filterDeliveryBins($date1, $date2, $idBinType, $idBinSize, $zipcode);
			
			if (is_null($nondeliveryfilter)) {
				//return $this->response->errorNotFound('No bin type data');
				$offers = $this->_getOfferLists($date1, $date2, $idBinType, $idBinSize, $zipcode);
				return response()->json(['offers' => $offers, 'status' => 0, 'data' => '', 'isupdate' => ''], 400);
			} else {
				return $this->response->withItem($nondeliveryfilter, new  binhireTransformer());
			}
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }
	
    public function getBestDeal(Request $request){
		$date1= date('Y-m-d', strtotime('now'));
		$today = date('l', strtotime('now'));
        if ($request->isMethod('get')){
			if($today == 'Saturday'){
				$binhire = DB::table('tblbinservice')
					->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
					->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
					->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
					->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
					->leftJoin('tblsupplier', 'tblbinservice.idSupplier','=','tblsupplier.idSupplier')
					->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					//->where([
					//	'tblsupplier.isOpenSaturday' => 1
					//])
					->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->orderBy('tblbinservice.price','asc')
					->get();
			} elseif ($today == 'Sunday') {
				$binhire = DB::table('tblbinservice')
					->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
					->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
					->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
					->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
					->leftJoin('tblsupplier', 'tblbinservice.idSupplier','=','tblsupplier.idSupplier')
					->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					//->where([
					//	'tblsupplier.isOpenSunday' => 1
					//])
					->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->orderBy('tblbinservice.price','asc')
					->get();
			} else {
				$binhire = DB::table('tblbinservice')
					->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
					->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
					->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
					->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
					->leftJoin('tblsupplier', 'tblbinservice.idSupplier','=','tblsupplier.idSupplier')
					->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->orderBy('tblbinservice.price','asc')
					->get();
			}
            $i=1;
            while ($i<=count($binhire)){
				if($today == 'Saturday'){
					$binhire = DB::table('tblbinservice')
						->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
						->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
						->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
						->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
						->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
						->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
						->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
						->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
						->leftJoin('tblsize', 'tblbinservice.idBinSize', '=','tblsize.idSize')
						->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblsize.size', 'tblbintype.name')
						->where('tblbinservice.stock','>',0)
						->where('tblbinservice.price','>',0)
						//->where('tblsupplier.isOpenSaturday','=',1)
						->where('tbluser.userStatus', '=', 1)
						->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblsize.size', 'tblbintype.name')
						->orderBy('tblbinservice.price','asc')
						->first();
				} elseif ($today == 'Sunday') {
					$binhire = DB::table('tblbinservice')
						->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
						->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
						->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
						->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
						->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
						->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
						->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
						->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
						->leftJoin('tblsize', 'tblbinservice.idBinSize', '=','tblsize.idSize' )
						->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblsize.size', 'tblbintype.name')
						->where('tblbinservice.stock','>',0)
						->where('tblbinservice.price','>',0)
						//->where('tblsupplier.isOpenSunday','=',1)
						->where('tbluser.userStatus', '=', 1)
						->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblsize.size', 'tblbintype.name')
						->orderBy('tblbinservice.price','asc')
						->first();
				} else {
					$binhire = DB::table('tblbinservice')
						->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
						->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
						->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
						->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
						->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
						->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
						->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
						->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
						->leftJoin('tblsize', 'tblbinservice.idBinSize', '=','tblsize.idSize' )
						 ->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblsize.size', 'tblbintype.name')
						->where('tblbinservice.stock','>',0)
						->where('tblbinservice.price','>',0)
						->where('tbluser.userStatus', '=', 1)
						->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblsize.size', 'tblbintype.name')
						->orderBy('tblbinservice.price','asc')
						->first();
				}
                if(!is_null($binhire)){
                    /**FILTER 1 (FIND NON DELIVERY DATA)**/
                    $binnondeliverydays = DB::table('tblbinnondelivery')
                    ->select('date','idSupplier','idBinType')
                    ->where([
                            'idSupplier' => $binhire->idSupplier, 
                            'idBinType' => $binhire->idBinType,
                        ])
                    ->where('date','=',$date1)
                    ->get();

                    if(!is_null($binnondeliverydays)){
                        /** GET THE WANTED VALUE **/
                        $nondeliveryfilter = $binhire;
                        break;
                    } 


                }
                $i++;
            }
            if (!$nondeliveryfilter) {
                return $this->response->errorNotFound('No bin type data');
            } else {
                return $this->response->withItem($nondeliveryfilter, new  binhire_dailyTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }

    public function showState(Request $request){
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['parentareacode' => '0'])->paginate();
             if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withPaginator($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }

    public function showArea(Request $request){
        $parentareacode = $request['zipcode'];
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['parentareacode' => $parentareacode])->orderBy('area', 'asc')->paginate(50);
             if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withPaginator($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }

    public function showPostcode(Request $request){
        $parentareacode = $request['zipcode'];
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['parentareacode' => $parentareacode])->paginate();
            if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withPaginator($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }

    public function findArea(Request $request){
        $zipcode = $request['zipcode'];
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['zipcode' => $zipcode])->first();
            if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withItem($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }

    public function binhireOptions(Request $request){
        $idBinType = $request['idBinType'];
        $idSupplier = $request['idSupplier'];

        $binhireoptions = DB::table('tblbinserviceoptions')
                        ->leftJoin('tblbinservice', function ($query){
                             $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                ->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                        })
                        ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                        ->where([
                            'tblbinserviceoptions.idBinType' => $idBinType,
                            'tblbinserviceoptions.idSupplier' =>$idSupplier
                            ])
                        ->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                        ->first();
        if (!$binhireoptions) {
            return $this->response->errorNotFound('No state data');
        } else {
            return $this->response->withItem($binhireoptions, new  binhireoptionsTransformer());
        }
    }

    public function payment(Request $request){
        $idBinHire = $request['idbinhire'];
        $zipcode = $request['zipcode'];
        $deliverydate = $request['deliverydate'];
        $collectiondate = $request['collectiondate'];

        if ($request->isMethod('get')){
            $binhire = DB::table('tblbinservice')
                    ->select('idBinService')
                    ->where(['idBinService' => $idBinHire])
                    ->first();
            if (!is_null($idBinHire)){
                $id = DB::table('tblpaymenttemp')->insertGetId(
                    ['idBinHire' => $binhire->idBinService, 'zipcode' => $zipcode, 'deliveryDate' => $deliverydate, 'collectionDate' => $collectiondate, 'paid' => 0]
                );

                if (!is_null($id)){
                    return $this->response->withItem($id, new  paymentTransformer());
                } else {
                    return $this->response->errorInternalError('Internal server error');
                }
                
            } else {
                return $this->response->errorNotFound('No bin hire');
            }

        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }

    /**
     * Filter query from showBinType method
     * Make it reuseable to other method
     */
    private function _filterDeliveryBins($date1, $date2, $idBinType, $idBinSize, $zipcode){
    	$nondeliveryfilter = null;
		$today = date('l', strtotime('now'));
		$day_deliv = date('l', strtotime($date1));
		$day_col = date('l', strtotime($date2));

		$queryLog = [];
		DB::enableQueryLog(); // Enable query log

		$args = [
			'tblbinservice.idBinType' => $idBinType,
			'tblbinservice.idBinSize' => $idBinSize,
			'tblservicearea.zipcode' => $zipcode
		];

		if( $day_deliv == 'Saturday' || $day_col == 'Saturday' ){
			$args['tblsupplier.isOpenSaturday'] = 1;
		}

		if( $day_deliv == 'Sunday' || $day_col == 'Sunday' ){
			$args['tblsupplier.isOpenSunday'] = 1;
		}

		$binhire = DB::table('tblbinservice')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->leftJoin('tblsupplier', 'tblbinservice.idSupplier','=','tblsupplier.idSupplier')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
			->where($args)
			->where('tblbinservice.stock','>',0)
			->where('tblbinservice.price','>',0)
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
			->orderBy('tblbinservice.price','asc')
			->get();

		$queryLog[0] = DB::getQueryLog();
		
		/**
		 * Check if there is any price update in tblbinserviceupdates
		 */
        $i=1;
        while ($i<=count($binhire)){
			$args = [
				'tblbinservice.idBinType' => $idBinType,
				'tblbinservice.idBinSize' => $idBinSize,
				'tblservicearea.zipcode' => $zipcode,
				'tbluser.userStatus' => '1'
			];

			if( $day_deliv == 'Saturday' || $day_col == 'Saturday' ){
				$args['tblsupplier.isOpenSaturday'] = 1;
			}

			if( $day_deliv == 'Sunday' || $day_col == 'Sunday' ){
				$args['tblsupplier.isOpenSunday'] = 1;
			}

			$binhire = DB::table('tblbinservice')
				->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
				->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
				->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
				->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
				->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
				->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
				->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
				->select('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
				->where($args)
				->where('tblbinserviceupdates.price','>',0)
				->whereColumn('tblbinserviceupdates.price','<>','tblbinservice.price')
				->where('tblbinserviceupdates.date','=', $date1)
				->groupBy('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
				->orderBy('tblbinserviceupdates.price','asc')
				->get();

			$queryLog[1] = DB::getQueryLog();
            if($binhire->count() != 0){

            	/**
            	 * Kalo ada price update, cek lagi stoknya
            	 */				
				foreach($binhire as $k){
					$binhire_send = DB::table('tblbinservice')
					->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
					->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
					->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
					->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
					->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
					->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
					->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
					->select('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->where([
						'tblbinservice.idBinType' => $idBinType,
						'tblbinservice.idBinSize' => $idBinSize,
						'tblbinservice.idSupplier' => $k->idSupplier,
						'tblservicearea.zipcode' => $zipcode,
						'tbluser.userStatus' => '1'
					])
					->where('tblbinserviceupdates.stock','>',0)
					->where('tblbinserviceupdates.price','>',0)
					->whereColumn('tblbinserviceupdates.price','<>','tblbinservice.price')
					->where('tblbinserviceupdates.date','=', $date1)
					->groupBy('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->orderBy('tblbinserviceupdates.price','asc')
					->first();

					$queryLog[10] = DB::getQueryLog();
					
					if(!is_null($binhire_send)){
						 $binhire_compare = DB::table('tblbinservice')
						->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
						->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
						->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
						->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
						->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
						->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
						->leftJoin('tblbinnondelivery', 'tblsupplier.idSupplier','=', 'tblsupplier.idSupplier')
						->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
						->where([
							'tblbinservice.idBinType' => $idBinType,
							'tblbinservice.idBinSize' => $idBinSize,
							'tblservicearea.zipcode' => $zipcode, 
						])
						->where('tblbinservice.stock','>=',0)
						->where('tblbinservice.price','>',0)
                        ->where('tblbinservice.idSupplier','<>',$binhire_send->idSupplier)
						->where('tblbinnondelivery.date', '<>' , $date1)
						
						->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
						->orderBy('tblbinservice.price','asc')
						->first();

						$queryLog[11] = DB::getQueryLog();
						
						if(!is_null($binhire_compare)){
							
							if($binhire_compare->price <= $binhire_send->price){
								
								/**FILTER 1 (FIND NON DELIVERY DATA)**/
								$binnondeliverydays = DB::table('tblbinnondelivery')
								->select('date','idSupplier','idBinType')
								->where([
									'idSupplier' => $binhire_compare->idSupplier, 
									'idBinType' => $idBinType,
								])
								//->whereBetween('date',[$date1,$date2])
								->where('date', '=', $date1)
								->get();
								
								
								if($binnondeliverydays->count() == 0){
									/** GET THE WANTED VALUE **/
									$nondeliveryfilter = $binhire_compare;
									break;
								} else {
									$nondeliveryfilter = null;
									
								}
							} else {
								/**FILTER 1 (FIND NON DELIVERY DATA)**/
								$binnondeliverydays = DB::table('tblbinnondelivery')
								->select('date','idSupplier','idBinType')
								->where([
									'idSupplier' => $k->idSupplier, 
									'idBinType' => $idBinType,
								])
								//->whereBetween('date',[$date1,$date2])
								->where('date', '=', $date1)
								->get();
								
								
								if($binnondeliverydays->count() == 0){
									/** GET THE WANTED VALUE **/
									$nondeliveryfilter = $k;
									break;
								} else {
									$nondeliveryfilter = null;
									
								}
							}
						}
						
					}
				}
            } else{
				$binhire = DB::table('tblbinservice')
					->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
					->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
					->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
					->leftJoin('tblsupplier', 'tblbinservice.idSupplier','=','tblsupplier.idSupplier')
					->leftJoin('tblbinnondelivery', 'tblsupplier.idSupplier','=', 'tblbinnondelivery.idSupplier')
					->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->where([
						'tblbinservice.idBinType' => $idBinType,
						'tblbinservice.idBinSize' => $idBinSize,
						'tblservicearea.zipcode' => $zipcode,
					])
					->where('tblbinservice.stock','>',0)
					->where('tblbinservice.price','>',0)
		
					->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->orderBy('tblbinservice.price','asc')
					->get();

					$queryLog[12] = DB::getQueryLog();
				
				foreach($binhire as $k){
					$binhire_send = DB::table('tblbinservice')
					->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
					->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
					->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
					->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
					->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
					->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
					->leftJoin('tblbinnondelivery', 'tblsupplier.idSupplier','=', 'tblsupplier.idSupplier')
					->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->where([
						'tblbinservice.idBinType' => $idBinType,
						'tblbinservice.idBinSize' => $idBinSize,
						'tblbinservice.idSupplier' => $k->idSupplier,
						'tblservicearea.zipcode' => $zipcode, 
					])
					->where('tblbinservice.stock','>=',0)
					->where('tblbinservice.price','>',0)
					->where('tblbinnondelivery.date', '<>' , $date1)
					
					->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
					->orderBy('tblbinservice.price','asc')
					->first();

					$queryLog[13] = DB::getQueryLog();
					
					 if(!is_null($binhire_send)){
						//FILTER 1 (FIND NON DELIVERY DATA)
						$binnondeliverydays = DB::table('tblbinnondelivery')
						->select('date','idSupplier','idBinType')
						->where([
							'idSupplier' => $k->idSupplier, 
							'idBinType' => $idBinType,
						])
						//->whereBetween('date',[$date1,$date2])
						->where('date', '=', $date1)
						->get();
						
						
						if($binnondeliverydays->count() == 0){
							// GET THE WANTED VALUE
							$nondeliveryfilter = $k;
							break;
						} else {
							$nondeliveryfilter = null;
							
						}
					}
				}
			}
			$i++;
		}
		return $nondeliveryfilter;
		//return $queryLog;
    }

    private function _getOfferLists($date1, $date2, $idBinType, $idBinSize, $zipcode){
    	$offers = array();

    	/**
    	 * Offer 1: Offer other type with same size and location
    	 */
    	$types = tblbintype::whereNotIn('idBinType', [3, $idBinType])->pluck('idBinType')->toArray();
    	foreach($types as $type_id){
    		// Check availability based on previous data but different bin type
        	$nondeliveryfilter = $this->_filterDeliveryBins($date1, $date2, $type_id, $idBinSize, $zipcode);
			
			if (!is_null($nondeliveryfilter)) {
				$nondeliveryfilter->type_name = tblbintype::where('idBinType', $type_id)->first()->name;
				$nondeliveryfilter->size_name = tblsize::where('idSize', $idBinSize)->first()->size;
				$offers['type'][] = $nondeliveryfilter;
			}
    	}

    	/**
    	 * Offer 2: Offer other date
    	 */
    	// Case 1: Delivery date kalo lebih dari tgl besok dimajuin 3 hari lebih awal ato sampe tgl besok
    	$offers['date'] = array();
    	if( date_create($date1) > date_create("tomorrow") ){
    		$diff = date_diff( date_create($date1), date_create("tomorrow") );
    		$max = $diff->d < 3 ? $diff->d : 3;

    		for ($i=1; $i <= $max; $i++) { 
    			// Change date1
    			$new_date1 = date_create($date1)->modify('-'.$i.' day')->format('Y-m-d');
    			$new_date2 = date_create($date2)->modify('-'.$i.' day')->format('Y-m-d');
	    		// Check availability based on previous data but different date
	        	$nondeliveryfilter = $this->_filterDeliveryBins($new_date1, $new_date2, $idBinType, $idBinSize, $zipcode);
				
				if (!is_null($nondeliveryfilter)) {
					$nondeliveryfilter->type_name = tblbintype::where('idBinType', $idBinType)->first()->name;
					$nondeliveryfilter->size_name = tblsize::where('idSize', $idBinSize)->first()->size;
					$nondeliveryfilter->delivery_date = $new_date1;
					$nondeliveryfilter->collection_date = $new_date2;
					$offers['date'][] = $nondeliveryfilter;
				}
    		}
    	}

    	// Case 2: Delivery date dimundurin 3 hari, cek dulu yg sebelumnya udah ada penawaran ato ga. Maksimal 3 penawaran biar optimal. Kebanyakan pilihan juga jele
    	if( isset($offers['date']) && count($offers['date']) < 3 ){
    		$max = 3 - count($offers['date']);

    		for ($i=1; $i <= $max; $i++) { 
    			// Change date1
    			$new_date1 = date_create($date1)->modify('+'.$i.' day')->format('Y-m-d');
    			$new_date2 = date_create($date2)->modify('+'.$i.' day')->format('Y-m-d');
	    		// Check availability based on previous data but different date
	        	$nondeliveryfilter = $this->_filterDeliveryBins($new_date1, $new_date2, $idBinType, $idBinSize, $zipcode);
				
				if (!is_null($nondeliveryfilter)) {
					$nondeliveryfilter->type_name = tblbintype::where('idBinType', $idBinType)->first()->name;
					$nondeliveryfilter->size_name = tblsize::where('idSize', $idBinSize)->first()->size;
					$nondeliveryfilter->delivery_date = $new_date1;
					$nondeliveryfilter->collection_date = $new_date2;
					$offers['date'][] = $nondeliveryfilter;
				}
    		}
    	}


    	/**
    	 * Offer 3: Offer other size
    	 */
    	$sizes = tblsize::whereNotIn('idSize', [$idBinSize])->pluck('idSize')->toArray();
    	foreach($sizes as $size){
    		// Check availability based on previous data but different bin size
        	$nondeliveryfilter = $this->_filterDeliveryBins($date1, $date2, $idBinType, $size, $zipcode);
			
			if (!is_null($nondeliveryfilter)) {
				$nondeliveryfilter->type_name = tblbintype::where('idBinType', $idBinType)->first()->name;
				$nondeliveryfilter->size_name = tblsize::where('idSize', $size)->first()->size;
				$offers['size'][] = $nondeliveryfilter;
			}
    	}

    	return $offers;
    }
}
