<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblservicearea extends Model
{
	protected $table = 'tblservicearea';
	public $timestamps = false;
	public $fillable = ['zipcode', 'area', 'parentareacode'];
}
