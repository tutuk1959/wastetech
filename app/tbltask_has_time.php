<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbltask_has_time extends Model
{
	protected $table = 'tbltask_has_time';
	public $timestamps = false;

	protected $fillable = [
		'task_status_id',
		'type_id',
		'time',
	];

	protected $casts = [
		'time' => 'datetime'
	];
}
