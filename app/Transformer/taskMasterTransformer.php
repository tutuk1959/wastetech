<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class taskMasterTransformer extends TransformerAbstract {
 
    public function transform($tasks) {
        return [
            'idtask' => $tasks->idtask,
            'tasktitle' => $tasks->tasktitle,
			'jobtype' => $tasks->jobtype,
            'tasknote' => $tasks->tasknote,
            'taskstatus' => $tasks->taskstatus,
			'taskstatusid' => $tasks->taskstatusid,
			'jobcards_orderstatus' => $tasks->jobcards_orderstatus,
			'suppliername' => $tasks->suppliername,
			'supplieremail' => $tasks->supplieremail,
			'supplierPhoneNumber' => $tasks->supplierPhoneNumber,
			'supplierMobile' => $tasks->supplierMobile,
			'customername' => $tasks->customername,
			'customerlandline' => $tasks->customernlandline,
			'customerphone' => $tasks->customerphone,
			'idjobcard' => $tasks->idjobcard,
			'idorderitem' => $tasks->idorderitem,
			'bintype' => $tasks->bintype,
			'size' => $tasks->size,
			'paymentrefrence' => $tasks->paymentrefrence,
			'orderquantity' => $tasks->orderquantity,
			'orderdeliverycomments' => $tasks->orderdeliverycomments,
			'delivery_date' => $tasks->delivery_date,
			'collection_date' => $tasks->collection_date,
			'unit_number' => $tasks->unit_number,
			'lot_number' => $tasks->lot_number,
			'street_name' => $tasks->street_name,
			'suburb' => $tasks->suburb,
			'postcode' => $tasks->post_code,
			'state' => $tasks->state,
			'latitude' => $tasks->latitude,
			'contact' => $tasks->contact,
			'idDriver' => $tasks->idDriver,
			'drivername' => $tasks->drivername,
			'onDuty' => $tasks->onDuty,
			'idtrucks' => $tasks->idtrucks,
			'truckname' => $tasks->truckname,
        ];
    }
 }