<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class driverTransformer extends TransformerAbstract {
 
    public function transform($driver) {
		
        return [
            'driverName' => $driver->driverName,
            'hours' => $driver->hours,
			'licence' => $driver->licence,
			'status' => $driver->status,
			'notes' => $driver->notes,
			'onDuty' => $driver->onDuty,
			'email_address' => $driver->email_address,
			'supplierName' => $driver->supplierName,
			'fullAddress' => $driver->fullAddress,
        ];
    }
 }