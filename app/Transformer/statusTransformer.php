<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class statusTransformer extends TransformerAbstract {
 
    public function transform($status) {
		
        return [
            'id' => $status->id,
            'status' => $status->status,
			'status_html' => $status->status_html,
        ];
    }
 }