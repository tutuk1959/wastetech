<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class taskProgressTransformer extends TransformerAbstract {

    public function transform($tasks) {
        return [
            'idtask' => $tasks->idtask,
            'hastatusid' => $tasks->hastatusid,
            'totaltime' => $tasks->totaltime,
            'status' => $tasks->status,
			'has_times' => $tasks->has_times,
        ];
    }
 }
