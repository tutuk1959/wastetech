<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblbinserviceupdates extends Model
{
	protected $table = 'tblbinserviceupdates';
	public $timestamps = false;
}
