<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterUserMail extends Mailable
{
	use Queueable, SerializesModels;
	public $RegisterUser;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($RegisterUser)
    {
		$this->RegisterUser = $RegisterUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		return $this->from('admin@somsdevone.com')
		->view('mails.mail_register_user')
		->text('mails.mail_register_user_plain');
	}
}
