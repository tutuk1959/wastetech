<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblprojectsites extends Model
{
	protected $table = 'tblproject_sites';
	public $timestamps = false;
}
