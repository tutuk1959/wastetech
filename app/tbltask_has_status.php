<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbltask_has_status extends Model
{
	protected $table = 'tbltask_has_status';
	public $timestamps = false;

	protected $fillable = [
		'task_id',
		'status_id',
		'total_time'
	];

	public function hasTimes()
	{
		return $this->hasMany(tbltask_has_time::class, 'task_status_id', 'id');
	}
}
