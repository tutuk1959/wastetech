<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblbinserviceoptions extends Model
{
	protected $table = 'tblbinserviceoptions';
	public $timestamps = false;
}
