<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'tblorderservice';
	public $timestamps = false;
}
