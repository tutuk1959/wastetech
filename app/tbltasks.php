<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbltasks extends Model
{
	protected $table = 'tbltasks';

	public $timestamps = false;

	public function statuses()
	{
		return $this->hasMany(tbltask_has_status::class, 'task_id', 'id');
	}
}
