<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_confirmation extends Model
{
	protected $table = 'user_confirmation';
	public $timestamps = false;
}
